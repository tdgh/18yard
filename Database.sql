-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 19, 2020 at 06:38 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `football`
--

-- --------------------------------------------------------

--
-- Table structure for table `unisoft_agents`
--

CREATE TABLE `unisoft_agents` (
  `id` int(11) NOT NULL,
  `name` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `passport_no` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `unisoft_agents_players`
--

CREATE TABLE `unisoft_agents_players` (
  `id` int(11) NOT NULL,
  `agent_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `mandate_file` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entry_date` datetime NOT NULL,
  `expiry_date` datetime NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `status_date` date NOT NULL,
  `status_comment` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `unisoft_countries`
--

CREATE TABLE `unisoft_countries` (
  `id` int(11) NOT NULL,
  `name` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nationality` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `unisoft_countries`
--

INSERT INTO `unisoft_countries` (`id`, `name`, `country_code`, `nationality`, `status`) VALUES
(1, 'ANDORRA', '+376', 'ANDORRAN', 1),
(2, 'UNITED ARAB EMIRATES', '+971', 'EMIRATI', 1),
(3, 'AFGHANISTAN', '+93', 'AFGHAN', 1),
(4, 'ANTIGUA AND BARBUDA', '+1268', 'ANTIGUANS', 1),
(5, 'ANGUILLA', '+1264', 'ANGUILLIAN', 1),
(6, 'ALBANIA', '+355', 'ALBANIAN', 1),
(7, 'ARMENIA', '+374', 'ARMENIAN', 1),
(8, 'NETHERLANDS ANTILLES', '+599', 'DUTCH', 1),
(9, 'ANGOLA', '+244', 'ANGOLAN', 1),
(10, 'ANTARCTICA', '+672', 'ANTARTIC RESIDENT', 1),
(11, 'ARGENTINA', '+54', 'ARGENTINEAN', 1),
(12, 'AMERICAN SAMOA', '+1684', 'AMERICAN SAMOAN', 1),
(13, 'AUSTRIA', '+43', 'AUSTRIAN', 1),
(14, 'AUSTRALIA', '+61', 'AUSTRALIAN', 1),
(15, 'ARUBA', '+297', 'ARUBAN', 1),
(16, 'AZERBAIJAN', '+994', 'AZERBAIJANI', 1),
(17, 'BOSNIA AND HERZEGOVINA', '+387', 'BOSNIAN', 1),
(18, 'BARBADOS', '+1246', 'BARBADIAN', 1),
(19, 'BANGLADESH', '+880', 'BANGLADESHI', 1),
(20, 'BELGIUM', '+32', 'BELGIAN', 1),
(21, 'BURKINA FASO', '+226', 'BURKINABE', 1),
(22, 'BULGARIA', '+359', 'BULGARIAN', 1),
(23, 'BAHRAIN', '+973', 'BAHRAINI', 1),
(24, 'BURUNDI', '+257', 'BURUNDIAN', 1),
(25, 'BENIN', '+229', 'BENINESE', 1),
(26, 'SAINT BARTHELEMY', '+590', 'BARTHELEMOISE', 1),
(27, 'BERMUDA', '+1441', 'BERMUDIAN', 1),
(28, 'BRUNEI DARUSSALAM', '+673', 'BRUNEIAN', 1),
(29, 'BOLIVIA', '+591', 'BOLIVIAN', 1),
(30, 'BRAZIL', '+55', 'BRAZILIAN', 1),
(31, 'BAHAMAS', '+1242', 'BAHAMIAN', 1),
(32, 'BHUTAN', '+975', 'BHUTANESE', 1),
(33, 'BOTSWANA', '+267', 'MOTSWANA', 1),
(34, 'BELARUS', '+375', 'BELARUSIAN', 1),
(35, 'BELIZE', '+501', 'BELIZEAN', 1),
(36, 'CANADA', '+1', 'CANADIAN', 1),
(37, 'COCOS (KEELING) ISLANDS', '+61', 'COCOS ISLANDER', 1),
(38, 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', '+243', 'CONGOLESE', 1),
(39, 'CENTRAL AFRICAN REPUBLIC', '+236', 'CENTRAL AFRICAN', 1),
(40, 'CONGO', '+242', 'CONGOLESE', 1),
(41, 'SWITZERLAND', '+41', 'SWISS', 1),
(42, 'COTE D\'IVOIRE', '+225', 'IVORIAN', 1),
(43, 'COOK ISLANDS', '+682', 'COOK ISLANDER', 1),
(44, 'CHILE', '+56', 'CHILEAN', 1),
(45, 'CAMEROON', '+237', 'CAMEROONIAN', 1),
(46, 'CHINA', '+86', 'CHINESE', 1),
(47, 'COLOMBIA', '+57', 'COLOMBIAN', 1),
(48, 'COSTA RICA', '+506', 'COSTA RICAN', 1),
(49, 'CUBA', '+53', 'CUBAN', 1),
(50, 'CAPE VERDE', '+238', 'CAPE VERDEAN', 1),
(51, 'CHRISTMAS ISLAND', '+61', 'CHRISTMAS ISLANDER', 1),
(52, 'CYPRUS', '+357', 'CYPRIOT', 1),
(53, 'CZECH REPUBLIC', '+420', 'CZECH', 1),
(54, 'GERMANY', '+49', 'GERMAN', 1),
(55, 'DJIBOUTI', '+253', 'DJIBOUTIAN', 1),
(56, 'DENMARK', '+45', 'DANISH', 1),
(57, 'DOMINICA', '+1767', 'DOMINICAN', 1),
(58, 'DOMINICAN REPUBLIC', '+1809', 'DOMINICAN', 1),
(59, 'ALGERIA', '+213', 'ALGERIAN', 1),
(60, 'ECUADOR', '+593', 'ECUADOREAN', 1),
(61, 'ESTONIA', '+372', 'ESTONIAN', 1),
(62, 'EGYPT', '+20', 'EGYPTIAN', 1),
(63, 'ERITREA', '+291', 'ERITREAN', 1),
(64, 'SPAIN', '+34', 'SPANIARD', 1),
(65, 'ETHIOPIA', '+251', 'ETHIOPIAN', 1),
(66, 'FINLAND', '+358', 'FINNISH', 1),
(67, 'FIJI', '+679', 'FIJIAN', 1),
(68, 'FALKLAND ISLANDS (MALVINAS)', '+500', 'FALKLAND ISLANDER', 1),
(69, 'MICRONESIA, FEDERATED STATES OF', '+691', 'MICRONESIAN', 1),
(70, 'FAROE ISLANDS', '+298', 'FAROESE', 1),
(71, 'FRANCE', '+33', 'FRENCH', 1),
(72, 'GABON', '+241', 'GABONESE', 1),
(73, 'UNITED KINGDOM', '+44', 'BRITON', 1),
(74, 'GRENADA', '+1473', 'GRENADIAN', 1),
(75, 'GEORGIA', '+995', 'GEORGIAN', 1),
(76, 'GHANA', '+233', 'GHANAIAN', 1),
(77, 'GIBRALTAR', '+350', 'GILBRALTARIAN', 1),
(78, 'GREENLAND', '+299', 'GREENLANDER', 1),
(79, 'GAMBIA', '+220', 'GAMBIAN', 1),
(80, 'GUINEA', '+224', 'GUINEAN', 1),
(81, 'EQUATORIAL GUINEA', '+240', 'EQUATORIAL GUINEAN', 1),
(82, 'GREECE', '+30', 'GREEK', 1),
(83, 'GUADELOUPE', '+590', 'GUANDELOUPIAN', 1),
(84, 'GUATEMALA', '+502', 'GUATEMALAN', 1),
(85, 'GUAM', '+1671', 'GUAMANIAN', 1),
(86, 'GUINEA-BISSAU', '+245', 'GUINEA-BISSAUAN', 1),
(87, 'GUYANA', '+592', 'GUYANESE', 1),
(88, 'HONG KONG', '+852', 'HONGKONGER', 1),
(89, 'HONDURAS', '+504', 'HONDURAN', 1),
(90, 'CROATIA', '+385', 'CROATIAN', 1),
(91, 'HAITI', '+509', 'HAITIAN', 1),
(92, 'HUNGARY', '+36', 'HUNGARIAN', 1),
(93, 'INDONESIA', '+62', 'INDONESIAN', 1),
(94, 'IRELAND', '+353', 'IRISH', 1),
(95, 'ISRAEL', '+972', 'ISRAELI', 1),
(96, 'ISLE OF MAN', '+44', 'MANX', 1),
(97, 'INDIA', '+91', 'INDIAN', 1),
(98, 'IRAQ', '+964', 'IRAQI', 1),
(99, 'IRAN, ISLAMIC REPUBLIC OF', '+98', 'IRANIAN', 1),
(100, 'ICELAND', '+354', 'ICELANDER', 1),
(101, 'ITALY', '+39', 'ITALIAN', 1),
(102, 'JAMAICA', '+1876', 'JAMAICAN', 1),
(103, 'JORDAN', '+962', 'JORDANIAN', 1),
(104, 'JAPAN', '+81', 'JAPANESE', 1),
(105, 'KENYA', '+254', 'KENYAN', 1),
(106, 'KYRGYZSTAN', '+996', 'KYRGYZ', 1),
(107, 'CAMBODIA', '+855', 'CAMBODIAN', 1),
(108, 'KIRIBATI', '+686', 'I-KIRIBATI', 1),
(109, 'COMOROS', '+269', 'COMORAN', 1),
(110, 'SAINT KITTS AND NEVIS', '+1869', 'KITTITIAN', 1),
(111, 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', '+850', 'NORTH KOREAN', 1),
(112, 'KOREA, REPUBLIC OF', '+82', 'KOREAN', 1),
(113, 'KUWAIT', '+965', 'KUWAITI', 1),
(114, 'CAYMAN ISLANDS', '+1345', 'CAYMANIAN', 1),
(115, 'KAZAKHSTAN', '+7', 'KAZAKHSTANI', 1),
(116, 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', '+856', 'LAOS', 1),
(117, 'LEBANON', '+961', 'LEBANESE', 1),
(118, 'SAINT LUCIA', '+1758', 'SAINT LUCIAN', 1),
(119, 'LIECHTENSTEIN', '+423', 'LIECHTENSTEINER', 1),
(120, 'SRI LANKA', '+94', 'SRI LANKAN', 1),
(121, 'LIBERIA', '+231', 'LIBERIAN', 1),
(122, 'LESOTHO', '+266', 'BASOTHO', 1),
(123, 'LITHUANIA', '+370', 'LITHUANIAN', 1),
(124, 'LUXEMBOURG', '+352', 'LUXEMBOURGER', 1),
(125, 'LATVIA', '+371', 'LATVIAN', 1),
(126, 'LIBYAN ARAB JAMAHIRIYA', '+218', 'LIBYAN', 1),
(127, 'MOROCCO', '+212', 'MOROCCAN', 1),
(128, 'MONACO', '+377', 'MONACAN', 1),
(129, 'MOLDOVA, REPUBLIC OF', '+373', 'MOLDOVAN', 1),
(130, 'MONTENEGRO', '+382', 'MONTENEGRIN', 1),
(131, 'SAINT MARTIN', '+1599', 'SAINT-MARTINOISE', 1),
(132, 'MADAGASCAR', '+261', 'MALAGASY', 1),
(133, 'MARSHALL ISLANDS', '+692', 'MARSHALLESE', 1),
(134, 'MARTINIQUE', '+596', 'MARTINIQUAISE', 1),
(135, 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', '+389', 'MACEDONIAN', 1),
(136, 'MALI', '+223', 'MALIAN', 1),
(137, 'MYANMAR', '+95', 'BURMESE', 1),
(138, 'MONGOLIA', '+976', 'MONGOLIAN', 1),
(139, 'MACAU', '+853', 'MACANESE', 1),
(140, 'NORTHERN MARIANA ISLANDS', '+1670', 'NORTHERN MARIANANS', 1),
(141, 'MAURITANIA', '+222', 'MAURITANIAN', 1),
(142, 'MONTSERRAT', '+1664', 'MONTSERRATIAN', 1),
(143, 'MALTA', '+356', 'MALTESE', 1),
(144, 'MAURITIUS', '+230', 'MAURITANIAN', 1),
(145, 'MALDIVES', '+960', 'MALDIVAN', 1),
(146, 'MALAWI', '+265', 'MALAWIAN', 1),
(147, 'MEXICO', '+52', 'MEXICAN', 1),
(148, 'MALAYSIA', '+60', 'MALAYSIAN', 1),
(149, 'MOZAMBIQUE', '+258', 'MOZAMBICAN', 1),
(150, 'NAMIBIA', '+264', 'NAMIBIAN', 1),
(151, 'NEW CALEDONIA', '+687', 'NEW CALEDONIAN', 1),
(152, 'NIGER', '+227', 'NIGERIEN', 1),
(153, 'NIGERIA', '+234', 'NIGERIAN', 1),
(154, 'NICARAGUA', '+505', 'NICARAGUAN', 1),
(155, 'NETHERLANDS', '+31', 'DUTCH', 1),
(156, 'NORWAY', '+47', 'NORWEGIAN', 1),
(157, 'NEPAL', '+977', 'NEPALESE', 1),
(158, 'NAURU', '+674', 'NAURUAN', 1),
(159, 'NIUE', '+683', 'NIUEAN', 1),
(160, 'NEW ZEALAND', '+64', 'NEW ZEALANDER', 1),
(161, 'OMAN', '+968', 'OMANI', 1),
(162, 'PALESTINE', '+970', 'PALESTINIAN', 1),
(163, 'PANAMA', '+507', 'PANAMANIAN', 1),
(164, 'PERU', '+51', 'PERUVIAN', 1),
(165, 'FRENCH POLYNESIA', '+689', 'FRENCH POLYNESIAN', 1),
(166, 'PAPUA NEW GUINEA', '+675', 'PAPUA NEW GUINEAN', 1),
(167, 'PHILIPPINES', '+63', 'FILIPINO', 1),
(168, 'PAKISTAN', '+92', 'PAKISTANI', 1),
(169, 'POLAND', '+48', 'POLISH', 1),
(170, 'SAINT PIERRE AND MIQUELON', '+508', 'SAINT-PIERRAISE', 1),
(171, 'PITCAIRN ISLAND', '+870', 'PITCAIRN ISLANDER', 1),
(172, 'PUERTO RICO', '+1', 'PUERTO RICAN', 1),
(173, 'PORTUGAL', '+351', 'PORTUGUESE', 1),
(174, 'PALAU', '+680', 'PALAUAN', 1),
(175, 'PARAGUAY', '+595', 'PARAGUAYAN', 1),
(176, 'QATAR', '+974', 'QATARI', 1),
(177, 'ROMANIA', '+40', 'ROMANIAN', 1),
(178, 'REUNION', '+262', 'REUNIONESE', 1),
(179, 'SERBIA', '+381', 'SERBIAN', 1),
(180, 'RUSSIAN FEDERATION', '+7', 'RUSSIAN', 1),
(181, 'RWANDA', '+250', 'RWANDAN', 1),
(182, 'SAUDI ARABIA', '+966', 'SAUDI', 1),
(183, 'SOLOMON ISLANDS', '+677', 'SOLOMON ISLANDER', 1),
(184, 'SEYCHELLES', '+248', 'SEYCHELLOIS', 1),
(185, 'SUDAN', '+249', 'SUDANESE', 1),
(186, 'SWEDEN', '+46', 'SWEDISH', 1),
(187, 'SINGAPORE', '+65', 'SINGAPOREAN', 1),
(188, 'SAINT HELENA', '+290', 'SAINT HELENIAN', 1),
(189, 'SLOVENIA', '+386', 'SLOVENIAN', 1),
(190, 'SLOVAKIA', '+421', 'SLOVAKIAN', 1),
(191, 'SIERRA LEONE', '+232', 'SIERRA LEONEAN', 1),
(192, 'SAN MARINO', '+378', 'SAN MARINESE', 1),
(193, 'SENEGAL', '+221', 'SENEGALESE', 1),
(194, 'SOMALIA', '+252', 'SOMALI', 1),
(195, 'SURINAME', '+597', 'SURINAMER', 1),
(196, 'SAO TOME AND PRINCIPE', '+239', 'SAO TOMEAN', 1),
(197, 'EL SALVADOR', '+503', 'EL SALVADORIAN', 1),
(198, 'SYRIAN ARAB REPUBLIC', '+963', 'SYRIAN', 1),
(199, 'SWAZILAND', '+268', 'SWAZI', 1),
(200, 'TURKS AND CAICOS ISLANDS', '+1649', 'TURKS AND CAICOS ISLANDERS', 1),
(201, 'CHAD', '+235', 'CHADIAN', 1),
(202, 'TOGO', '+228', 'TOGOLESE', 1),
(203, 'THAILAND', '+66', 'THAI', 1),
(204, 'TAJIKISTAN', '+992', 'TAJIK', 1),
(205, 'TOKELAU', '+690', 'TOKELAUAN', 1),
(206, 'TIMOR-LESTE', '+670', 'EAST TIMORESE', 1),
(207, 'TURKMENISTAN', '+993', 'TURKMEN', 1),
(208, 'TUNISIA', '+216', 'TUNISIAN', 1),
(209, 'TONGA', '+676', 'TONGAN', 1),
(210, 'TURKEY', '+90', 'TURK', 1),
(211, 'TRINIDAD AND TOBAGO', '+1868', 'TRINIDADIAN OR TOBAGONIAN', 1),
(212, 'TUVALU', '+688', 'TUVALUAN', 1),
(213, 'TAIWAN, PROVINCE OF CHINA', '+886', 'TAIWANESE', 1),
(214, 'TANZANIA, UNITED REPUBLIC OF', '+255', 'TANZANIAN', 1),
(215, 'UKRAINE', '+380', 'UKRAINIAN', 1),
(216, 'UGANDA', '+256', 'UGANDAN', 1),
(217, 'UNITED STATES OF AMERICA', '+1', 'AMERICAN', 1),
(218, 'URUGUAY', '+598', 'URUGUAYAN', 1),
(219, 'UZBEKISTAN', '+998', 'UZBEKISTANI', 1),
(220, 'HOLY SEE (VATICAN CITY STATE)', '+39', 'VATICAN CITIZEN', 1),
(221, 'SAINT VINCENT AND THE GRENADINES', '+1784', 'SAINT VICENTIAN', 1),
(222, 'VENEZUELA', '+58', 'VENEZUELAN', 1),
(223, 'VIRGIN ISLANDS, BRITISH', '+1284', 'BRITISH VIRGIN ISLANDER', 1),
(224, 'VIRGIN ISLANDS, U.S.', '+1340', 'U.S VIRGIN ISLANDER', 1),
(225, 'VIETNAM', '+84', 'VIETNAMESE', 1),
(226, 'VANUATU', '+678', 'NI-VANUATU', 1),
(227, 'WALLIS AND FUTUNA', '+681', 'WALLISIAN', 1),
(228, 'WESTERN SAHARA', '+212', 'SAHRAWIS', 1),
(229, 'SAMOA', '+685', 'SAMOAN', 1),
(230, 'KOSOVO', '+381', 'KOSOVAR', 1),
(231, 'YEMEN', '+967', 'YEMENI', 1),
(232, 'MAYOTTE', '+262', 'MAHORAN', 1),
(233, 'SOUTH AFRICA', '+27', 'SOUTH AFRICAN', 1),
(234, 'ZAMBIA', '+260', 'ZAMBIAN', 1),
(235, 'ZIMBABWE', '+263', 'ZIMBABWEAN', 1);

-- --------------------------------------------------------

--
-- Table structure for table `unisoft_events_and_info`
--

CREATE TABLE `unisoft_events_and_info` (
  `id` int(11) NOT NULL,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `entry_by` int(11) NOT NULL,
  `entry_date` datetime NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `unisoft_income_and_expenses`
--

CREATE TABLE `unisoft_income_and_expenses` (
  `id` int(11) NOT NULL,
  `transaction_date` date NOT NULL,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `player_id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `unisoft_players`
--

CREATE TABLE `unisoft_players` (
  `player_id` int(11) NOT NULL,
  `first_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `sex` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nationality` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `marital_status` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `height` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `passport_no` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `preferred_position` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_position` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `current_club` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contract_expiration` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level_played` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `registration_type` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `playing_foot` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `health` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `player_profile` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `right_foot` float NOT NULL,
  `left_foot` float NOT NULL,
  `speed` float NOT NULL,
  `athleticism` float NOT NULL,
  `tactics` float NOT NULL,
  `hard_work` float NOT NULL,
  `teamwork` float NOT NULL,
  `adaptability` float NOT NULL,
  `personality` float NOT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `other_phone` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `player_number` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contract` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_joined` date NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `status_date` date NOT NULL,
  `status_comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `entry_by` int(11) NOT NULL,
  `entry_date` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `unisoft_players_achievements`
--

CREATE TABLE `unisoft_players_achievements` (
  `id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `entry_by` int(11) NOT NULL,
  `entry_date` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `unisoft_players_career_path`
--

CREATE TABLE `unisoft_players_career_path` (
  `id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `period` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `club` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `matches` int(11) NOT NULL,
  `goals` int(11) NOT NULL,
  `club_level` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entry_by` int(11) NOT NULL,
  `entry_date` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `unisoft_users`
--

CREATE TABLE `unisoft_users` (
  `id` int(10) NOT NULL,
  `photo` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sex` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `unisoft_users`
--

INSERT INTO `unisoft_users` (`id`, `photo`, `name`, `sex`, `role`, `username`, `password`, `status`) VALUES
(1, '', 'Administrator', 'Male', 1, 'admin', '$2y$10$0rzv0HEFJx.ZMHWuK5YM2.ZbEz3WmMbGi8n6qBIlx1BHdpfUmEzRy', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `unisoft_agents`
--
ALTER TABLE `unisoft_agents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unisoft_agents_players`
--
ALTER TABLE `unisoft_agents_players`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unisoft_countries`
--
ALTER TABLE `unisoft_countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unisoft_events_and_info`
--
ALTER TABLE `unisoft_events_and_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unisoft_income_and_expenses`
--
ALTER TABLE `unisoft_income_and_expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unisoft_players`
--
ALTER TABLE `unisoft_players`
  ADD PRIMARY KEY (`player_id`);

--
-- Indexes for table `unisoft_players_achievements`
--
ALTER TABLE `unisoft_players_achievements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unisoft_players_career_path`
--
ALTER TABLE `unisoft_players_career_path`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unisoft_users`
--
ALTER TABLE `unisoft_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_name` (`username`),
  ADD KEY `user_role_id` (`role`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `unisoft_agents`
--
ALTER TABLE `unisoft_agents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `unisoft_agents_players`
--
ALTER TABLE `unisoft_agents_players`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `unisoft_countries`
--
ALTER TABLE `unisoft_countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=236;

--
-- AUTO_INCREMENT for table `unisoft_events_and_info`
--
ALTER TABLE `unisoft_events_and_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `unisoft_income_and_expenses`
--
ALTER TABLE `unisoft_income_and_expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `unisoft_players`
--
ALTER TABLE `unisoft_players`
  MODIFY `player_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `unisoft_players_achievements`
--
ALTER TABLE `unisoft_players_achievements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `unisoft_players_career_path`
--
ALTER TABLE `unisoft_players_career_path`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `unisoft_users`
--
ALTER TABLE `unisoft_users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

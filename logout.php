<?php

$__logout = true;
require __DIR__.'/includes/partials/config.php';

session_destroy();
setcookie(LOG_LOGIN_KEY, false, time()-1, DOMAIN);
setcookie(LOG_LOGOUT_KEY, true, time()+60*30, DOMAIN);

\CSRF_TOKEN::destroy();

header('Location: '.DOMAIN);
die();

<?php

require __DIR__.'/includes/partials/temporary_login.php';


if( TEMPORARY\LOGIN\temporary_login() ){
    $_g_file = $_GET['__u'] ?? '';
    $g_user_role = $_SESSION[USER_DETS_KEY]['role'];

    if($_g_file === '/' || $_g_file === ''){
        require __DIR__.'/includes/pages/index.php';
    }else if($_g_file == 'events'){
        require __DIR__.'/includes/pages/events.php';
    }else if($_g_file == 'add_player' && in_array($g_user_role, ['1', '2'])){
        require __DIR__.'/includes/pages/add_player.php';
    }else if($_g_file == 'players' && in_array($g_user_role, ['1', '2'])){
        require __DIR__.'/includes/pages/players.php';
    }else if($_g_file == 'players/player' && in_array($g_user_role, ['1', '2'])){
        require __DIR__.'/includes/pages/player.php';
    }else if($_g_file == 'cv' && in_array($g_user_role, ['1', '2'])){
        require __DIR__.'/includes/pages/cv.php';
    }else if($_g_file == 'users' && in_array($g_user_role, ['1', '2'])){
        require __DIR__.'/includes/pages/users.php';
    }else if($_g_file == 'users' && in_array($g_user_role, ['1', '2'])){
        require __DIR__.'/includes/pages/users.php';
    }else if($_g_file == 'agents' && in_array($g_user_role, ['1', '2'])){
        require __DIR__.'/includes/pages/agents.php';
    }else if($_g_file == 'income_and_expenses' && in_array($g_user_role, ['1', '2'])){
        require __DIR__.'/includes/pages/income_and_expenses.php';
    }elseif($_g_file == 'evaluation_report' && in_array($g_user_role, ['1', '2'])){
        require __DIR__.'/includes/pages/evaluation_report.php';
    }else{
        http_response_code(404);
        require __DIR__.'/http_error.php';
    }
}else{
    require __DIR__.'/includes/pages/login.php';
}

?>
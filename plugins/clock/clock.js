(function($) {
  	$.fn.unstClock = function(options) {
	    var options = $.extend({
	    	timestamp: Date.now()
	    }, options);


	    let $this = this;

	    setInterval(function() {
	    	options.timestamp = options.timestamp + 1000;

			let date 	= new Date(options.timestamp);
			let time    = date.getTime();
			let hours   = date.getHours();
			let minutes = date.getMinutes();
			let seconds = date.getSeconds();
			let ampm 	= (hours < 12) ? " pm" : " pm";

			hours    = (hours > 12) ? hours - 12 : hours;
			hours    = (hours < 10) ? "0"+hours : hours;
			minutes  = (minutes < 10) ? "0"+minutes : minutes;
			seconds  = (seconds < 10) ? "0"+seconds : seconds;

			$this.html(`
				<ul class="unst-time">
					<li>${hours}</li><li>:</li><li>${minutes}</li><li>:</li><li>${seconds}</li>
					<li> ${ampm}</li>
				</ul>
			`);
	    }, 1000);

	    return this;
	};
}(jQuery));
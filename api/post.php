<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
require dirname(__DIR__, 1).'/includes/classes/evaluation.class.php';
if( isset($_GET['p']) ){
	require dirname(__DIR__).'/includes/pages/'.urldecode($_GET['p']).'.req.php';
}

if(isset($_GET['all_player_data'])){
    $eval = new evaluation(Null);
    $allplayers['list'] = $eval->get_all_players();
    echo json_encode($allplayers);
    return;
}

if(isset($_GET['player_data'])){
    $player_id = $_GET['player_data'];
    if(!is_numeric($player_id)){
        echo json_encode($player_details['profile'] =[]);
    }else{
        $player_id = (int) $player_id;
        $eval = new evaluation("{$player_id}");
        $player_details= $eval->get_player_details();
        echo json_encode($player_details);
    }
    return;
}

if(isset($_GET['eval_data'])){
    $eval_id = $_GET['eval_data'];
    if(!is_numeric($eval_id)){
        echo json_encode($evaluation['eval'] =[]);
    }else{
        $eval_id = (int) $eval_id;
        $eval = new evaluation("{$eval_id}");
        $eval_details = $eval->get_player_report();
        echo json_encode($eval_details);
    }
    return;
}

if(isset($_POST)){
    //    public $evaluation_tables = [
    //        'attitude'          => 'unisoft_attitude_eval',
    //        'coexistence'       => 'unisoft_coexistence',
    //        'header'            => 'unisoft_evaluation_header',
    //        'technical'         => 'unisoft_technical_eval',
    //        'tactical'          => 'unisoft_tactical_eval',
    //        'profile'           => 'unisoft_player_profile',
    //        'psycho'            => 'unisoft_physical_psycho',
    //        'notes'             => 'unisoft_notes',
    //        'player'            => 'unisoft_players'
    //    ];

     $_POST = json_decode(file_get_contents('php://input'), true);

    //you can mysql real escape string if you like
    //feeling lazy to loop through you can do that by yourself if you want to
         $data_technical = [
            'ball_skills' => $_POST['postData']['ball_skills'],
            'heading' => $_POST['postData']['heading'],
            'pass' => $_POST['postData']['pass'],
            'dribble' => $_POST['postData']['dribble'],
            'carrying_the_ball' => $_POST['postData']['carrying_the_ball'],
            'control' => $_POST['postData']['control'],
            'shooting' => $_POST['postData']['shooting'],
            'skills' => $_POST['postData']['skills'],
         ];

         $data_tactical = [
            'positioning_on_the_field' => $_POST['postData']['positioning_on_the_field'],
            'combination_game' => $_POST['postData']['combination_game'],
            'decision_making' => $_POST['postData']['decision_making'],
            'offensive_tactics_overall_evaluation' => $_POST['postData']['offensive_tactics_overall_evaluation'],
            'defensive_tactics_overall_evaluation' => $_POST['postData']['defensive_tactics_overall_evaluation'],
         ];

         $data_psycho = [
             'strength' => $_POST['postData']['strength'],
             'speed' => $_POST['postData']['speed'],
             'endurance' => $_POST['postData']['endurance'],
             'flexibility' => $_POST['postData']['flexibility'],
             'coordination' => $_POST['postData']['coordination'],
             'laterality' => $_POST['postData']['laterality'],
             'agility' => $_POST['postData']['agility'],
         ];

         $data_profile = [
             'technical_average' => $_POST['postData']['technical_average'],
             'tactical_average' => $_POST['postData']['tactical_average'],
             'physical_psychomotor_average' => $_POST['postData']['physical_psychomotor_average'],
             'attitude_average' => $_POST['postData']['attitude_average'],
             'coexistence_average' => $_POST['postData']['coexistence_average']
         ];

         $data_attitude = [
             'concentration' => $_POST['postData']['concentration'],
             'selfishness' => $_POST['postData']['selfishness'],
             'discipline' => $_POST['postData']['discipline'],
             'self_confidence' => $_POST['postData']['self_confidence'],
             'solidarity_comradeship' => $_POST['postData']['solidarity_comradeship']
         ];

         $data_coexistence = [
             'tidiness_cleanliness' => $_POST['postData']['tidiness_cleanliness'],
             'relationship_with_the_team' => $_POST['postData']['relationship_with_the_team'],
             'manners_respect' => $_POST['postData']['manners_respect'],
             'personal_care_and_hygiene' => $_POST['postData']['personal_care_and_hygiene'],
         ];

         $data_notes = [
             'notes' => $_POST['notes']
         ];

         $data_header = [
             'coach_name' => $_POST['coach_name'],
             'date' => $_POST['date'],
             'category' => $_POST['category'],
             'position' => $_POST['position']
//             'profile_photo' => $_POST['profile_photo'],
         ];



        $eval = new evaluation(Null);
        //do technical queries
        $update = $eval->update_player_evaluation($_POST['player_id'], $eval->evaluation_tables['technical'], $data_technical);
        if(!$update){
            $new_data = array_merge($data_technical, ['player_id' => $_POST['player_id']]);
            $create = $eval->create_player_evaluation($eval->evaluation_tables['technical'], $new_data);
        }


        // do tactical queries
        $update = $eval->update_player_evaluation($_POST['player_id'], $eval->evaluation_tables['tactical'], $data_tactical);
        if(!$update){
            $new_data = array_merge($data_tactical, ['player_id' => $_POST['player_id']]);
            $create = $eval->create_player_evaluation($eval->evaluation_tables['tactical'], $new_data);
        }


        //do psycho queries
        $update = $eval->update_player_evaluation($_POST['player_id'], $eval->evaluation_tables['psycho'], $data_psycho);
        if(!$update){
            $new_data = array_merge($data_psycho, ['player_id' => $_POST['player_id']]);
            $create = $eval->create_player_evaluation($eval->evaluation_tables['psycho'], $new_data);
        }

        //do player profile queries
        $update = $eval->update_player_evaluation($_POST['player_id'], $eval->evaluation_tables['profile'], $data_profile);
        if(!$update){
            $new_data = array_merge($data_profile, ['player_id' => $_POST['player_id']]);
            $create = $eval->create_player_evaluation($eval->evaluation_tables['profile'], $new_data);
        }

        //do attitude queries
        $update = $eval->update_player_evaluation($_POST['player_id'], $eval->evaluation_tables['attitude'], $data_attitude);
        if(!$update){
            $new_data = array_merge($data_attitude, ['player_id' => $_POST['player_id']]);
            $create = $eval->create_player_evaluation($eval->evaluation_tables['attitude'], $new_data);
        }


        //do coexistence queries
        $update = $eval->update_player_evaluation($_POST['player_id'], $eval->evaluation_tables['coexistence'], $data_coexistence);
        if(!$update){
            $new_data = array_merge($data_coexistence, ['player_id' => $_POST['player_id']]);
            $create = $eval->create_player_evaluation($eval->evaluation_tables['coexistence'], $new_data);
        }


        //do header queries
//        $update = $eval->update_player_evaluation($_POST['player_id'], $eval->evaluation_tables['header'], $data_header);
//        if(!$update){
//            $new_data = array_merge($data_header, ['player_id' => $_POST['player_id']]);
//            $create = $eval->create_player_evaluation($eval->evaluation_tables['header'], $new_data);
//        }
        //do notes queries
//        $update = $eval->update_player_evaluation($_POST['player_id'], $eval->evaluation_tables['notes'], $data_notes);
//        if(!$update){
//            $new_data = array_merge($data_notes, ['player_id' => $_POST['player_id']]);
//            $create = $eval->create_player_evaluation($eval->evaluation_tables['notes'], $new_data);
//        }

        echo json_encode(['status' => true, 'message' => 'Success']);
}
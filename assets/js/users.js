$(document).ready(function(){
    const reqUrl = DOMAIN+'api/post?p=requests%2Fusers';
	prefix = 'add_user_';

    $('#add-new-user').on('click', function(){
        ele('form')[0].reset();
        $('#add-user-modal').modal('show');
    });

    ele('change_password').on('change', function(){
        if( $(this).is(':checked') ){
            $(this).parent().hide();
            $('#password-holder').show();
        }else{
            $(this).parent().show();
            $('#password-holder').hide();
        }
    });

    $('.edit-user').on('click', function(){
        let data = users[$(this).data('idx')];

        ele('form')[0].reset();
        $('#action-title').text('edit');
        $('#action-reset').text('Cancel').attr('data-dismiss', 'modal');
        $('#__add_user').val('edit');
        ele('change_password').prop('checked', false).trigger('change');

        ele('id').val(data.id);
        ele('name').val(data.name);
        ele('sex_'+data.sex.toLowerCase()).prop('checked', true).trigger('click');
        ele('role').val(data.role).trigger('change').removeClass('is-valid');
        ele('username').val(data.username).attr('disabled', 'disabled');
        ele('status_'+data.status.toLowerCase()).prop('checked', true).trigger('click');

        $('#add-user-modal').modal('show');
    });

    /* --- validate on keyup, blur, change --- */
    ele('name').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: true});
    });

    radioEle('sex').on('change', function(){
        radioEle('sex').VALIDATE({type: "radio", required: true, rName: 'sex', rOpts: ['Male', 'Female']});
    });

    ele('role').on('change select2:close', function(){
        $(this).VALIDATE({type: "", required: true});
    });

    radioEle('status').on('change', function(){
        radioEle('status').VALIDATE({type: "radio", required: true, rName: 'status', rOpts: ['1', '0']});
    });

    ele('username').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: true});
    });

    ele('password').on('keyup blur', function(){
        $(this).VALIDATE({type: "password", required: true});
        ele('confirm_password').VALIDATE({type: "confirm_password", passVal: $(this).val()});
    });

    ele('confirm_password').on('keyup blur', function(){
        $(this).VALIDATE({type: "confirm_password", passVal: ele('password').val()});
    });


    /* ****************************
     form submission section
    **************************** */
    ele('form').on('submit', function(e){
		e.preventDefault();

        let $this = $(this), errorBag = [], password = ele('password');

        errorBag.push(ele('name').VALIDATE({type: "", required: true}));
        errorBag.push(radioEle('sex').VALIDATE({type: "radio", required: true, rName: 'sex', rOpts: ['Male', 'Female']}));
        errorBag.push(ele('role').VALIDATE({type: "", required: true}));
        errorBag.push(radioEle('status').VALIDATE({type: "radio", required: true, rName: 'status', rOpts: ['1', '0']}));
        errorBag.push(ele('username').VALIDATE({type: "", required: true}));

        if( ele('change_password').is(':checked') ){
            errorBag.push(password.VALIDATE({type: "password", required: true}));
            errorBag.push(ele('confirm_password').VALIDATE({type: "confirm_password", passVal: password.val()}));
        }

        if( errorBag.indexOf(false) == -1 ){
            let xhrData = new FormData($this[0]);

            let sucCb = function(res){
                if( res.success == 'success' ){
                    notify('Record successfully saved', 'success');
                    $('#add-user-modal').modal('hide');
                    $this[0].reset();
                    setTimeout(function(){
                        window.location.reload();
                    }, 2000);
                }else if( res.success == 'error' ){
                    for(let key in res.errors){
                        displayError(key, res.errors[key]);
                    }

                    notify("An error occurred. Please review your entries and save again", 'error');
                }else{
                    notifyAjaxError();
                }
            };

            ajaxRequest(reqUrl, false, xhrData, sucCb, null, null, '1');
        }else{
            notify("An error occurred. Please review your entries and save again", 'error');
        }
	});

    ele('form').on('reset', function(){
        $('#action-title').text('add new');
        $('#action-reset').text('Reset').removeAttr('data-dismiss');
        $('#__add_user').val('add new');
        ele('change_password').prop('checked', true).trigger('change');

        ele('id').val('');
        ele('username').removeAttr('disabled');
        ele('form select').val('').trigger('change');
        ele('form .invalid-feedback').text('');
        ele('form .is-invalid').removeClass('is-invalid');
        ele('form .is-valid').removeClass('is-valid');
    });
});
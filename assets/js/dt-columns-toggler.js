function hiddenColumns(name, type='', cols=[]){
    name = name + '-' + 'hiddenColumns';
    cols = $.isArray(cols) ? cols : cols.toString().split(',');

    if( localStorage ){
        try{
            let g_cols = localStorage[name] ? localStorage[name].split(',') : [];

            if( type == 'add' ){
                g_cols = g_cols.concat(cols);
            }else if( type == 'delete' ){
                for(let i=0; i<cols.length; i++){
                    let idx = g_cols.indexOf(cols[i]);

                    if( idx != -1 ) g_cols.splice(idx, 1);
                }
            }else if( type == 'clear' ){
                g_cols = [];
            }else{
                cols = g_cols;
            }

            localStorage[name] = g_cols.join(',');
        }catch(e){
            console.log(e);
        }
    }else{
        alert('Your browers is very old. Please update your browser');
    }

    return cols;
}

function hiddenColumnsAction(name){
    let hiddenColsIndexs = [];

    try{
        let hiddenCols = hiddenColumns(name);
        
        $('[data-toggle-col-vis="'+name+'"] [data-toggle-col-vis-items] input').each(function(dtIdx){
            if( hiddenCols.indexOf( $(this).data('dt-col') ) != -1 ){
                hiddenColsIndexs.push(dtIdx);
                $(this).prop('checked', false);
            }
        });
    }catch(e){
        console.log(e);
    }

    return hiddenColsIndexs;
}

$(document).ready(function(){
    $('[data-toggle-col-vis]').on('input', '[data-toggle-col-vis-filter]', function(){
        let parent   = $(this).closest('[data-toggle-col-vis]');
        let items    = parent.find('[data-toggle-col-vis-items]');
        let searched = $(this).val().toLowerCase();

        items.show();

        if( searched != '' ){
            items.each(function(){
                let item = $(this);
                let text = item.text().toLowerCase();

                if( text.indexOf(searched) == -1 ){
                    item.hide();
                }
            });
        }
    }).on('change', '[data-toggle-col-vis-items] input', function(){
        let parent   = $(this).closest('[data-toggle-col-vis]');
        let hdcNames = parent.data('toggle-col-vis'); // hidden columns name
        let target   = parent.data('ctarget');
        let col      = $(this).data('dt-col'); // can be index or name eg 0 or student_id
        let colExt   = $(this).data('col-ext'); // can be :name or ''
        let column   = $(target).DataTable().column(col + colExt);

        column.visible(!column.visible());
        hiddenColumns(hdcNames, 'delete', col);

        if( !$(this).is(':checked') ){
            hiddenColumns(hdcNames, 'add', col);
        }
    }).on('click', '[data-check]', function(){
        let parent     = $(this).closest('[data-toggle-col-vis]');
        let checkboxes = parent.find('[data-toggle-col-vis-items] input');
        let hdcNames   = parent.data('toggle-col-vis'); // hidden columns name
        let target     = parent.data('ctarget');
        let showAll    = parseInt($(this).data('check'));
        
        $(target).DataTable().columns().visible(showAll);
        checkboxes.prop('checked', showAll);
        hiddenColumns(hdcNames, 'clear');

        if( !showAll ){
            let cols = [];
            checkboxes.each(function(){ cols.push($(this).data('dt-col')); });
            hiddenColumns(hdcNames, 'add', cols);
        }
    });
});
var xhr = null;
var prefix = '';

console.log("%cWARNING!", "color: red; font-size: 100px; margin: 0px;text-shadow: 5px 5px 0 yellow;");
console.log("%cThis is a browser feature intended for developers. If someone told you to copy-paste something here it is a scam and can give them access to this system.", "font-size: large");
console.log("%cRegards "+$('meta[name="company-name"]').attr('content'), "font-size: medium; color: blue;font-weight: bolder;");

$(window).on('beforeunload', function(){
	if( xhr != null ) xhr.abort();
});

var CSRF_TOKEN = $('meta[name="CSRF-TOKEN"]').attr('content');
$.ajaxSetup({
    headers: {'X-CSRF-TOKEN': CSRF_TOKEN}
});

function ele(name){
    // let eles = name.split(',').map(function(el){return '#'+prefix+el.trim();});
    // return $(eles.join(','));

    return $('#'+prefix+name);
}

function radioEle(name){
    return $('input[type="radio"][name="'+prefix+name+'"]');
}

function unstLoader(status='off'){ 
	let loader = $('body').find('.unst-loader');

   	if( (status == 'on') && !loader.length ){
		$('body').append('<div class="unst-loader"></div>');

		$(':focus').trigger('blur');
   	}else if( (status == 'off') && loader.length ){
   		loader.remove();
   	}
}

function unstLoader2(status='off', msg='Please wait...'){
	let loader = $('body').find('.unst-loader2-wrapper');

   	if( (status == 'on') && !loader.length ){
		$('body').append(`
   			<div class="unst-loader2-wrapper">
   				<span class="unst-loader2">${msg}</span>
   			</div>
		`);

		$(':focus').trigger('blur');
   	}else if( (status == 'off') && loader.length ){
   		loader.remove();
   	}
}

function escapeHtml(text){
	let map = {
		'&': '&amp;',
		'<': '&lt;',
		'>': '&gt;',
		'"': '&quot;',
		"'": '&#039;',
		"`": "&#x60;"
	}

	return text.replace(/[&<>"'`]/g, function(key){ return map[key]; });
}

function numberFormat(num, dp=0){
	num = !isNaN(num) ? parseFloat(num) : 0;
	return num.toFixed(dp).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
}

function notify(text, type, timer=3000, heading=''){
	$.toast({
        heading: heading,
        text: text,
        icon: type,
        loader: true,
        loaderBg: type == 'error' ? '#57c7d4' : '#f96868',
        hideAfter: timer,
        position: 'top-right',
        stack: false
    });
}

function ajaxError(res){
	if( typeof res !== 'undefined' && res.hasOwnProperty('success') && res.success == '__refresh__' ){
		window.location.reload();
	}else{
		return false;
	}
};

function notifyAjaxError(){
	let msg = `Oops! An Error Occurred 
			This may be due to a <b class="text-warning">poor network connection</b> or 
			<b class="text-warning">an error on the server</b>. 
			Kindly check your network connection and try again or contact the system administrator for assistance. Thanks`;

	notify(msg, 'error', 10000, 'Error');
}

function ajaxRequest(url, t=true, xhrData, sucCb=null, compCb=null, errCb=null, loader='2', _notifyAjaxError=true){
    switch(loader){
        case '1': unstLoader('on'); break;
        case '2': unstLoader2('on'); break;
        default: break;
    }

    xhr = $.ajax({
        type: 'POST',
        url: url,
        processData: t,
        contentType: t ? 'application/x-www-form-urlencoded; charset=UTF-8' : false,
        cache: t,
        data: xhrData,
        dataType: 'json',
        beforeSend: function(){
            if( xhr != null ) xhr.abort();
        },
        success: function(data){
            let result = data;

            if( ajaxError(result) !== false ){
                return;
            }else{
                if( sucCb != null ) sucCb(result);
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            if( textStatus != 'abort' && _notifyAjaxError ) notifyAjaxError();
            if( errCb != null ) errCb();
        },
        complete: function(jqXHR, textStatus){
            xhr = null;
            switch(loader){
                case '1': unstLoader('off'); break;
                case '2': unstLoader2('off'); break;
                default: break;
            }

            if( compCb != null ) compCb();
        }
    });
}

$(document).ready(function() {
	"use strict";

    $('form.top-bar-search').on('submit', function(e){
        e.preventDefault();

        let value = $(this).find('input').val().trim();

        if( value != '' ){
            document.cookie = 'search='+encodeURIComponent(value)+';path='+DOMAIN;
            document.location.href=DOMAIN+"players";
        }else{
            // this field is required
        }
    });

    $('body').on('click', '.scroll-btn', function(){
        $($(this).data('ctarget')).stop().animate({
            scrollLeft: $(this).data('scroll')
        });
    });

    $('body').on('focus', 'input[type="number"].disabled-spin-button', function(e){
        $(this).on('wheel.disableScroll', function(e){
            e.preventDefault();
        });
    }).on('blur', 'input[type="number"].disabled-spin-button', function(e){
        $(this).off('wheel.disableScroll', function(e){
            e.preventDefault();
        });
    });
});
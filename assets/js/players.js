$(document).ready(function(){
    const reqUrl = DOMAIN+'api/post?p=requests%2Fplayers';
    prefix = 'get_players_';

    if( localStorage.getItem('players-hiddenColumns') == null ) localStorage.setItem('players-hiddenColumns', $db_columns_default_hidden);

    var hiddenCols       = hiddenColumnsAction('players');
    var __searched_value = decodeURIComponent(Cookies.get('search'));
    if( __searched_value != 'undefined' ){
        Cookies.remove('search', { path: DOMAIN });
    }

	var t = $('#players-table').DataTable({
        dom: "<'text-muted d-flex justify-content-between mb-1'fi><'#players-table-wrapper.table-responsive hide-scroll border-left border-right'tr><'mt-4 d-flex justify-content-center'p>",
		language: {
			emptyTable: 'No record found',
			paginate: {
				previous: '<i class="fa fa-angle-left"></i>',
				next: '<i class="fa fa-angle-right"></i>'
			}
		},
		autoWidth: false,
        processing: true,
        deferRender: true,
        "ajax": {
            type: 'POST',
            url: reqUrl,
            data: {__get_players: true},
            beforeSend: function(){
                if( typeof t != 'undefined' && t.settings()[0].jqXHR != null ){
                    t.settings()[0].jqXHR.abort();
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                if( textStatus != 'abort' ){
                    notify(`Couldn't load table content. An error occurred`, 'error');
                    $('#players-table_processing').hide();
                    t.clear().draw();
                }
            }
        },
        "initComplete": function(settings, json){},
        "createdRow": function(row, data, index){},
        "search": {
            "search": __searched_value != 'undefined' ? __searched_value : ''
        },
        "columns" : $dt_columns,
        "columnDefs": [{
            "searchable": false,
            "orderable": false,
            "targets": 0,
            render: function(data, type, row, meta){
                if( type == 'display' ){
            	    data = `<a href="${DOMAIN+'players/player?h='+row.hash+'&__='+row.player_id+'&_token='+CSRF_TOKEN}" class="btn btn-sm btn-icon btn-secondary"><i class="fa fa-list"></i></a>
                            <button type="button" class="btn btn-sm btn-icon btn-secondary delete-player"><i class="fa fa-trash-alt"></i></button>`;
                }

                return data;
            }
        },{
            "searchable": false,
            "orderable": false,
            "targets": 1,
            render: function(data, type, row, meta){
                if( type == 'display' ){
                    data = `<a href="javascript:void(0);" data-src="${row.photo}" data-fancybox data-type="image" data-caption="${row.name}"><img class="img-fluid rounded-circle player-thumb" src="${row.photo}" alt=""></a>`;
                }

                return data;
            }
        },{
            "targets": 2,
            render: function(data, type, row, meta){
                if( type == 'display' ){
                    data = `<a href="${DOMAIN+'players/player?h='+row.hash+'&__='+row.player_id+'&_token='+CSRF_TOKEN}">${row.name}</a>`;
                }

                return data;
            }
        },{
            "targets": 34,
            render: function(data, type, row, meta){
                if( type == 'display' ){
                    data = `<span class="badge px-2 rounded-0 text-light text-uppercase bg-${$player_statuses[row.status].class}">${$player_statuses[row.status].name}</span>`;
                }else{
                    data = $player_statuses[row.status].name;
                }

                return data;
            }
        },{
            "targets": hiddenCols,
            "visible": false
        }],
        "iDisplayLength": 25,
        "aLengthMenu": [
            [25, 50, 100],
            [25, 50, 100]
        ],
        "order": [[ 2, 'asc' ]]
    });


    // reload
    function reloadDataTable(){
        unstLoader2('on');
        t.ajax.reload(function(json){
            if( ajaxError(json) !== false ){
                return;
            }else{
                unstLoader2('off');
            }
        });
    }

	$('#reload-table').on('click', function(){
        reloadDataTable();
    });

    // delete player
    $('#players-table').on('click', '.delete-player', function(){
        let xhrData = {
            __delete_player: true,
            player_id: t.row($(this).closest('tr')).data()['player_id']
        };

        let sucCb = function(res){
            if( res.success == 'success' ){
                notify('Player successfully deleted', 'success');
                reloadDataTable();
            }else if( res.success == 'error' ){
                notify(res.errors.error, 'error', 7000);
            }else{
                notifyAjaxError();
            }
        }

        swal({
            title: 'Delete Player!',
            html: `
                <h6 class="mb-1 font-weight-light">Do you want to delete selected player?</h6>
                <h6 class="mb-2 font-weight-bolder font-italic">Note! All associated documents will be deleted.</h6>
                <h6 class="mb-4 font-weight-light">In order to proceed, please type the word 'delete' in the box below</h6>
                <input type="text" class="form-control form-control-lg text-center" id="delete_player" spellcheck="false" placeholder="Enter the word 'delete'">
                <hr class="mb-0">
            `,
            allowEnterKey: false,
            customClass: 'p-5',
            showCancelButton: true,
            cancelButtonText: 'Cancel',
            confirmButtonText: 'Delete!',
            confirmButtonColor: 'red',
            reverseButtons: true,
            preConfirm: function(value){
                if( $('#delete_player').val().toLowerCase() != 'delete' ){
                    Swal.showValidationMessage("Please enter the word 'delete'");
                }
            },
            onOpen: function(){
                $('#delete_player').focus();
            }
        }).then((result) => {
            if( result.value ){
                ajaxRequest(reqUrl, true, xhrData, sucCb, null, null, '1');
            }
        });
    });
});
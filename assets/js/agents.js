$(document).ready(function(){
    const reqUrl = DOMAIN+'api/post?p=requests%2Fagents';
    prefix = 'agents_';


    var t = $('#agents-table').DataTable({
        dom: "<'text-muted d-md-flex justify-content-md-between align-items-md-center'if><'#agents-table-wrapper.table-responsive'tr><'d-flex justify-content-center'p>",
        language: {
            emptyTable: 'No record found',
            paginate: {
                previous: '<i class="fa fa-angle-left"></i>',
                next: '<i class="fa fa-angle-right"></i>'
            }
        },
        autoWidth: false,
        deferRender: true,
        "initComplete": function(settings, json){},
        "createdRow": function(row, data, index){},
        "columns" : [{data:'id'},{data:'photo'},{data:'name'},{data:'passport_no'},{data:'entry_date'},{data:'expiry_date'},{data:'duration'},{data: 'status'},{data: 'status_date'},{data: 'status_comment'}],
        "columnDefs": [{
            "searchable": false,
            "orderable": false,
            "targets": 0,
            render: function(data, type, row, meta){
                if( type == 'display' ){
                    let attrs = row.mandate_file == '' ? 'disabled' : 'data-fancybox data-type="iframe" data-src="'+DOMAIN+'assets/documents/mandates/'+row.mandate_file+'" data-iframe=\'{"preload":false, "css":{"width":"900px", "height":"100%", "maxWidth":"90%", "maxHeight":"100%"}}\'';
                    data = `<button type="button" class="btn btn-sm btn-icon btn-secondary" ${attrs}><i class="fa fa-file-pdf"></i></button> <!-- <button type="button" class="btn btn-sm btn-icon btn-secondary change-status"><i class="fa fa-check-double"></i></button> --> <button type="button" class="btn btn-sm btn-icon btn-secondary delete-record"><i class="fa fa-trash-alt"></i></button>`;
                }

                return data;
            }
        },{
            "searchable": false,
            "orderable": false,
            "targets": 1,
            render: function(data, type, row, meta){
                if( type == 'display' ){
                    data = `<a href="javascript:void(0);" data-src="${row.photo}" data-fancybox data-type="image" data-caption="${row.name}"><img class="img-fluid rounded-circle player-thumb" src="${row.photo}" alt=""></a>`;
                }else{
                    data = row.name;
                }

                return data;
            }
        },{
            "targets": 2,
            render: function(data, type, row, meta){
                if( type == 'display' ){
                    data = `<a href="${DOMAIN+'players/player?h='+row.hash+'&__='+row.player_id+'&_token='+CSRF_TOKEN}">${row.name}</a>`;
                }else{
                    data = row.name;
                }

                return data;
            }
        },{
            "targets": 7,
            render: function(data, type, row, meta){
                if( type == 'display' ){
                    data = `<span class="badge px-2 rounded-0 text-light text-uppercase bg-${$agent_player_statuses[row.status].class}">${$agent_player_statuses[row.status].name}</span>`;
                }else{
                    data = $agent_player_statuses[row.status].name;
                }

                return data;
            }
        }],
        "iDisplayLength": 10,
        "aLengthMenu": [
            [10, 15, 25, 50],
            [10, 15, 25, 50]
        ],
        "order": [[ 2, 'asc' ]]
    });

    
    $('#agents').on('click', '.list-group-item', function(e){
        let $this = $(this);
        let id = $this.data('id');
        let name = $this.data('name');

        let xhrData = {
            __get_agent_details: true,
            id: id
        };

        let sucCb = function(res){
            if( res.success == 'success' ){
                $this.siblings().removeClass('active');
                $this.addClass('active');

                for(let key in res.details.details){
                    $('[data-agent-details="'+key+'"]').html(res.details.details[key]);
                }

                t.search('').clear().rows.add(res.details.players).draw();
                $('#tie_player_to_agent_agent').attr('value', id).val(id);
                $('#tie_player_to_agent_agent_name').attr('value', name).val(name);
                $('#no-agent-selected').hide();
                Looper.showSidebar();
            }else if( res.success == 'error' ){
                notify(res.errors.error, 'error');
            }else{
                notifyAjaxError();
            }
        };

        ajaxRequest(reqUrl, true, xhrData, sucCb);
    });

    //
    $('.modal [data-toggle="intltelinput"]').css('padding-left', '67px');

    // add agent
    $('#add_agent_name').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: true});
    });

    $('#add_agent_phone').on('keyup blur', function(){
        validatePhone(this);
    });

    $('#add_agent_passport_no').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    $('#add_agent_form').on('submit', function(e){
        e.preventDefault();

        let $this = $(this), errorBag = [];

        errorBag.push($('#add_agent_name').VALIDATE({type: "", required: true}));
        errorBag.push(validatePhone('#add_agent_phone'));
        errorBag.push($('#add_agent_passport_no').VALIDATE({type: "", required: false}));

        if( errorBag.indexOf(false) == -1 ){
            let xhrData = new FormData($this[0]);

            // set valid phone numbers
            $this.find('[data-toggle="intltelinput"]').each(function(){
                xhrData.set($(this).attr('name'), window.intlTelInputGlobals.getInstance(this).getNumber());
            });

            let sucCb = function(res){
                if( res.success == 'success' ){
                    notify('Record successfully saved', 'success');
                    $('#addNewAgentModal').modal('hide');
                    $this[0].reset();
                    unstLoader2('on');
                    setTimeout(function(){
                        window.location.reload();
                    }, 2000);
                }else if( res.success == 'error' ){
                    for(let key in res.errors){
                        displayError(key, res.errors[key]);
                    }

                    notify("An error occurred. Please review your entries and save again", 'error');
                }else{
                    notifyAjaxError();
                }
            };

            ajaxRequest(reqUrl, false, xhrData, sucCb, null, null, '1');
        }else{
            notify("An error occurred. Please review your entries and save again", 'error');
        }
    });

    $('#add_agent_form').on('reset', function(){
        // remove validation
        $(this).find('.invalid-feedback').text('');
        $(this).find('.is-invalid').removeClass('is-invalid');
        $(this).find('.is-valid').removeClass('is-valid');
    });

    // tie player to agent
    $('#tie-player-to-agent').on('click', function(){
        $('#tiePlayerToAgentModal').modal('show');
    });

    $("#tie_player_to_agent_player").select2({
        ajax: {
            url: reqUrl,
            type: 'post',
            dataType: 'json',
            delay: 250,
            data: function(params){
                return {
                    __get_select2_players: true,
                    search: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                    results: data.details.players,
                    pagination: {
                        more: (params.page * 15) < data.details.total
                    }
                };
            },
            cache: true
        },
        dropdownParent: $("#tie_player_to_agent_form"),
        placeholder: "",
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 3,
        templateResult: formatPlayer,
        templateSelection: function(player) {
            return player.name || player.text;
        }
    });

    function formatPlayer (player) {
        if( player.loading ) return player.text;

        return  `<div class="d-flex align-items-center">
                    <img src="${player.photo}" alt="" class="img-fluid rounded-circle select2-player-thumb mr-2 flex-shrink-0">
                    <div><h6 class="mb-0">${player.name}</h6> <p class="mb-0">${player.player_number}</p></div>
                </div>`;
    }

    $('#tie_player_to_agent_player').on('change', function(){
        $(this).VALIDATE({type: "", required: true});
    });

    $('#tie_player_to_agent_mandate_file').on('change', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    $('#tie_player_to_agent_entry_date').on('change', function(){
        $(this).VALIDATE({type: "", required: true});
    });

    $('#tie_player_to_agent_expiry_date').on('change', function(){
        $(this).VALIDATE({type: "", required: true});
    });

    $('#tie_player_to_agent_status').on('change', function(){
        $(this).VALIDATE({type: "", required: true});
    });

    $('#tie_player_to_agent_status_date').on('change', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    $('#tie_player_to_agent_status_comment').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    $('#tie_player_to_agent_form').on('submit', function(e){
        e.preventDefault();

        let $this = $(this), errorBag = [];

        errorBag.push($('#tie_player_to_agent_player').VALIDATE({type: "", required: true}));
        errorBag.push($('#tie_player_to_agent_mandate_file').VALIDATE({type: "", required: false}));
        errorBag.push($('#tie_player_to_agent_entry_date').VALIDATE({type: "", required: true}));
        errorBag.push($('#tie_player_to_agent_expiry_date').VALIDATE({type: "", required: true}));
        errorBag.push($('#tie_player_to_agent_status').VALIDATE({type: "", required: true}));
        errorBag.push($('#tie_player_to_agent_status_date').VALIDATE({type: "", required: false}));
        errorBag.push($('#tie_player_to_agent_status_comment').VALIDATE({type: "", required: false}));

        if( errorBag.indexOf(false) == -1 ){
            let xhrData = new FormData($this[0]);

            let sucCb = function(res){
                if( res.success == 'success' ){
                    notify('Record successfully saved', 'success');
                    $('#tiePlayerToAgentModal').modal('hide');
                    $this[0].reset();
                    t.rows.add(res.details.player).draw();
                }else if( res.success == 'error' ){
                    for(let key in res.errors){
                        displayError(key, res.errors[key]);
                    }

                    notify("An error occurred. Please review your entries and save again", 'error');
                }else{
                    notifyAjaxError();
                }
            };

            ajaxRequest(reqUrl, false, xhrData, sucCb, null, null, '1');
        }else{
            notify("An error occurred. Please review your entries and save again", 'error');
        }
    });

    $('#tie_player_to_agent_form').on('reset', function(){
        $('#tie_player_to_agent_player').html('<option value=""></option>');

        // reset select2 values
        $(this).find('[data-toggle="select2"]').val('').trigger('change');

        // remove validation
        $(this).find('.invalid-feedback').text('');
        $(this).find('.is-invalid').removeClass('is-invalid');
        $(this).find('.is-valid').removeClass('is-valid');

        // reset file upload label
        $(this).find('.custom-file label[for="tie_player_to_agent_mandate_file"]').text('Choose a PDF file');
    });

    // delete player
    $('#agents-table').on('click', '.delete-record', function(){
        let row = $(this).closest('tr');

        let xhrData = {
            __delete_record: true,
            id: t.row(row).data()['id']
        };

        let sucCb = function(res){
            if( res.success == 'success' ){
                notify('Record successfully deleted', 'success');
                t.row(row).remove().draw();
            }else if( res.success == 'error' ){
                notify(res.errors.error, 'error', 7000);
            }else{
                notifyAjaxError();
            }
        }

        swal({
            title: 'Delete Record!',
            html: `
                <h6 class="mb-1 font-weight-light">Do you want to delete selected record?</h6>
                <h6 class="mb-2 font-weight-bolder font-italic">Note! All associated documents will be deleted.</h6>
                <h6 class="mb-4 font-weight-light">In order to proceed, please type the word 'delete' in the box below</h6>
                <input type="text" class="form-control form-control-lg text-center" id="delete_record" spellcheck="false" placeholder="Enter the word 'delete'">
                <hr class="mb-0">
            `,
            allowEnterKey: false,
            customClass: 'p-5',
            showCancelButton: true,
            cancelButtonText: 'Cancel',
            confirmButtonText: 'Delete!',
            confirmButtonColor: 'red',
            reverseButtons: true,
            preConfirm: function(value){
                if( $('#delete_record').val().toLowerCase() != 'delete' ){
                    Swal.showValidationMessage("Please enter the word 'delete'");
                }
            },
            onOpen: function(){
                $('#delete_record').focus();
            }
        }).then((result) => {
            if( result.value ){
                ajaxRequest(reqUrl, true, xhrData, sucCb, null, null, '1');
            }
        });
    });
});
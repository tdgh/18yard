$(document).ready(function(){
    const reqUrl = DOMAIN+'api/post?p=requests%2Fadd_player';
    prefix = 'add_player_';

    // repeater
    $('#achievements-repeater').repeater({
        defaultValues: {},
        show: function(){
            if( $('[data-repeater-item="achievements"]').length >= 7 ) $('[data-repeater-create="achievements"]').attr('disabled', 'disabled');
        },
        hide: function(deleteElement){
            $(this).hide(deleteElement);
            $('[data-repeater-create="achievements"]').removeAttr('disabled');
        },
        ready: function(setIndexes){},
        isFirstItemUndeletable: false
    });

    $('#career-path-repeater').repeater({
        defaultValues: {},
        show: function(){
            if( $('[data-repeater-item="career-path"]').length >= 7 ) $('[data-repeater-create="career-path"]').attr('disabled', 'disabled');
        },
        hide: function(deleteElement){
            $(this).hide(deleteElement);
            $('[data-repeater-create="career-path"]').removeAttr('disabled');
        },
        ready: function(setIndexes){},
        isFirstItemUndeletable: false
    });

    /* --- bio data --- */
    ele('first_name').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: true});
    });

    ele('last_name').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: true});
    });

    ele('date_of_birth').on('change', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    radioEle('sex').on('change', function(){
        $(this).VALIDATE({type: "radio", required: true, rOpts: ['Male', 'Female']});
    });

    ele('nationality').on('change', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('marital_status').on('change', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('weight').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('height').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('passport_no').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('preferred_position').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('sub_position').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('current_club').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('contract_expiration').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('level_played').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('registration_type').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('playing_foot').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('health').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('player_profile').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    /* --- personal skills --- */
    ele('right_foot').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('left_foot').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('speed').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('athleticism').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('tactics').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('hard_work').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('teamwork').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('adaptability').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('personality').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    /* --- achievements --- */
    ele('form').on('keyup blur', '[name^="add_player_achievements"]', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    /* --- career path --- */
    ele('form').on('keyup blur', '[name^="add_player_career_path"]', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    /* --- contact details --- */
    ele('phone').on('keyup blur', function(){
        validatePhone(this);
    });

    ele('other_phone').on('keyup blur', function(){
        validatePhone(this);
    });

    ele('email').on('keyup blur', function(){
        $(this).VALIDATE({type: "email", required: false});
    });

    ele('address').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    /* --- agency details --- */
    ele('player_number').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: true});
    });

    ele('contract').on('change', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('date_joined').on('change', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('status').on('change', function(){
        $(this).VALIDATE({type: "", required: true});
    });

    ele('status_date').on('change', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('status_comment').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    /* ****************************
     form submission section
    **************************** */
    function errorExistNav(){
        $('#nav-content a').each(function(){
            let $this = $(this);

            if( $( $this.attr('href') ).find('.invalid-feedback:not(:empty)').text().length > 0 ){
                $this.addClass('error-nav');
            }else{
                $this.removeClass('error-nav');
            }
        });
    }

    ele('form').on('submit', function(e){
        e.preventDefault();

        let $this = $(this), errorBag = [];

        /* --- bio data --- */
        errorBag.push(ele('first_name').VALIDATE({type: "", required: true}));
        errorBag.push(ele('last_name').VALIDATE({type: "", required: true}));
        errorBag.push(ele('date_of_birth').VALIDATE({type: "", required: false}));
        errorBag.push(radioEle('sex').VALIDATE({type: "radio", required: true, rOpts: ['Male', 'Female']}));
        errorBag.push(ele('nationality').VALIDATE({type: "", required: false}));
        errorBag.push(ele('marital_status').VALIDATE({type: "", required: false}));
        errorBag.push(ele('weight').VALIDATE({type: "", required: false}));
        errorBag.push(ele('height').VALIDATE({type: "", required: false}));
        errorBag.push(ele('passport_no').VALIDATE({type: "", required: false}));
        errorBag.push(ele('preferred_position').VALIDATE({type: "", required: false}));
        errorBag.push(ele('sub_position').VALIDATE({type: "", required: false}));
        errorBag.push(ele('current_club').VALIDATE({type: "", required: false}));
        errorBag.push(ele('contract_expiration').VALIDATE({type: "", required: false}));
        errorBag.push(ele('level_played').VALIDATE({type: "", required: false}));
        errorBag.push(ele('registration_type').VALIDATE({type: "", required: false}));
        errorBag.push(ele('playing_foot').VALIDATE({type: "", required: false}));
        errorBag.push(ele('health').VALIDATE({type: "", required: false}));
        errorBag.push(ele('player_profile').VALIDATE({type: "", required: false}));

        /* --- personal skills --- */
        errorBag.push(ele('right_foot').VALIDATE({type: "", required: false}));
        errorBag.push(ele('left_foot').VALIDATE({type: "", required: false}));
        errorBag.push(ele('speed').VALIDATE({type: "", required: false}));
        errorBag.push(ele('athleticism').VALIDATE({type: "", required: false}));
        errorBag.push(ele('tactics').VALIDATE({type: "", required: false}));
        errorBag.push(ele('hard_work').VALIDATE({type: "", required: false}));
        errorBag.push(ele('teamwork').VALIDATE({type: "", required: false}));
        errorBag.push(ele('adaptability').VALIDATE({type: "", required: false}));
        errorBag.push(ele('personality').VALIDATE({type: "", required: false}));

        /* --- achievements --- */
        $('[name^="add_player_achievements"]').each(function(i){
            errorBag.push($(this).VALIDATE({type: "", required: false}));
        });

        /* --- career path --- */
        $('[name^="add_player_career_path"]').each(function(i){
            errorBag.push($(this).VALIDATE({type: "", required: false}));
        });

        /* --- contact details --- */
        errorBag.push(validatePhone('#'+prefix+'phone'));
        errorBag.push(validatePhone('#'+prefix+'other_phone'));
        errorBag.push(ele('email').VALIDATE({type: "email", required: false}));
        errorBag.push(ele('address').VALIDATE({type: "", required: false}));

        /* --- agency details --- */
        errorBag.push(ele('player_number').VALIDATE({type: "", required: true}));
        errorBag.push(ele('date_joined').VALIDATE({type: "", required: false}));
        errorBag.push(ele('contract').VALIDATE({type: "", required: false}));
        errorBag.push(ele('status').VALIDATE({type: "", required: true}));
        errorBag.push(ele('status_date').VALIDATE({type: "", required: false}));
        errorBag.push(ele('status_comment').VALIDATE({type: "", required: false}));

        // add/remove errors to/from navs
        errorExistNav();

        if( errorBag.indexOf(false) == -1 ){
            let xhrData = new FormData($this[0]);

            // set valid phone numbers
            $this.find('[data-toggle="intltelinput"]').each(function(){
                xhrData.set($(this).attr('name'), window.intlTelInputGlobals.getInstance(this).getNumber());
            });

            let sucCb = function(res){
                if( res.success == 'success' ){
                    notify('Record successfully saved', 'success');
                    $this[0].reset();
                }else if( res.success == 'error' ){
                    for(let key in res.errors){
                        displayError(key, res.errors[key]);
                    }

                    notify("An error occurred. Please review your entries and save again", 'error');
                    errorExistNav();
                }else{
                    notifyAjaxError();
                }
            };

            swal({
                type: 'warning',
                title: 'WARNING!',
                html: '<p>Are you sure the correct details have been entered and as such you want to save this record?</p>',
                allowEnterKey: false,
                showCancelButton: true,
                cancelButtonText: 'No! Cancel',
                confirmButtonText: 'Yes! Save',
                confirmButtonColor: 'green',
                reverseButtons: true
            }).then((result) => {
                if( result.value ){
                    ajaxRequest(reqUrl, false, xhrData, sucCb, null, null, '1');
                }
            });
        }else{
            notify("An error occurred. Please review your entries and save again", 'error');
        }
    });

    ele('form').on('reset', function(){
        // reset select2 values
        $(this).find('[data-toggle="select2"]').val('').trigger('change');

        // remove validation
        $(this).find('.invalid-feedback').text('');
        $(this).find('.is-invalid').removeClass('is-invalid');
        $(this).find('.is-valid').removeClass('is-valid');

        // reset dropify
        $(this).find('.dropify-clear').click();
        $(this).find('.dropify-wrapper').removeClass('has-error');
        $(this).find('.dropify-errors-container ul').html('');

        // reset file upload label
        $(this).find('.custom-file label[for="add_player_contract"]').text('Choose a PDF file');

        // reset repeater
        $('[data-repeater-list] [data-repeater-item]:not(:first-child)').remove();

        // remove errors from navs
        $('#nav-content a').removeClass('error-nav');

        // scroll to top
        $('body, html').animate({scrollTop: $('body').offset().top}, 300);
    });
});
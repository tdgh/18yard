function displayError(name, errorText='', cls='is-invalid', result=false){
    let $this   = $('[name="'+name+'"]'), 
        parent  = $this.closest('.form-group'),
        errCont = parent.find('.invalid-feedback');

    cls = ['checkbox', 'radio'].indexOf($this.attr('type')) != -1 ? '' : cls;
    $this.removeClass('is-invalid').removeClass('is-valid').addClass(cls);
    errCont.text(errorText).show();

    return result;
}

// validate phone
function validatePhone(el, required=false){
    let phone = $(el),
        valid = phone.VALIDATE({type: "phone", required: required});

    if( valid && phone.val() != '' ){
        if( !window.intlTelInputGlobals.getInstance(phone[0]).isValidNumber() ){
            valid = displayError(phone.attr('name'), 'A valid phone number is required');
        }else{
            valid = displayError(phone.attr('name'), '', 'is-valid', true);
        }
    }

    return valid;
}

;(function($){
    $.fn.VALIDATE = function(options){
        var options = $.extend({
            required: false,
            type: "",
            noClass: false,
            rOpts: [],
            trim: true,
            min: 0,
            passVal: '',
            strongPass: true
        }, options);

        let $this   = this,
            parent  = $this.closest('.form-group'),
            errCont = parent.find('.invalid-feedback'),
            value   = $this.val(),
            result  = false;

        if( options.type == 'radio' ){
            value = $('input[name="'+$this.attr('name')+'"]:checked').val();

            if( typeof value == 'undefined' || options.rOpts.indexOf(value) == -1 ){
                errCont.text("Please choose a valid option").show();
                result = false;
            }else{
                errCont.text("").hide();
                result = true;
            }

            return result;
        }


        if( $this.length == 0 ){
            return false;
        }else{
            if( options.trim && 
                ['password', 'confirm_password'].indexOf(options.type) == -1 && 
                $this[0].tagName != 'SELECT' 
            ){
                value = value.trim();
            }
            

            let props = {
                name: {
                    validate: function(){
                        return /^[a-zA-Z\.\-,& ]{3,}$/.test(value);
                    },
                    errMsg: "A valid name is required"
                },
                email: {
                    validate: function(){
                        return /^([a-zA-Z0-9_.+-]{2,4})+\@(([a-zA-Z0-9-]{2,8})+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
                    },
                    errMsg: "A valid email is required"
                },
                phone: {
                    validate: function(){
                        return /^(\+)?([\d ]){8,15}$/.test(value);
                    },
                    errMsg: "A valid phone number is required"
                },
                username: {
                    validate: function(){
                        return /^[a-zA-Z0-9\.\-,& ]{3,}$/.test(value);
                    },
                    errMsg: "A valid username is required"
                },
                password: {
                    validate: function(){
                        if( options.strongPass ){
                            return /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[?!@#$%\^&*()_\-+=]).{8,}$/.test(value);
                        }else{
                            return value.length >= 6;
                        }
                    },
                    errMsg: options.strongPass ? "A strong password is required" : "Password must be at least 6 characters"
                },
                number: {
                    validate: function(){
                        return !isNaN(value);
                    },
                    errMsg: "A valid number is required"
                }
            };

            let error = function(errorText="", addClass="", res=true){
                let cls = options.noClass ? "" : addClass;
                
                $this.removeClass('is-invalid').removeClass('is-valid').addClass(cls);
                errCont.text(errorText)[errorText!='' ? 'show' : 'hide']();

                return res;
            };

            if( options.type === 'confirm_password' ){
                if( options.passVal !== value ){
                    result = error('Password does not match', 'is-invalid', false);
                }else if( options.passVal === value && options.passVal !== '' ){
                    result = error('', 'is-valid', true);
                }else{
                    result = error('', '', false);
                }
            }else if( options.required && [null, undefined, []].indexOf(value) != -1 && $this[0].tagName == 'SELECT' ){
                result = error('This field is required', 'is-invalid', false);
            }else if( options.required && value.length == 0 ){
                result = error('This field is required', 'is-invalid', false);
            }else if( options.required && value.length < options.min ){
                result = error('Minimum of '+options.min+' characters required', 'is-invalid', false);
            }else if( !options.required && value.length == 0 ){
                result = error('', 'is-valid', true);
            }else if( props.hasOwnProperty(options.type) && !props[options.type].validate() ){
                result = error(props[options.type].errMsg, 'is-invalid', false);
            }else{
                result = error('', 'is-valid', true);
            }

            return result;
        }
    }
}(jQuery));
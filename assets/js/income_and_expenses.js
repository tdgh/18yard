$(document).ready(function(){
    const reqUrl = DOMAIN+'api/post?p=requests%2Fincome_and_expenses';
    prefix = 'add_income_or_expenses_';

    // sum plugin
    jQuery.fn.dataTable.Api.register( 'sum()', function ( ) {
        return this.flatten().reduce( function ( a, b ) {
            if ( typeof a === 'string' ) {
                a = a.replace(/[^\d.-]/g, '') * 1;
            }
            if ( typeof b === 'string' ) {
                b = b.replace(/[^\d.-]/g, '') * 1;
            }
     
            return a + b;
        }, 0 );
    } );


	var t = $('#income-and-expenses-table').DataTable({
        dom: "<'text-muted d-flex justify-content-between mb-1'fi><'#income-and-expenses-table-wrapper.table-responsive hide-scroll border-left border-right'tr><'mt-4 d-flex justify-content-center'p>",
		language: {
			emptyTable: 'No record found',
			paginate: {
				previous: '<i class="fa fa-angle-left"></i>',
				next: '<i class="fa fa-angle-right"></i>'
			}
		},
		autoWidth: false,
        processing: true,
        deferRender: true,
        "ajax": {
            type: 'POST',
            url: reqUrl,
            data: {__get_income_and_expenses: true},
            beforeSend: function(){
                if( typeof t != 'undefined' && t.settings()[0].jqXHR != null ){
                    t.settings()[0].jqXHR.abort();
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                if( textStatus != 'abort' ){
                    notify(`Couldn't load table content. An error occurred`, 'error');
                    $('#income-and-expenses-table_processing').hide();
                    t.clear().draw();
                }
            }
        },
        "initComplete": function(settings, json){},
        "createdRow": function(row, data, index){},
        drawCallback: function () {
            let api = this.api();
            let totalIncome = api.column( 4, {search:'applied'} ).data().sum();
            let totalExpenses = api.column( 5, {search:'applied'} ).data().sum();
            let grossProfit = totalIncome - totalExpenses;

            $('#total-income').html(numberFormat(totalIncome, 2));
            $('#total-expenses').html(numberFormat(totalExpenses, 2));
            $('#gross-profit').html(numberFormat(grossProfit, 2));
        },
        "columns" : [
            {data: 'id', className: 'text-center'},
            {data: 'transaction_date'},
            {data: 'description'},
            {data: 'player'},
            {data: 'income'},
            {data: 'expenses'},
            {data: 'comment'}
        ],

        "columnDefs": [{
            "searchable": false,
            "orderable": false,
            "targets": 0,
            render: function(data, type, row, meta){
                if( type == 'display' ){
            	    data = `<button type="button" class="btn btn-sm btn-icon btn-secondary delete-record"><i class="fa fa-trash-alt"></i></button>`;
                }

                return data;
            }
        },{
            "targets": 1,
            render: function(data, type, row, meta){
                if( type == 'sort' ){
                    data = row.timestamp;
                }

                return data;
            }
        },{
            "targets": [4, 5],
            "className": "text-right",
            render: function(data, type, row, meta){
                if( type == 'display' ){
                    data = data != '' ? numberFormat(data, 2) : '';
                }

                return data;
            }
        }],
        "iDisplayLength": 25,
        "aLengthMenu": [
            [25, 50, 100],
            [25, 50, 100]
        ],
        "order": [[ 1, 'desc' ]]
    });


    // reload
    function reloadDataTable(){
        unstLoader2('on');
        t.ajax.reload(function(json){
            if( ajaxError(json) !== false ){
                return;
            }else{
                unstLoader2('off');
            }
        });
    }

	$('#reload-table').on('click', function(){
        reloadDataTable();
    });

    // add income or expenses
    ele("player").select2({
        ajax: {
            url: reqUrl,
            type: 'post',
            dataType: 'json',
            delay: 250,
            data: function(params){
                return {
                    __get_select2_players: true,
                    search: params.term, // search term
                    page: params.page
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                    results: data.details.players,
                    pagination: {
                        more: (params.page * 15) < data.details.total
                    }
                };
            },
            cache: true
        },
        dropdownParent: $("#add_income_or_expenses_form"),
        placeholder: "",
        allowClear: true,
        escapeMarkup: function (markup) { return markup; },
        minimumInputLength: 3,
        templateResult: function(player) {
            if( player.loading ) return player.text;

            return  `<div class="d-flex align-items-center">
                        <img src="${player.photo}" alt="" class="img-fluid rounded-circle select2-player-thumb mr-2 flex-shrink-0">
                        <div><h6 class="mb-0">${player.name}</h6> <p class="mb-0">${player.player_number}</p></div>
                    </div>`;
        },
        templateSelection: function(player) {
            return player.name || player.text;
        }
    });

    //
    ele('transaction_date').on('change', function(){
        $(this).VALIDATE({type: "", required: true});
    });

    radioEle('type').on('change', function(){
        $(this).VALIDATE({type: "radio", required: true, rOpts: ['income', 'expenses']});
    });

    ele('description').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: true});
    });

    ele('player').on('change', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('amount').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: true});
    });

    ele('comment').on('keyup blur', function(){
        $(this).VALIDATE({type: "", required: false});
    });

    ele('form').on('submit', function(e){
        e.preventDefault();

        let $this = $(this), errorBag = [];

        errorBag.push(ele('transaction_date').VALIDATE({type: "", required: true}));
        errorBag.push(radioEle('type').VALIDATE({type: "radio", required: true, rOpts: ['income', 'expenses']}));
        errorBag.push(ele('description').VALIDATE({type: "", required: true}));
        errorBag.push(ele('player').VALIDATE({type: "", required: false}));
        errorBag.push(ele('amount').VALIDATE({type: "", required: true}));
        errorBag.push(ele('comment').VALIDATE({type: "", required: false}));

        if( errorBag.indexOf(false) == -1 ){
            let xhrData = new FormData($this[0]);

            let sucCb = function(res){
                if( res.success == 'success' ){
                    notify('Record successfully saved', 'success');
                    $('#addIncomeOrExpensesModal').modal('hide');
                    $this[0].reset();
                    reloadDataTable();
                }else if( res.success == 'error' ){
                    for(let key in res.errors){
                        displayError(key, res.errors[key]);
                    }

                    notify("An error occurred. Please review your entries and save again", 'error');
                }else{
                    notifyAjaxError();
                }
            };

            ajaxRequest(reqUrl, false, xhrData, sucCb, null, null, '1');
        }else{
            notify("An error occurred. Please review your entries and save again", 'error');
        }
    });

    ele('form').on('reset', function(){
        ele('player').html('<option value=""></option>');

        // reset select2 values
        $(this).find('[data-toggle="select2"]').val('').trigger('change');

        // remove validation
        $(this).find('.invalid-feedback').text('');
        $(this).find('.is-invalid').removeClass('is-invalid');
        $(this).find('.is-valid').removeClass('is-valid');
    });

    // delete record
    $('#income-and-expenses-table').on('click', '.delete-record', function(){
        let row = $(this).closest('tr');

        let xhrData = {
            __delete_record: true,
            id: t.row(row).data()['id']
        };

        let sucCb = function(res){
            if( res.success == 'success' ){
                notify('Record successfully deleted', 'success');
                t.row(row).remove().draw();
            }else if( res.success == 'error' ){
                notify(res.errors.error, 'error', 7000);
            }else{
                notifyAjaxError();
            }
        }

        swal({
            title: 'Delete Record!',
            html: `
                <h6 class="mb-1 font-weight-light">Do you want to delete selected record?</h6>
                <h6 class="mb-2 font-weight-bolder font-italic">Note! All associated documents will be deleted.</h6>
                <h6 class="mb-4 font-weight-light">In order to proceed, please type the word 'delete' in the box below</h6>
                <input type="text" class="form-control form-control-lg text-center" id="delete_record" spellcheck="false" placeholder="Enter the word 'delete'">
                <hr class="mb-0">
            `,
            allowEnterKey: false,
            customClass: 'p-5',
            showCancelButton: true,
            cancelButtonText: 'Cancel',
            confirmButtonText: 'Delete!',
            confirmButtonColor: 'red',
            reverseButtons: true,
            preConfirm: function(value){
                if( $('#delete_record').val().toLowerCase() != 'delete' ){
                    Swal.showValidationMessage("Please enter the word 'delete'");
                }
            },
            onOpen: function(){
                $('#delete_record').focus();
            }
        }).then((result) => {
            if( result.value ){
                ajaxRequest(reqUrl, true, xhrData, sucCb, null, null, '1');
            }
        });
    });
});
#!/usr/bin/env bash

mysql_config_file="/etc/mysql/mysql.conf.d/mysqld.cnf"
php_config_apache2_file="/etc/php/7.1/apache2/php.ini"
php_config_cli_file="/etc/php/7.1/cli/php.ini"
xdebug_config_file="/etc/php/7.1/apache2/conf.d/20-xdebug.ini"


function wait_for_apt_lock() {
    while [ "" = "" ]; do
        eval "$1" 2>/dev/null
        if [ $? -eq 0 ]; then
            break
        fi
        sleep 1
        echo "Waiting for apt lock..."
    done
}

# This function is called at the very bottom of the file
main() {
	#repositories_go
	update_go
	#network_go
	tools_go
	apache_go
	mysql_go
	php_go
	autoremove_go
	nodejs_setup
	complete_setup
}

network_go() {
	IPADDR=$(/sbin/ifconfig eth0 | awk '/inet / { print $2 }' | sed 's/addr://')
	sudo sed -i "s/^${IPADDR}.*//" /etc/hosts
	echo ${IPADDR} ubuntu.localhost >> /etc/hosts
}

update_go() {
	# Update the server
	sudo apt-get update
	# apt-get -y upgrade
}

autoremove_go() {
	sudo apt-get -y autoremove
}

nodejs_setup(){
    curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
    sudo apt-get -y install gcc g++ make nodejs build-essential
    sudo npm install --global gulp
    sudo npm install --global gulp-cli
}

complete_setup(){
    sudo apt-get -y install supervisor
    sudo cp /var/www/vagrant/alfapay-worker.conf /etc/supervisor/conf.d/
    sudo cp /var/www/vagrant/alfapay-socketcluster-server.conf /etc/supervisor/conf.d/
    sudo supervisorctl reread
    sudo supervisorctl update

    sudo chmod -R 777 /var/www/bootstrap/cache
    sudo mkdir -p /var/www/storage/cache
    sudo chmod -R 777 /var/www/storage/cache
    sudo mkdir -p /var/www/boostrap/cache
    sudo chmod -R 777 /var/www/boostrap/cache

    cd /var/www
    composer install
    php artisan migrate

}

tools_go() {
	# Install basic tools
	sudo apt-get install -y curl
	sudo apt-get install -y make
	sudo apt-get install -y python
	sudo apt-get install -y openssl
	sudo apt-get install -y unzip
	sudo apt-get install -y vim
	sudo apt-get install -y tree
	sudo apt-get install -y git
	sudo apt-get install -y redis-server
	sudo apt-get install -y debconf-utils
	sudo apt-get install -y software-properties-common
    sudo add-apt-repository -y ppa:ondrej/php
    sudo apt-get update

}

apache_go() {
	# Install Apache

	echo "[vagrant provisioning] Installing apache2..."
	sudo apt-get install -y apache2 # installs apache and some dependencies
	sudo apt-get install -y libxml2-dev

	sudo a2enmod ssl
	sudo a2enmod proxy
	sudo a2enmod proxy_http
	sudo a2enmod proxy_wstunnel

	sudo service apache2 restart # restarting for sanities' sake
	
	#echo "[vagrant provisioning] Generating SSL"
	#openssl genrsa -out /vagrant/alfapay.local.key 2048
	#openssl req -new -x509 -key /vagrant/alfapay.local.key -out /vagrant/alfapay.local.cert -days 3650 -subj /CN=alfapay.local

	echo "[vagrant provisioning] Applying Apache vhost conf..."
	sudo rm -f /etc/apache2/sites-available/default
	sudo rm -f /etc/apache2/sites-enabled/000-default
	sudo rm -f /etc/apache2/sites-enabled/000-default.conf
	sudo rm -f /etc/apache2/sites-available/000-default.conf

	sudo rm -f /etc/apache2/sites-available/default-ssl
	sudo rm -f /etc/apache2/sites-available/default-ssl.conf
	sudo rm -f /etc/apache2/sites-enabled/000-default-ssl
	sudo rm -f /etc/apache2/sites-enabled/000-default-ssl.conf
	sudo rm -f /etc/apache2/sites-available/000-default-ssl.conf

	if [ ! -f "/etc/apache2/sites-available/default-ssl.conf" ]; then
		cat << EOF > "/etc/apache2/sites-available/default-ssl.conf"

		<IfModule mod_ssl.c>
	<VirtualHost _default_:443>
		ServerAdmin webmaster@localhost
		ServerName alfapay.local
		ServerAlias alfapay.digital

		DocumentRoot /var/www

		<Directory />
			Options FollowSymLinks
			AllowOverride All
			Require all denied
		</Directory>
		<Directory /var/www>
			Options Indexes FollowSymLinks MultiViews
			AllowOverride All
			Require all granted
		</Directory>

		#adding custom SSL cert
		SSLEngine on
		SSLCertificateFile /vagrant/alfapay.local.cert
		SSLCertificateKeyFile /vagrant/alfapay.local.key

		# Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
		# error, crit, alert, emerg.
		# It is also possible to configure the loglevel for particular
		# modules, e.g.
		#LogLevel info ssl:warn

		ErrorLog \${APACHE_LOG_DIR}/error.log
		CustomLog \${APACHE_LOG_DIR}/access.log combined

		# For most configuration files from conf-available/, which are
		# enabled or disabled at a global level, it is possible to
		# include a line for only one particular virtual host. For example the
		# following line enables the CGI configuration for this host only
		# after it has been globally disabled with "a2disconf".
		#Include conf-available/serve-cgi-bin.conf

		#   SSL Engine Switch:
		#   Enable/Disable SSL for this virtual host.
		#SSLEngine on

		#   A self-signed (snakeoil) certificate can be created by installing
		#   the ssl-cert package. See
		#   /usr/share/doc/apache2/README.Debian.gz for more info.
		#   If both key and certificate are stored in the same file, only the
		#   SSLCertificateFile directive is needed.
		#SSLCertificateFile	/etc/ssl/certs/ssl-cert-snakeoil.pem
		#SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key

		#   Server Certificate Chain:
		#   Point SSLCertificateChainFile at a file containing the
		#   concatenation of PEM encoded CA certificates which form the
		#   certificate chain for the server certificate. Alternatively
		#   the referenced file can be the same as SSLCertificateFile
		#   when the CA certificates are directly appended to the server
		#   certificate for convinience.
		#SSLCertificateChainFile /etc/apache2/ssl.crt/server-ca.crt

		#   Certificate Authority (CA):
		#   Set the CA certificate verification path where to find CA
		#   certificates for client authentication or alternatively one
		#   huge file containing all of them (file must be PEM encoded)
		#   Note: Inside SSLCACertificatePath you need hash symlinks
		#		 to point to the certificate files. Use the provided
		#		 Makefile to update the hash symlinks after changes.
		#SSLCACertificatePath /etc/ssl/certs/
		#SSLCACertificateFile /etc/apache2/ssl.crt/ca-bundle.crt

		#   Certificate Revocation Lists (CRL):
		#   Set the CA revocation path where to find CA CRLs for client
		#   authentication or alternatively one huge file containing all
		#   of them (file must be PEM encoded)
		#   Note: Inside SSLCARevocationPath you need hash symlinks
		#		 to point to the certificate files. Use the provided
		#		 Makefile to update the hash symlinks after changes.
		#SSLCARevocationPath /etc/apache2/ssl.crl/
		#SSLCARevocationFile /etc/apache2/ssl.crl/ca-bundle.crl

		#   Client Authentication (Type):
		#   Client certificate verification type and depth.  Types are
		#   none, optional, require and optional_no_ca.  Depth is a
		#   number which specifies how deeply to verify the certificate
		#   issuer chain before deciding the certificate is not valid.
		#SSLVerifyClient require
		#SSLVerifyDepth  10

		#   SSL Engine Options:
		#   Set various options for the SSL engine.
		#   o FakeBasicAuth:
		#	 Translate the client X.509 into a Basic Authorisation.  This means that
		#	 the standard Auth/DBMAuth methods can be used for access control.  The
		#	 user name is the `one line' version of the client's X.509 certificate.
		#	 Note that no password is obtained from the user. Every entry in the user
		#	 file needs this password: `xxj31ZMTZzkVA'.
		#   o ExportCertData:
		#	 This exports two additional environment variables: SSL_CLIENT_CERT and
		#	 SSL_SERVER_CERT. These contain the PEM-encoded certificates of the
		#	 server (always existing) and the client (only existing when client
		#	 authentication is used). This can be used to import the certificates
		#	 into CGI scripts.
		#   o StdEnvVars:
		#	 This exports the standard SSL/TLS related SSL_* environment variables.
		#	 Per default this exportation is switched off for performance reasons,
		#	 because the extraction step is an expensive operation and is usually
		#	 useless for serving static content. So one usually enables the
		#	 exportation for CGI and SSI requests only.
		#   o OptRenegotiate:
		#	 This enables optimized SSL connection renegotiation handling when SSL
		#	 directives are used in per-directory context.
		#SSLOptions +FakeBasicAuth +ExportCertData +StrictRequire
		<FilesMatch "\.(cgi|shtml|phtml|php)$">
				SSLOptions +StdEnvVars
		</FilesMatch>
		<Directory /usr/lib/cgi-bin>
				SSLOptions +StdEnvVars
		</Directory>

		#   SSL Protocol Adjustments:
		#   The safe and default but still SSL/TLS standard compliant shutdown
		#   approach is that mod_ssl sends the close notify alert but doesn't wait for
		#   the close notify alert from client. When you need a different shutdown
		#   approach you can use one of the following variables:
		#   o ssl-unclean-shutdown:
		#	 This forces an unclean shutdown when the connection is closed, i.e. no
		#	 SSL close notify alert is send or allowed to received.  This violates
		#	 the SSL/TLS standard but is needed for some brain-dead browsers. Use
		#	 this when you receive I/O errors because of the standard approach where
		#	 mod_ssl sends the close notify alert.
		#   o ssl-accurate-shutdown:
		#	 This forces an accurate shutdown when the connection is closed, i.e. a
		#	 SSL close notify alert is send and mod_ssl waits for the close notify
		#	 alert of the client. This is 100% SSL/TLS standard compliant, but in
		#	 practice often causes hanging connections with brain-dead browsers. Use
		#	 this only for browsers where you know that their SSL implementation
		#	 works correctly.
		#   Notice: Most problems of broken clients are also related to the HTTP
		#   keep-alive facility, so you usually additionally want to disable
		#   keep-alive for those clients, too. Use variable "nokeepalive" for this.
		#   Similarly, one has to force some clients to use HTTP/1.0 to workaround
		#   their broken HTTP/1.1 implementation. Use variables "downgrade-1.0" and
		#   "force-response-1.0" for this.
		# BrowserMatch "MSIE [2-6]" \
		#		nokeepalive ssl-unclean-shutdown \
		#		downgrade-1.0 force-response-1.0

	</VirtualHost>
</IfModule>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
EOF

	fi

	if [ ! -f "/etc/apache2/sites-available/laravel-default-ssl.conf" ]; then
		cat << EOF > "/etc/apache2/sites-available/laravel-default-ssl.conf"

		<IfModule mod_ssl.c>
	<VirtualHost _default_:443>
		ServerAdmin webmaster@localhost
		ServerName api.alfapay.local
		ServerAlias alfapay.digital

		DocumentRoot /var/www

		<Directory />
			Options FollowSymLinks
			AllowOverride All
			Require all denied
		</Directory>
		<Directory /var/www>
			Options Indexes FollowSymLinks MultiViews
			AllowOverride All
			Require all granted
		</Directory>

		#adding custom SSL cert
		SSLEngine on
		SSLCertificateFile /vagrant/alfapay.local.cert
		SSLCertificateKeyFile /vagrant/alfapay.local.key

		# Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
		# error, crit, alert, emerg.
		# It is also possible to configure the loglevel for particular
		# modules, e.g.
		#LogLevel info ssl:warn

		ErrorLog \${APACHE_LOG_DIR}/error.log
		CustomLog \${APACHE_LOG_DIR}/access.log combined

		# For most configuration files from conf-available/, which are
		# enabled or disabled at a global level, it is possible to
		# include a line for only one particular virtual host. For example the
		# following line enables the CGI configuration for this host only
		# after it has been globally disabled with "a2disconf".
		#Include conf-available/serve-cgi-bin.conf

		#   SSL Engine Switch:
		#   Enable/Disable SSL for this virtual host.
		#SSLEngine on

		#   A self-signed (snakeoil) certificate can be created by installing
		#   the ssl-cert package. See
		#   /usr/share/doc/apache2/README.Debian.gz for more info.
		#   If both key and certificate are stored in the same file, only the
		#   SSLCertificateFile directive is needed.
		#SSLCertificateFile	/etc/ssl/certs/ssl-cert-snakeoil.pem
		#SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key

		#   Server Certificate Chain:
		#   Point SSLCertificateChainFile at a file containing the
		#   concatenation of PEM encoded CA certificates which form the
		#   certificate chain for the server certificate. Alternatively
		#   the referenced file can be the same as SSLCertificateFile
		#   when the CA certificates are directly appended to the server
		#   certificate for convinience.
		#SSLCertificateChainFile /etc/apache2/ssl.crt/server-ca.crt

		#   Certificate Authority (CA):
		#   Set the CA certificate verification path where to find CA
		#   certificates for client authentication or alternatively one
		#   huge file containing all of them (file must be PEM encoded)
		#   Note: Inside SSLCACertificatePath you need hash symlinks
		#		 to point to the certificate files. Use the provided
		#		 Makefile to update the hash symlinks after changes.
		#SSLCACertificatePath /etc/ssl/certs/
		#SSLCACertificateFile /etc/apache2/ssl.crt/ca-bundle.crt

		#   Certificate Revocation Lists (CRL):
		#   Set the CA revocation path where to find CA CRLs for client
		#   authentication or alternatively one huge file containing all
		#   of them (file must be PEM encoded)
		#   Note: Inside SSLCARevocationPath you need hash symlinks
		#		 to point to the certificate files. Use the provided
		#		 Makefile to update the hash symlinks after changes.
		#SSLCARevocationPath /etc/apache2/ssl.crl/
		#SSLCARevocationFile /etc/apache2/ssl.crl/ca-bundle.crl

		#   Client Authentication (Type):
		#   Client certificate verification type and depth.  Types are
		#   none, optional, require and optional_no_ca.  Depth is a
		#   number which specifies how deeply to verify the certificate
		#   issuer chain before deciding the certificate is not valid.
		#SSLVerifyClient require
		#SSLVerifyDepth  10

		#   SSL Engine Options:
		#   Set various options for the SSL engine.
		#   o FakeBasicAuth:
		#	 Translate the client X.509 into a Basic Authorisation.  This means that
		#	 the standard Auth/DBMAuth methods can be used for access control.  The
		#	 user name is the `one line' version of the client's X.509 certificate.
		#	 Note that no password is obtained from the user. Every entry in the user
		#	 file needs this password: `xxj31ZMTZzkVA'.
		#   o ExportCertData:
		#	 This exports two additional environment variables: SSL_CLIENT_CERT and
		#	 SSL_SERVER_CERT. These contain the PEM-encoded certificates of the
		#	 server (always existing) and the client (only existing when client
		#	 authentication is used). This can be used to import the certificates
		#	 into CGI scripts.
		#   o StdEnvVars:
		#	 This exports the standard SSL/TLS related SSL_* environment variables.
		#	 Per default this exportation is switched off for performance reasons,
		#	 because the extraction step is an expensive operation and is usually
		#	 useless for serving static content. So one usually enables the
		#	 exportation for CGI and SSI requests only.
		#   o OptRenegotiate:
		#	 This enables optimized SSL connection renegotiation handling when SSL
		#	 directives are used in per-directory context.
		#SSLOptions +FakeBasicAuth +ExportCertData +StrictRequire
		<FilesMatch "\.(cgi|shtml|phtml|php)$">
				SSLOptions +StdEnvVars
		</FilesMatch>
		<Directory /usr/lib/cgi-bin>
				SSLOptions +StdEnvVars
		</Directory>

		#   SSL Protocol Adjustments:
		#   The safe and default but still SSL/TLS standard compliant shutdown
		#   approach is that mod_ssl sends the close notify alert but doesn't wait for
		#   the close notify alert from client. When you need a different shutdown
		#   approach you can use one of the following variables:
		#   o ssl-unclean-shutdown:
		#	 This forces an unclean shutdown when the connection is closed, i.e. no
		#	 SSL close notify alert is send or allowed to received.  This violates
		#	 the SSL/TLS standard but is needed for some brain-dead browsers. Use
		#	 this when you receive I/O errors because of the standard approach where
		#	 mod_ssl sends the close notify alert.
		#   o ssl-accurate-shutdown:
		#	 This forces an accurate shutdown when the connection is closed, i.e. a
		#	 SSL close notify alert is send and mod_ssl waits for the close notify
		#	 alert of the client. This is 100% SSL/TLS standard compliant, but in
		#	 practice often causes hanging connections with brain-dead browsers. Use
		#	 this only for browsers where you know that their SSL implementation
		#	 works correctly.
		#   Notice: Most problems of broken clients are also related to the HTTP
		#   keep-alive facility, so you usually additionally want to disable
		#   keep-alive for those clients, too. Use variable "nokeepalive" for this.
		#   Similarly, one has to force some clients to use HTTP/1.0 to workaround
		#   their broken HTTP/1.1 implementation. Use variables "downgrade-1.0" and
		#   "force-response-1.0" for this.
		# BrowserMatch "MSIE [2-6]" \
		#		nokeepalive ssl-unclean-shutdown \
		#		downgrade-1.0 force-response-1.0

	</VirtualHost>
</IfModule>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
EOF

	fi

	if [ ! -f "/etc/apache2/sites-available/socketcluster-ssl.conf" ]; then
		cat << EOF > "/etc/apache2/sites-available/socketcluster-ssl.conf"

		<IfModule mod_ssl.c>
	Listen 8443

NameVirtualHost *:8443
<VirtualHost *:8443>

    SSLEngine On

    # Set the path to SSL certificate
    # Usage: SSLCertificateFile /path/to/cert.pem
    #SSLCertificateFile /etc/apache2/ssl/file.pem

    SSLEngine on
    SSLCertificateFile /vagrant/alfapay.local.cert
    SSLCertificateKeyFile /vagrant/alfapay.local.key



    # Servers to proxy the connection, or;
    # List of application servers:
    # Usage:
    # ProxyPass / http://[IP Addr.]:[port]/
    # ProxyPassReverse / http://[IP Addr.]:[port]/
    # Example:
    ProxyPass / http://0.0.0.0:8000/
    ProxyPassReverse / http://0.0.0.0:8000/

    RewriteEngine on
    RewriteCond %{HTTP:UPGRADE} ^WebSocket$ [NC]
    RewriteCond %{HTTP:CONNECTION} ^Upgrade$ [NC]
    RewriteRule .* ws://localhost:3999%{REQUEST_URI} [P]

    # Or, balance the load:
    # ProxyPass / balancer://balancer_cluster_name

</VirtualHost>
</IfModule>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
EOF

	fi

	if [ ! -f "/etc/apache2/sites-available/default.conf" ]; then
		cat << EOF > "/etc/apache2/sites-available/default.conf"
<VirtualHost *:80>
	ServerAdmin webmaster@localhost
	ServerName alfapay.local
	ServerAlias alfapay.digital

	DocumentRoot /var/www

	#Redirect permanent / https://alfapay.local/

	<Directory />
		Options FollowSymLinks
		AllowOverride All
        Require all denied
	</Directory>
	<Directory /var/www>
		Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Require all granted
	</Directory>

	ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
	<Directory /usr/lib/cgi-bin>
		AllowOverride All
		Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
		Require all granted
	</Directory>

    ErrorLog /var/log/apache2/error.log
    LogLevel warn
    CustomLog /var/log/apache2/access.log combined

</VirtualHost>
EOF
	fi

	if [ ! -f "/etc/apache2/sites-available/laravel-default.conf" ]; then
		cat << EOF > "/etc/apache2/sites-available/laravel-default.conf"
<VirtualHost *:80>
	ServerAdmin webmaster@localhost
	ServerName api.alfapay.local
	ServerAlias alfapay.digital

	DocumentRoot /var/www/public

	#Redirect permanent / https://alfapay.local/

	<Directory />
		Options FollowSymLinks
		AllowOverride All
        Require all denied
	</Directory>
	<Directory /var/www/public>
		Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Require all granted
	</Directory>

	ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
	<Directory /usr/lib/cgi-bin>
		AllowOverride All
		Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
		Require all granted
	</Directory>

    ErrorLog /var/log/apache2/error.log
    LogLevel warn
    CustomLog /var/log/apache2/access.log combined

</VirtualHost>
EOF
	fi

	#sudo ln -s /etc/apache2/sites-available/default.conf /etc/apache2/sites-enabled/000-default
	sudo a2ensite default
	sudo a2ensite laravel-default
	sudo a2ensite default-ssl
	sudo a2ensite laravel-default-ssl
	sudo a2ensite socketcluster-ssl
	a2enmod rewrite
	a2enmod actions
	a2enmod ssl
	sudo service apache2 restart
}

mysql_go() {
	# Install MySQL
	echo "mysql-server mysql-server/root_password password root" | debconf-set-selections
	echo "mysql-server mysql-server/root_password_again password root" | debconf-set-selections
	sudo apt-get -y install mysql-client mysql-server

	sudo sed -i "s/bind-address\s*=\s*127.0.0.1/bind-address = 0.0.0.0/" ${mysql_config_file}

	# Allow root access from any host
	echo "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root' WITH GRANT OPTION" | mysql -u root --password=root
	echo "GRANT PROXY ON ''@'' TO 'root'@'%' WITH GRANT OPTION" | mysql -u root --password=root

	if [ -d "/var/www/vagrant/provision-sql" ]; then
		echo "Executing all SQL files in /var/www/vagrant/provision-sql folder ..."
		echo "-------------------------------------"
		for sql_file in /var/www/vagrant/provision-sql/*.sql
		do
			echo "EXECUTING $sql_file..."
	  		sudo time mysql -u root --password=root < $sql_file
	  		echo "FINISHED $sql_file"
	  		echo ""
		done
	fi

	sudo service mysql restart
	sudo apt-get -y install postgresql postgresql-contrib phppgadmin
}

php_go() {
	sudo apt-get -y install php7.1 php7.1-curl php7.1-mysql php7.1-sqlite3 php7.1-xdebug php7.1-mcrypt php7.1-gd php7.1-zip php7.1-intl libapache2-mod-php7.1 php7.1-fpm php7.1-xml php7.1-soap php7.1-mbstring php7.1-redis php7.1-pgsql php7.1-bcmath
    sudo apt-get -y install redis-server
    sudo apt-get -y install beanstalkd
    sudo apt-get -y install supervisor
    sudo apt-get -y install rubygems
    sudo apt-get -y install ruby-dev
    sudo gem update --system
    sudo gem install redis-browser

    sudo apt-get remove -y php7.2-cli
    sudo apt-get remove -y php7.3-cli
    sudo a2enmod php7.1



    sudo phpenmod mcrypt
    sudo phpenmod gd

	sudo sed -i "s/display_startup_errors = Off/display_startup_errors = On/g" ${php_config_apache2_file}
	sudo sed -i "s/display_errors = Off/display_errors = On/g" ${php_config_apache2_file}

	if [ ! -f "{$xdebug_config_file}" ]; then
		cat << EOF > ${xdebug_config_file}
zend_extension=xdebug.so
xdebug.remote_enable=On
xdebug.remote_connect_back=On
xdebug.remote_port=9000
xdebug.remote_host=192.168.66.7
xdebug.idekey = PHPSTORM
EOF
	fi

	sudo service apache2 restart

	# Install latest version of Composer globally
	if [ ! -f "/usr/local/bin/composer" ]; then
		sudo curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
	fi

	# Install PHP Unit 4.8 globally
	if [ ! -f "/usr/local/bin/phpunit" ]; then
		sudo curl -O -L https://phar.phpunit.de/phpunit-old.phar
		sudo chmod +x phpunit-old.phar
		sudo mv phpunit-old.phar /usr/local/bin/phpunit
	fi
}

main
exit 0
-- MySQL dump 10.13  Distrib 5.7.32, for Linux (x86_64)
--
-- Host: localhost    Database: football4all
-- ------------------------------------------------------
-- Server version	5.7.32-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `unisoft_agents`
--

DROP TABLE IF EXISTS `unisoft_agents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unisoft_agents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `passport_no` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `background` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unisoft_agents`
--

LOCK TABLES `unisoft_agents` WRITE;
/*!40000 ALTER TABLE `unisoft_agents` DISABLE KEYS */;
INSERT INTO `unisoft_agents` VALUES (1,'Frederick Nana Oduro','+233246307011','Fake Agent','green'),(2,'Khalid Tsouli','+32470104910','EN243925','teal'),(3,'Ivan Jaffar','+79092426919','Russia Agent','info'),(4,'Benjamin Adjei','+233271397445','Dubai Agent','white'),(5,'Prince Diabor','+233559876242','Brescia & Turkey','cyan'),(6,'Elvis Abban','+233207886700','Glowban Consult','gray'),(7,'Bro Eli Agent','+233546225414','Kazahstan Deals','red'),(8,'Mark Altaor','+421233221257','Slovakia Agent','purple'),(9,'Mensah Yusif Agent','+233559842664','Belarus Agent','success'),(10,'Jakob Griesbener','+436649150585','Austria Agent & Estonia Agent','red'),(11,'Gregori','+306980015079','Greece Agent','danger'),(13,'patric','','','teal');
/*!40000 ALTER TABLE `unisoft_agents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unisoft_agents_players`
--

DROP TABLE IF EXISTS `unisoft_agents_players`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unisoft_agents_players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `mandate_file` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entry_date` datetime NOT NULL,
  `expiry_date` datetime NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `status_date` date NOT NULL,
  `status_comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unisoft_agents_players`
--

LOCK TABLES `unisoft_agents_players` WRITE;
/*!40000 ALTER TABLE `unisoft_agents_players` DISABLE KEYS */;
INSERT INTO `unisoft_agents_players` VALUES (2,4,1,'AGREEMENT BETWEEN BENJAMIN AND GEORGE ADDY.pdf','2020-03-06 00:00:00','2020-05-14 00:00:00',3,'2020-05-18','Inability to raise Extra Funds'),(9,7,13,'','2020-10-06 00:00:00','2020-11-30 00:00:00',1,'0000-00-00','Move player to Kazakhstan'),(10,7,12,'','2020-09-22 00:00:00','2020-11-30 00:00:00',1,'0000-00-00','Move player to kazakhstan'),(11,7,10,'','2020-09-29 00:00:00','2020-11-30 00:00:00',1,'0000-00-00','Move player to Kazakstan'),(13,9,9,'','2020-09-22 00:00:00','2020-11-30 00:00:00',1,'0000-00-00','Move player (Samuel Keelson) to Belarus'),(14,9,11,'','2020-09-22 00:00:00','2020-11-30 00:00:00',1,'2020-11-30','Move player (Baffoe Hammond Raymond)'),(15,6,8,'','2020-08-10 00:00:00','2020-11-16 00:00:00',1,'2020-11-16','Move player to Belgium -K.V.C. Westerlo'),(16,9,6,'','2020-09-08 00:00:00','2020-11-30 00:00:00',1,'2020-11-30','Move player Prince Quarshie to Belarus'),(17,13,7,'AGREEMENT BETWEEN PATRIC AND FRANCIS SULLEY MOHAMMED.pdf','2020-11-04 00:00:00','2021-04-01 00:00:00',1,'0000-00-00','xckhgvcfcgjhcvhgcch');
/*!40000 ALTER TABLE `unisoft_agents_players` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unisoft_attitude_eval`
--

DROP TABLE IF EXISTS `unisoft_attitude_eval`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unisoft_attitude_eval` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) DEFAULT NULL,
  `concentration` int(11) DEFAULT NULL,
  `selfishness` int(11) DEFAULT NULL,
  `discipline` int(11) DEFAULT NULL,
  `self_confidence` int(11) DEFAULT NULL,
  `solidarity_comradeship` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unisoft_attitude_eval`
--

LOCK TABLES `unisoft_attitude_eval` WRITE;
/*!40000 ALTER TABLE `unisoft_attitude_eval` DISABLE KEYS */;
INSERT INTO `unisoft_attitude_eval` VALUES (1,1,4,2,5,3,2),(2,3,2,2,2,2,2),(3,3,2,2,2,2,2);
/*!40000 ALTER TABLE `unisoft_attitude_eval` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unisoft_coexistence`
--

DROP TABLE IF EXISTS `unisoft_coexistence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unisoft_coexistence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) DEFAULT NULL,
  `tidiness_cleanliness` int(11) DEFAULT NULL,
  `relationship_with_the_team` int(11) DEFAULT NULL,
  `manners_respect` int(11) DEFAULT NULL,
  `personal_care_and_hygiene` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unisoft_coexistence`
--

LOCK TABLES `unisoft_coexistence` WRITE;
/*!40000 ALTER TABLE `unisoft_coexistence` DISABLE KEYS */;
INSERT INTO `unisoft_coexistence` VALUES (1,1,4,2,5,4),(2,3,3,3,3,3),(3,3,3,3,3,3);
/*!40000 ALTER TABLE `unisoft_coexistence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unisoft_countries`
--

DROP TABLE IF EXISTS `unisoft_countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unisoft_countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nationality` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=236 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unisoft_countries`
--

LOCK TABLES `unisoft_countries` WRITE;
/*!40000 ALTER TABLE `unisoft_countries` DISABLE KEYS */;
INSERT INTO `unisoft_countries` VALUES (1,'ANDORRA','+376','ANDORRAN',1),(2,'UNITED ARAB EMIRATES','+971','EMIRATI',1),(3,'AFGHANISTAN','+93','AFGHAN',1),(4,'ANTIGUA AND BARBUDA','+1268','ANTIGUANS',1),(5,'ANGUILLA','+1264','ANGUILLIAN',1),(6,'ALBANIA','+355','ALBANIAN',1),(7,'ARMENIA','+374','ARMENIAN',1),(8,'NETHERLANDS ANTILLES','+599','DUTCH',1),(9,'ANGOLA','+244','ANGOLAN',1),(10,'ANTARCTICA','+672','ANTARTIC RESIDENT',1),(11,'ARGENTINA','+54','ARGENTINEAN',1),(12,'AMERICAN SAMOA','+1684','AMERICAN SAMOAN',1),(13,'AUSTRIA','+43','AUSTRIAN',1),(14,'AUSTRALIA','+61','AUSTRALIAN',1),(15,'ARUBA','+297','ARUBAN',1),(16,'AZERBAIJAN','+994','AZERBAIJANI',1),(17,'BOSNIA AND HERZEGOVINA','+387','BOSNIAN',1),(18,'BARBADOS','+1246','BARBADIAN',1),(19,'BANGLADESH','+880','BANGLADESHI',1),(20,'BELGIUM','+32','BELGIAN',1),(21,'BURKINA FASO','+226','BURKINABE',1),(22,'BULGARIA','+359','BULGARIAN',1),(23,'BAHRAIN','+973','BAHRAINI',1),(24,'BURUNDI','+257','BURUNDIAN',1),(25,'BENIN','+229','BENINESE',1),(26,'SAINT BARTHELEMY','+590','BARTHELEMOISE',1),(27,'BERMUDA','+1441','BERMUDIAN',1),(28,'BRUNEI DARUSSALAM','+673','BRUNEIAN',1),(29,'BOLIVIA','+591','BOLIVIAN',1),(30,'BRAZIL','+55','BRAZILIAN',1),(31,'BAHAMAS','+1242','BAHAMIAN',1),(32,'BHUTAN','+975','BHUTANESE',1),(33,'BOTSWANA','+267','MOTSWANA',1),(34,'BELARUS','+375','BELARUSIAN',1),(35,'BELIZE','+501','BELIZEAN',1),(36,'CANADA','+1','CANADIAN',1),(37,'COCOS (KEELING) ISLANDS','+61','COCOS ISLANDER',1),(38,'CONGO, THE DEMOCRATIC REPUBLIC OF THE','+243','CONGOLESE',1),(39,'CENTRAL AFRICAN REPUBLIC','+236','CENTRAL AFRICAN',1),(40,'CONGO','+242','CONGOLESE',1),(41,'SWITZERLAND','+41','SWISS',1),(42,'COTE D\'IVOIRE','+225','IVORIAN',1),(43,'COOK ISLANDS','+682','COOK ISLANDER',1),(44,'CHILE','+56','CHILEAN',1),(45,'CAMEROON','+237','CAMEROONIAN',1),(46,'CHINA','+86','CHINESE',1),(47,'COLOMBIA','+57','COLOMBIAN',1),(48,'COSTA RICA','+506','COSTA RICAN',1),(49,'CUBA','+53','CUBAN',1),(50,'CAPE VERDE','+238','CAPE VERDEAN',1),(51,'CHRISTMAS ISLAND','+61','CHRISTMAS ISLANDER',1),(52,'CYPRUS','+357','CYPRIOT',1),(53,'CZECH REPUBLIC','+420','CZECH',1),(54,'GERMANY','+49','GERMAN',1),(55,'DJIBOUTI','+253','DJIBOUTIAN',1),(56,'DENMARK','+45','DANISH',1),(57,'DOMINICA','+1767','DOMINICAN',1),(58,'DOMINICAN REPUBLIC','+1809','DOMINICAN',1),(59,'ALGERIA','+213','ALGERIAN',1),(60,'ECUADOR','+593','ECUADOREAN',1),(61,'ESTONIA','+372','ESTONIAN',1),(62,'EGYPT','+20','EGYPTIAN',1),(63,'ERITREA','+291','ERITREAN',1),(64,'SPAIN','+34','SPANIARD',1),(65,'ETHIOPIA','+251','ETHIOPIAN',1),(66,'FINLAND','+358','FINNISH',1),(67,'FIJI','+679','FIJIAN',1),(68,'FALKLAND ISLANDS (MALVINAS)','+500','FALKLAND ISLANDER',1),(69,'MICRONESIA, FEDERATED STATES OF','+691','MICRONESIAN',1),(70,'FAROE ISLANDS','+298','FAROESE',1),(71,'FRANCE','+33','FRENCH',1),(72,'GABON','+241','GABONESE',1),(73,'UNITED KINGDOM','+44','BRITON',1),(74,'GRENADA','+1473','GRENADIAN',1),(75,'GEORGIA','+995','GEORGIAN',1),(76,'GHANA','+233','GHANAIAN',1),(77,'GIBRALTAR','+350','GILBRALTARIAN',1),(78,'GREENLAND','+299','GREENLANDER',1),(79,'GAMBIA','+220','GAMBIAN',1),(80,'GUINEA','+224','GUINEAN',1),(81,'EQUATORIAL GUINEA','+240','EQUATORIAL GUINEAN',1),(82,'GREECE','+30','GREEK',1),(83,'GUADELOUPE','+590','GUANDELOUPIAN',1),(84,'GUATEMALA','+502','GUATEMALAN',1),(85,'GUAM','+1671','GUAMANIAN',1),(86,'GUINEA-BISSAU','+245','GUINEA-BISSAUAN',1),(87,'GUYANA','+592','GUYANESE',1),(88,'HONG KONG','+852','HONGKONGER',1),(89,'HONDURAS','+504','HONDURAN',1),(90,'CROATIA','+385','CROATIAN',1),(91,'HAITI','+509','HAITIAN',1),(92,'HUNGARY','+36','HUNGARIAN',1),(93,'INDONESIA','+62','INDONESIAN',1),(94,'IRELAND','+353','IRISH',1),(95,'ISRAEL','+972','ISRAELI',1),(96,'ISLE OF MAN','+44','MANX',1),(97,'INDIA','+91','INDIAN',1),(98,'IRAQ','+964','IRAQI',1),(99,'IRAN, ISLAMIC REPUBLIC OF','+98','IRANIAN',1),(100,'ICELAND','+354','ICELANDER',1),(101,'ITALY','+39','ITALIAN',1),(102,'JAMAICA','+1876','JAMAICAN',1),(103,'JORDAN','+962','JORDANIAN',1),(104,'JAPAN','+81','JAPANESE',1),(105,'KENYA','+254','KENYAN',1),(106,'KYRGYZSTAN','+996','KYRGYZ',1),(107,'CAMBODIA','+855','CAMBODIAN',1),(108,'KIRIBATI','+686','I-KIRIBATI',1),(109,'COMOROS','+269','COMORAN',1),(110,'SAINT KITTS AND NEVIS','+1869','KITTITIAN',1),(111,'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF','+850','NORTH KOREAN',1),(112,'KOREA, REPUBLIC OF','+82','KOREAN',1),(113,'KUWAIT','+965','KUWAITI',1),(114,'CAYMAN ISLANDS','+1345','CAYMANIAN',1),(115,'KAZAKHSTAN','+7','KAZAKHSTANI',1),(116,'LAO PEOPLE\'S DEMOCRATIC REPUBLIC','+856','LAOS',1),(117,'LEBANON','+961','LEBANESE',1),(118,'SAINT LUCIA','+1758','SAINT LUCIAN',1),(119,'LIECHTENSTEIN','+423','LIECHTENSTEINER',1),(120,'SRI LANKA','+94','SRI LANKAN',1),(121,'LIBERIA','+231','LIBERIAN',1),(122,'LESOTHO','+266','BASOTHO',1),(123,'LITHUANIA','+370','LITHUANIAN',1),(124,'LUXEMBOURG','+352','LUXEMBOURGER',1),(125,'LATVIA','+371','LATVIAN',1),(126,'LIBYAN ARAB JAMAHIRIYA','+218','LIBYAN',1),(127,'MOROCCO','+212','MOROCCAN',1),(128,'MONACO','+377','MONACAN',1),(129,'MOLDOVA, REPUBLIC OF','+373','MOLDOVAN',1),(130,'MONTENEGRO','+382','MONTENEGRIN',1),(131,'SAINT MARTIN','+1599','SAINT-MARTINOISE',1),(132,'MADAGASCAR','+261','MALAGASY',1),(133,'MARSHALL ISLANDS','+692','MARSHALLESE',1),(134,'MARTINIQUE','+596','MARTINIQUAISE',1),(135,'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF','+389','MACEDONIAN',1),(136,'MALI','+223','MALIAN',1),(137,'MYANMAR','+95','BURMESE',1),(138,'MONGOLIA','+976','MONGOLIAN',1),(139,'MACAU','+853','MACANESE',1),(140,'NORTHERN MARIANA ISLANDS','+1670','NORTHERN MARIANANS',1),(141,'MAURITANIA','+222','MAURITANIAN',1),(142,'MONTSERRAT','+1664','MONTSERRATIAN',1),(143,'MALTA','+356','MALTESE',1),(144,'MAURITIUS','+230','MAURITANIAN',1),(145,'MALDIVES','+960','MALDIVAN',1),(146,'MALAWI','+265','MALAWIAN',1),(147,'MEXICO','+52','MEXICAN',1),(148,'MALAYSIA','+60','MALAYSIAN',1),(149,'MOZAMBIQUE','+258','MOZAMBICAN',1),(150,'NAMIBIA','+264','NAMIBIAN',1),(151,'NEW CALEDONIA','+687','NEW CALEDONIAN',1),(152,'NIGER','+227','NIGERIEN',1),(153,'NIGERIA','+234','NIGERIAN',1),(154,'NICARAGUA','+505','NICARAGUAN',1),(155,'NETHERLANDS','+31','DUTCH',1),(156,'NORWAY','+47','NORWEGIAN',1),(157,'NEPAL','+977','NEPALESE',1),(158,'NAURU','+674','NAURUAN',1),(159,'NIUE','+683','NIUEAN',1),(160,'NEW ZEALAND','+64','NEW ZEALANDER',1),(161,'OMAN','+968','OMANI',1),(162,'PALESTINE','+970','PALESTINIAN',1),(163,'PANAMA','+507','PANAMANIAN',1),(164,'PERU','+51','PERUVIAN',1),(165,'FRENCH POLYNESIA','+689','FRENCH POLYNESIAN',1),(166,'PAPUA NEW GUINEA','+675','PAPUA NEW GUINEAN',1),(167,'PHILIPPINES','+63','FILIPINO',1),(168,'PAKISTAN','+92','PAKISTANI',1),(169,'POLAND','+48','POLISH',1),(170,'SAINT PIERRE AND MIQUELON','+508','SAINT-PIERRAISE',1),(171,'PITCAIRN ISLAND','+870','PITCAIRN ISLANDER',1),(172,'PUERTO RICO','+1','PUERTO RICAN',1),(173,'PORTUGAL','+351','PORTUGUESE',1),(174,'PALAU','+680','PALAUAN',1),(175,'PARAGUAY','+595','PARAGUAYAN',1),(176,'QATAR','+974','QATARI',1),(177,'ROMANIA','+40','ROMANIAN',1),(178,'REUNION','+262','REUNIONESE',1),(179,'SERBIA','+381','SERBIAN',1),(180,'RUSSIAN FEDERATION','+7','RUSSIAN',1),(181,'RWANDA','+250','RWANDAN',1),(182,'SAUDI ARABIA','+966','SAUDI',1),(183,'SOLOMON ISLANDS','+677','SOLOMON ISLANDER',1),(184,'SEYCHELLES','+248','SEYCHELLOIS',1),(185,'SUDAN','+249','SUDANESE',1),(186,'SWEDEN','+46','SWEDISH',1),(187,'SINGAPORE','+65','SINGAPOREAN',1),(188,'SAINT HELENA','+290','SAINT HELENIAN',1),(189,'SLOVENIA','+386','SLOVENIAN',1),(190,'SLOVAKIA','+421','SLOVAKIAN',1),(191,'SIERRA LEONE','+232','SIERRA LEONEAN',1),(192,'SAN MARINO','+378','SAN MARINESE',1),(193,'SENEGAL','+221','SENEGALESE',1),(194,'SOMALIA','+252','SOMALI',1),(195,'SURINAME','+597','SURINAMER',1),(196,'SAO TOME AND PRINCIPE','+239','SAO TOMEAN',1),(197,'EL SALVADOR','+503','EL SALVADORIAN',1),(198,'SYRIAN ARAB REPUBLIC','+963','SYRIAN',1),(199,'SWAZILAND','+268','SWAZI',1),(200,'TURKS AND CAICOS ISLANDS','+1649','TURKS AND CAICOS ISLANDERS',1),(201,'CHAD','+235','CHADIAN',1),(202,'TOGO','+228','TOGOLESE',1),(203,'THAILAND','+66','THAI',1),(204,'TAJIKISTAN','+992','TAJIK',1),(205,'TOKELAU','+690','TOKELAUAN',1),(206,'TIMOR-LESTE','+670','EAST TIMORESE',1),(207,'TURKMENISTAN','+993','TURKMEN',1),(208,'TUNISIA','+216','TUNISIAN',1),(209,'TONGA','+676','TONGAN',1),(210,'TURKEY','+90','TURK',1),(211,'TRINIDAD AND TOBAGO','+1868','TRINIDADIAN OR TOBAGONIAN',1),(212,'TUVALU','+688','TUVALUAN',1),(213,'TAIWAN, PROVINCE OF CHINA','+886','TAIWANESE',1),(214,'TANZANIA, UNITED REPUBLIC OF','+255','TANZANIAN',1),(215,'UKRAINE','+380','UKRAINIAN',1),(216,'UGANDA','+256','UGANDAN',1),(217,'UNITED STATES OF AMERICA','+1','AMERICAN',1),(218,'URUGUAY','+598','URUGUAYAN',1),(219,'UZBEKISTAN','+998','UZBEKISTANI',1),(220,'HOLY SEE (VATICAN CITY STATE)','+39','VATICAN CITIZEN',1),(221,'SAINT VINCENT AND THE GRENADINES','+1784','SAINT VICENTIAN',1),(222,'VENEZUELA','+58','VENEZUELAN',1),(223,'VIRGIN ISLANDS, BRITISH','+1284','BRITISH VIRGIN ISLANDER',1),(224,'VIRGIN ISLANDS, U.S.','+1340','U.S VIRGIN ISLANDER',1),(225,'VIETNAM','+84','VIETNAMESE',1),(226,'VANUATU','+678','NI-VANUATU',1),(227,'WALLIS AND FUTUNA','+681','WALLISIAN',1),(228,'WESTERN SAHARA','+212','SAHRAWIS',1),(229,'SAMOA','+685','SAMOAN',1),(230,'KOSOVO','+381','KOSOVAR',1),(231,'YEMEN','+967','YEMENI',1),(232,'MAYOTTE','+262','MAHORAN',1),(233,'SOUTH AFRICA','+27','SOUTH AFRICAN',1),(234,'ZAMBIA','+260','ZAMBIAN',1),(235,'ZIMBABWE','+263','ZIMBABWEAN',1);
/*!40000 ALTER TABLE `unisoft_countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unisoft_evaluation_header`
--

DROP TABLE IF EXISTS `unisoft_evaluation_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unisoft_evaluation_header` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) DEFAULT NULL,
  `player_name` varchar(225) DEFAULT NULL,
  `coach_name` varchar(225) DEFAULT NULL,
  `date` varchar(225) DEFAULT NULL,
  `category` varchar(225) DEFAULT NULL,
  `position` varchar(80) DEFAULT NULL,
  `profile_photo` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unisoft_evaluation_header`
--

LOCK TABLES `unisoft_evaluation_header` WRITE;
/*!40000 ALTER TABLE `unisoft_evaluation_header` DISABLE KEYS */;
INSERT INTO `unisoft_evaluation_header` VALUES (1,1,NULL,'Osbert Agbaga','2020-05-05','Transfer Market','Striker',NULL);
/*!40000 ALTER TABLE `unisoft_evaluation_header` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unisoft_events_and_info`
--

DROP TABLE IF EXISTS `unisoft_events_and_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unisoft_events_and_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `entry_by` int(11) NOT NULL,
  `entry_date` datetime NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unisoft_events_and_info`
--

LOCK TABLES `unisoft_events_and_info` WRITE;
/*!40000 ALTER TABLE `unisoft_events_and_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `unisoft_events_and_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unisoft_income_and_expenses`
--

DROP TABLE IF EXISTS `unisoft_income_and_expenses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unisoft_income_and_expenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_date` date NOT NULL,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `player_id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unisoft_income_and_expenses`
--

LOCK TABLES `unisoft_income_and_expenses` WRITE;
/*!40000 ALTER TABLE `unisoft_income_and_expenses` DISABLE KEYS */;
INSERT INTO `unisoft_income_and_expenses` VALUES (1,'2019-08-14','expenses','0',5,2000,'For upkeep and traing kits'),(2,'2019-02-11','expenses','300',7,5000,'For upkeep and training kit and for Russia Deal'),(3,'2019-08-05','expenses','500',8,5000,'4000gh Amount paid for Russia Trip and 500-Upkeep'),(4,'2020-11-04','expenses','fooorvjfghxhvbhjfdcghvhgfxcghv',7,300,'htxhmbjgfgchchchcgchvhjvkjvbjh');
/*!40000 ALTER TABLE `unisoft_income_and_expenses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unisoft_notes`
--

DROP TABLE IF EXISTS `unisoft_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unisoft_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) DEFAULT NULL,
  `notes` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unisoft_notes`
--

LOCK TABLES `unisoft_notes` WRITE;
/*!40000 ALTER TABLE `unisoft_notes` DISABLE KEYS */;
INSERT INTO `unisoft_notes` VALUES (1,1,'Baller is very skilled');
/*!40000 ALTER TABLE `unisoft_notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unisoft_physical_psycho`
--

DROP TABLE IF EXISTS `unisoft_physical_psycho`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unisoft_physical_psycho` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) DEFAULT NULL,
  `strength` int(11) DEFAULT NULL,
  `speed` int(11) DEFAULT NULL,
  `endurance` int(11) DEFAULT NULL,
  `flexibility` int(11) DEFAULT NULL,
  `coordination` int(11) DEFAULT NULL,
  `laterality` int(11) DEFAULT NULL,
  `agility` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unisoft_physical_psycho`
--

LOCK TABLES `unisoft_physical_psycho` WRITE;
/*!40000 ALTER TABLE `unisoft_physical_psycho` DISABLE KEYS */;
INSERT INTO `unisoft_physical_psycho` VALUES (1,1,5,5,5,5,5,5,5),(2,3,3,3,2,2,5,5,3),(3,3,3,3,2,2,5,5,3);
/*!40000 ALTER TABLE `unisoft_physical_psycho` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unisoft_player_profile`
--

DROP TABLE IF EXISTS `unisoft_player_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unisoft_player_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) DEFAULT NULL,
  `technical_average` int(11) DEFAULT NULL,
  `tactical_average` int(11) DEFAULT NULL,
  `physical_psychomotor_average` int(11) DEFAULT NULL,
  `attitude_average` int(11) DEFAULT NULL,
  `coexistence_average` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unisoft_player_profile`
--

LOCK TABLES `unisoft_player_profile` WRITE;
/*!40000 ALTER TABLE `unisoft_player_profile` DISABLE KEYS */;
INSERT INTO `unisoft_player_profile` VALUES (1,1,3,4,3,4,4),(2,3,3,4,4,4,3),(3,3,3,4,4,4,3);
/*!40000 ALTER TABLE `unisoft_player_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unisoft_players`
--

DROP TABLE IF EXISTS `unisoft_players`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unisoft_players` (
  `player_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `sex` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nationality` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `marital_status` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `height` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `passport_no` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `preferred_position` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_position` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `current_club` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contract_expiration` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level_played` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `registration_type` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `playing_foot` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `health` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `player_profile` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `right_foot` float NOT NULL,
  `left_foot` float NOT NULL,
  `speed` float NOT NULL,
  `athleticism` float NOT NULL,
  `tactics` float NOT NULL,
  `hard_work` float NOT NULL,
  `teamwork` float NOT NULL,
  `adaptability` float NOT NULL,
  `personality` float NOT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `other_phone` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `player_number` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contract` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_joined` date NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `status_date` date NOT NULL,
  `status_comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `entry_by` int(11) NOT NULL,
  `entry_date` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`player_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unisoft_players`
--

LOCK TABLES `unisoft_players` WRITE;
/*!40000 ALTER TABLE `unisoft_players` DISABLE KEYS */;
INSERT INTO `unisoft_players` VALUES (1,'George','Addy','1996-08-11','Male','GHANAIAN','SINGLE','74kG','1M 86CM','G1582157','player_5ec6795a45b266024.jpeg','Striker','9','Free Agent','None','','Semi Professional','Both feet but right preferred','No history of injury','Excellent skimmer skills, he operates effectively as a game\r\nChanger. He is very flexible, and shoots accurately. He is\r\ngood against one on one situation and has a powerful\r\nShoot on the run. He has also very effective touches and\r\nball possessions and scores goals in almost game situation',92,86,78,83,80,88,83,85,83,'+233542365481','+233261961164','18yardboxltd@gmail.com','House Number 207 Accra - Anyaa','18yrd-20-01-01','18YARDBOX FOOTBALL AGENCY LIMITED AGREEMENT FOR GEORGE ADDY.pdf','2020-01-20',1,'0000-00-00','',1,'2020-05-21 12:51:38',1,'2020-05-21 12:57:34'),(2,'Joe','De-graft Tagoe','2001-07-31','Male','GHANAIAN','SINGLE','65kg','1m 79cm','G2238590','player_5ec691d88d4083719.jpg','Winger','11','18yardbox FC','2024','Division 2','Semi Professional','Both feet but right preferred','No history of injury','Excellent skimmer skills, he operates effectively as a game-changer. He is very flexible and shoots accurately. He is good against one on one situations and has a powerful shot on the run. He has also very effective touches and ball possessions.',99,85,94,99,94,97,99,97,100,'+233261961164','+233542365481','18yardboxltd@gmail.com','House Number 207 Accra - Anyaa','18yrd20-05-002','18YARDBOX FOOTBALL AGENCY LIMITED AGREEMENT FOR JOE DEGRAFT TAGOE.pdf','2018-02-09',1,'0000-00-00','',1,'2020-05-21 14:36:08',1,'2020-09-22 15:52:03'),(3,'Alex','Agyemang','1999-08-23','Male','GHANAIAN','SINGLE','69kg','5.7ft','G2458160','player_5ec6961d2661d2162.jpeg','Defender','4/5','Free Agent','None','Division 2','Semi Professional','Both feet but right preferred','No history of injury','VERY GOOD IN SHOOTING\r\n2. VERY GOOD WITH AREA BALLS\r\n3. GOOD IN MARKING AND GOOD COVERING RESPONSIBILITIES\r\n4. CAN PLAY BOTH LEGS\r\n5. ACCURACY IN PASSING\r\n6. GOOD IN DEFENDING',92,90,91,88,83,91,92,92,87,'+233261961164','+233542365481','18yardboxltd@gmail.com','House Number 207 Accra - Anyaa','18yrd19-08-003','18YARDBOX FOOTBALL AGENCY LIMITED AGREEMENT FOR ALEX AGYEMANG.pdf','2019-10-08',1,'0000-00-00','',1,'2020-05-21 14:54:21',1,'2020-10-21 00:03:56'),(4,'Samuel','Kyei Baffour','2002-08-22','Male','GHANAIAN','SINGLE','65kg','1m 79cm','G1495753','player_5ec69a03d9f6e8053.jpeg','Left back','3','Free Agent','None','Division 3','Amateur','Both feet but left preferred','No history of Injury','Excellent defensive skills, he operates effectively as a game-changer. He is very flexible and shoots accurately. He is good against one on one situation. He has also very effective touches and ball possessions.',76,89,70,61,67,76,77,73,61,'+233261961164','+233542365481','18yardboxltd@gmail.com','House Number 207 Accra - Anyaa','18yrd19-05-004','','2019-05-06',1,'0000-00-00','',1,'2020-05-21 15:10:59',1,'2020-05-21 15:14:18'),(5,'Yusif','Rahman','1993-01-26','Male','GHANAIAN','SINGLE','74kg','1m 86cm','G1736966','player_5ec6ed633c6247955.png','Striker','9','Free Agent','None','Primiership','Professional','Both feet but right preferred','No history of injury','Excellent skimmer skills, he operates effectively as a game-changer. He is very flexible and shoots accurately. He is good against one on one situation and has a powerful shot on the run. He has also very effective touches and ball possessions.',88,79,84,85,91,91,88,80,86,'+233555960678','','','House Number 207 Accra - Anyaa','18yrd19-06-005','18YARDBOX FOOTBALL AGENCY LIMITED AGREEMENT FOR YUSIF RAHMAN.pdf','2019-05-17',4,'2020-02-20','Breach of contract',1,'2020-05-21 21:06:43',1,'2020-05-21 21:09:17'),(6,'Prince','Quarshie','2002-05-19','Male','GHANAIAN','SINGLE','64kg','1m 54cm','G2705711','player_5f0f7da8e50b77164.png','Left Back','2/10','Free Agent','None','Division 2','Semi Professional','Both feet but left preferred','No history of Injury','Very prolific with the ball. Plays both feet and has speed on the run. Accurate shooting and quick decision making in one on one situations.',69,81,85,80,74,70,88,75,89,'+233261961164','+233549245306','18yardboxltd@gmail.com','House Number 207 Accra - Anyaa','18yrd19-06-006','18YARDBOX FOOTBALL AGENCY LIMITED AGREEMENT FOR PRINCE QUARSHIE.pdf','2019-05-29',1,'0000-00-00','',1,'2020-05-23 05:46:13',1,'2020-07-15 22:05:28'),(7,'Francis','Sulley Mohammed','2002-09-16','Male','GHANAIAN','SINGLE','67kg','1m 86cm','G1031509','player_5ef8a85c38b353286.jpeg','Goal keeper','1','18yardbox Football Club','None','Division 2','Semi Professional','Both feet but right prefferred','No history of injury','A prolific player by all standards with, powerful kick for both feet. Quick reflexes, Long throws. Good timing & Precision. Also fast, Active, and creative on the field. Good jumping ability, fantastic co-ordination, excellent distribution, solid catching, wonderful agility, strong punching, and fast reflexes.',92,90,89,89,91,93,92,94,92,'+233261961164','','18yardboxltd@gmail.com','House Number 207 Accra - Anyaa','18yrd19-11-007','18YARDBOX FOOTBALL AGENCY LIMITED AGREEMENT FOR FRANCIS SULLEY MOHAMMED.pdf','2019-11-07',1,'0000-00-00','',1,'2020-05-23 06:09:39',1,'2020-10-27 05:50:35'),(8,'Herod Nii Bentum','Quayfio','1996-01-17','Male','GHANAIAN','SINGLE','72kg','6.12ft','G2929631','player_5ecd7c7a2144c6045.jpeg','GoalKeeper','1','Free Agent','None','Premiership','Professional','Both feet but right preferred','No history of injury','Excellent timing, Strong grip on the ball, Long throws, powerful kicks with both legs. Good with Arial balls, impeccable diving skill and strong with one on one situations.',96,89,90,95,91,95,93,94,93,'+233261961164','+233262105663','18yardboxltd@gmail.com','House Number 207 Accra - Anyaa','18yrd20-01-008','18YARDBOX FOOTBALL AGENCY LIMITED AGREEMENT FOR HEROD NII QUAYFIO BENTUM.pdf','2020-01-09',1,'2020-11-04','Sold to Westerlo KVC in Belgium in 2020 at 300,000 euros.  Signing fee 150,000 euros. The contract comes with accommodation & Car',1,'2020-05-26 20:30:50',1,'2020-10-31 14:55:27'),(9,'Samuel Okyere','Keelson','2000-09-23','Male','GHANAIAN','SINGLE','75kg','6ft','G2878096','player_5f3cc45432cb13930.png','Central Back','5','18yardbox FC','2024','Division 2','Semi Professional','Both feet but right foot preffered','No health condition','He is a very impressive distributor of the ball. He is right-footed and can distribute to all areas of the pitch with accuracy. He is also constantly looking for opportunities to pass vertically and help his team to progress into the opposition half. Also good at a one-on-one situation in the 18yardbox.',100,92,93,95,97,90,94,91,93,'+233261961164','','18yardboxltd@gmail.com','House Number 207 Accra - Anyaa','18yrd20-01-009','18YARDBOX FOOTBALL AGENCY LIMITED AGREEMENT FOR SAMUEL OKYERE KEELSON.pdf','2020-02-03',1,'0000-00-00','',1,'2020-08-19 06:19:00',1,'2020-10-20 23:53:08'),(10,'Joseph','Annor','2002-10-10','Male','GHANAIAN','SINGLE','75KG','153CM','G1577867','player_5f8ea73d5c1c66729.jpeg','Attacking Midfielder','8','18yardbox FC','None','Division 3','Semi Professional','Both feet but right preffered','No history of Injury','He is a very impressive distributor of the ball. He is right-footed and can distribute to all areas of the pitch with accuracy. He is also constantly looking for opportunities to pass vertically and help his team to progress into the opposition half. Also good at a one-on-one situation in the 18yardbox.',85,77,86,86,87,90,90,87,82,'+233261961164','','18yardboxltd@gmail.com','House Number 207 Accra - Anyaa','18yrd20-01-010','','2020-01-06',1,'0000-00-00','',1,'2020-09-18 01:12:21',1,'2020-10-20 09:00:45'),(11,'Raymond','Baffoe-Hammond','2002-07-28','Male','GHANAIAN','SINGLE','74kg','160cm','G2338596','','Defender','4/5','18yardbox FC','2025','Division 3','Semi Professional','both feet but right preffered','No history of injury','Excellent defending skills, he operates effectively in a game. He is very flexible and shoots accurately. He is good against one on one situation and has a powerful Shoot on the run. He has also very effective touches and ball possessions.',93,81,90,89,93,92,90,81,85,'+233261961164','','18yardboxltd@gmail.com','House Number 207 Accra - Anyaa','18yrd20-01-011','','2020-01-06',1,'0000-00-00','',1,'2020-09-18 01:28:24',0,'0000-00-00 00:00:00'),(12,'Ebenezer','Nyamador','1998-04-16','Male','GHANAIAN','SINGLE','76kg','173cm','G1289199','player_5f6a18309cc088320.jpeg','Offensive Midfielder','10','Ahafoman Football Club','2022','Division 2','Semi Professional','Both Feet but right preffered','No history of Injury','A complete technical player with pace, good combination of both legs effectively. Endurance, very skilful, with the ability to be universal player and adapt to any position. Experience knowledge of the game with excellent ball handling and accurate last passing. \r\nAggressive, good penetrative passes, long balls and a good air ball timing. A tireless competitor who always work hard productively towards achieving a teamâ€™s objectives both on the field and out of the field \r\n\r\nOBJECTIVES\r\nMy passion is football. I am completely motivated in becoming the best player I can be. My main goal is to be the best and most useful player on any team I play for. I am a hard worker with a mentality very strong and a very coachable player. I get along and respect all my teammates. I have had experience for playing in many different countries and against many different teams. I use that experience to better myself and my teammates. \r\n\r\nTechnical & Tactical Performance (TTP) â€“ Maximum Point = 10 \r\nSpeed   	=  	 	        9 \r\nShot   	=  	 	        8 \r\nAccuracy  	=  	 	9 \r\nTeamwork  	= 	 	9 \r\nDiscipline  	= 	 	10 \r\nTiming  	=  	 	        9 \r\nEndurance  	= 	 	8',100,84,91,90,93,97,96,90,96,'+233261961164','+233243961523','18yardboxltd@gmail.com','House Number 207 Accra - Anyaa','18yrd20-01-012','18YARDBOX FOOTBALL AGENCY LIMITED AGREEMENT FOR EBENEZER NYAMADOR.pdf','2020-02-10',1,'0000-00-00','',1,'2020-09-22 15:28:48',0,'0000-00-00 00:00:00'),(13,'Isaac','Essien','2000-12-04','Male','GHANAIAN','SINGLE','74KG','6FT','G2034975','player_5f7747c9ded305466.jpeg','Striker','9','','','Division 2','Semi Professional','Both feet but right preferred','No history of Injury','A complete technical player with pace, a good combination of both legs effectively. Endurance, very skillful, with the ability to be a universal player and adapt to any position. Experience knowledge of the game with excellent ball-handling and accurate last passing. Aggressive, good penetrative passes, long balls, and a good air ball timing. A tireless competitor who always work hard productively towards achieving a team\'s objectives both on the field and out of the field.',96,90,96,95,95,92,93,97,79,'+233261961164','','18yardboxltd@gmail.com','House Number 207 Accra - Anyaa','18yrd20-01-013','','2020-10-01',1,'0000-00-00','',1,'2020-10-02 15:31:21',0,'0000-00-00 00:00:00'),(14,'Nana Kweku','Tutu','2000-05-06','Male','GHANAIAN','SINGLE','65KG','6ft','G2506601','','Midfielder','6 / 8','18yardbox FC','2024','Division 2','Semi Professional','Both feet but right preferred','No history of Injury','Excellent skimmer skills, he operates effectively as a game-changer. He is very flexible and shoots accurately. He is good against one on one situation and has a powerful\r\nShoot on the run. He has also very effective touches and ball possessions.',96,91,90,93,93,96,96,97,96,'+233277091710','','18yardboxltd@gmail.com','House Number 207 Accra - Anyaa','18yrd20-01-014','18YARDBOX FOOTBALL AGENCY LIMITED AGREEMENT FOR NANA KWEKU TUTU.pdf','2020-07-15',1,'0000-00-00','',1,'2020-10-19 22:53:20',1,'2020-10-20 23:57:03');
/*!40000 ALTER TABLE `unisoft_players` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unisoft_players_achievements`
--

DROP TABLE IF EXISTS `unisoft_players_achievements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unisoft_players_achievements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `entry_by` int(11) NOT NULL,
  `entry_date` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unisoft_players_achievements`
--

LOCK TABLES `unisoft_players_achievements` WRITE;
/*!40000 ALTER TABLE `unisoft_players_achievements` DISABLE KEYS */;
INSERT INTO `unisoft_players_achievements` VALUES (1,1,'Won the highest goal scorer ever in the history of the club Player award',1,'2020-05-21 12:51:38',0,'0000-00-00 00:00:00'),(2,2,'Won the Excellence Performers Player award in the Zaytuna FC. Believer United FC 2014-2015). Discovery of the year Award (Zaytuna FC) and Best Player (Zaytuna United- 2017-2018).',1,'2020-05-21 14:36:08',0,'0000-00-00 00:00:00'),(3,3,'Alex Agyemang is a hard-working player that takes his training serious, also he is charitable and respectful. he is a disciplined and outstanding player. Won the best defender.',1,'2020-05-21 14:54:21',0,'0000-00-00 00:00:00'),(4,4,'Won the Excellence Performers Player award in the MTN Soccer Academy Season 3',1,'2020-05-21 15:10:59',0,'0000-00-00 00:00:00'),(6,6,'Top Assist Player 2019 Season with 14 assists and 8 goals to his credit.',1,'2020-07-01 15:37:26',0,'0000-00-00 00:00:00'),(7,11,'Best defender in the 2019 - Ghana Zonal League',1,'2020-09-18 01:28:24',0,'0000-00-00 00:00:00'),(8,2,'Most Discipline player 2019',1,'2020-09-22 15:50:49',0,'0000-00-00 00:00:00'),(9,14,'Won the Excellence Performers Player award in the MTN Soccer Academy Season 2',1,'2020-10-19 22:53:20',0,'0000-00-00 00:00:00'),(10,8,'Best goalkeeper from 2016 in the Ghana Premiership.',1,'2020-10-31 14:51:27',0,'0000-00-00 00:00:00');
/*!40000 ALTER TABLE `unisoft_players_achievements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unisoft_players_career_path`
--

DROP TABLE IF EXISTS `unisoft_players_career_path`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unisoft_players_career_path` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) NOT NULL,
  `period` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `club` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `matches` int(11) NOT NULL,
  `goals` int(11) NOT NULL,
  `club_level` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entry_by` int(11) NOT NULL,
  `entry_date` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unisoft_players_career_path`
--

LOCK TABLES `unisoft_players_career_path` WRITE;
/*!40000 ALTER TABLE `unisoft_players_career_path` DISABLE KEYS */;
INSERT INTO `unisoft_players_career_path` VALUES (1,1,'2012-2014','Eleven Wise FC','Ghana',30,17,'Division 1',1,'2020-05-21 12:51:38',0,'0000-00-00 00:00:00'),(2,1,'2015-2016','Proud United','Ghana',19,9,'Division 2',1,'2020-05-21 12:51:38',0,'0000-00-00 00:00:00'),(3,1,'2015-2017','Anoited FC','Ghana',28,15,'Division 2',1,'2020-05-21 12:51:38',0,'0000-00-00 00:00:00'),(4,1,'2017-2019','Birem United','Ghana',19,19,'Division 2',1,'2020-05-21 12:51:38',0,'0000-00-00 00:00:00'),(5,2,'2016-2017','Zaytuna FC','Ghana',35,15,'Division 1',1,'2020-05-21 14:36:08',0,'0000-00-00 00:00:00'),(6,3,'2006','MISSION RANGERâ€™S F/C','Ghana',0,0,'UNDER 12',1,'2020-05-21 14:54:21',0,'0000-00-00 00:00:00'),(7,3,'2010','TOGO ETOIL POLE F/C','Ghana',0,0,'2ND DIVISION',1,'2020-05-21 14:54:21',0,'0000-00-00 00:00:00'),(8,3,'2012','MAWULI SOCCER ACADEMY','Ghana',0,0,'2ND DIVISION',1,'2020-05-21 14:54:21',0,'0000-00-00 00:00:00'),(9,3,'2014','ABOR COMBONI STARS','Ghana',0,0,'2ND DIVISION',1,'2020-05-21 14:54:21',0,'0000-00-00 00:00:00'),(10,3,'2016','JUVENIUS F/C','Ghana',0,0,'2ND DIVISION',1,'2020-05-21 14:54:21',0,'0000-00-00 00:00:00'),(11,4,'2015-2016','Fayenod Academy','Ghana',40,9,'U12',1,'2020-05-21 15:10:59',0,'0000-00-00 00:00:00'),(12,4,'2016-2017','UniStar FC','Ghana',25,18,'Division 3',1,'2020-05-21 15:10:59',0,'0000-00-00 00:00:00'),(13,4,'2017-2018','Ghanaba Soccer Academy','Ghana',18,8,'Division 2',1,'2020-05-21 15:10:59',0,'0000-00-00 00:00:00'),(14,5,'2011-2014','Alki Larnakas','Cyprus',14,1,'Division 1',1,'2020-05-21 21:06:43',0,'0000-00-00 00:00:00'),(15,5,'2014-2016','Paeek','Cyprus',41,14,'Division 2',1,'2020-05-21 21:06:43',0,'0000-00-00 00:00:00'),(16,5,'2014-2014','Black Starlet','Ghana',2,0,'National Assignment',1,'2020-05-21 21:06:43',0,'0000-00-00 00:00:00'),(17,5,'2014-2016','Ethnikos Latsion','Cyprus',5,0,'Division 1',1,'2020-05-21 21:06:43',0,'0000-00-00 00:00:00'),(18,5,'2016-2017','P O Ormideias FC','Cyprus',15,4,'Division 3',1,'2020-05-21 21:06:43',0,'0000-00-00 00:00:00'),(19,7,'2018-2019','Ekumfi United','Ghana',2,0,'Division 2',1,'2020-05-23 06:09:39',0,'0000-00-00 00:00:00'),(20,7,'2017-2018','Global United','Ghana',13,5,'Division3',1,'2020-05-23 06:09:39',0,'0000-00-00 00:00:00'),(21,6,'2017 - 2019','18yardbox FC','Ghana',8,5,'Division 3',1,'2020-07-15 22:05:28',0,'0000-00-00 00:00:00'),(22,9,'2016-2019','Emmanuel City','Ghana',15,4,'Division 2',1,'2020-08-19 06:26:50',0,'0000-00-00 00:00:00'),(23,10,'2018-2019','18yardbox Fc','Ghana',8,3,'Division 3',1,'2020-09-18 01:12:21',0,'0000-00-00 00:00:00'),(24,11,'2019-2020','18yardbox FC','Ghana',6,2,'Division 3',1,'2020-09-18 01:28:24',0,'0000-00-00 00:00:00'),(25,9,'2018-2020','18yardbox FC','Ghana',10,4,'Division 3',1,'2020-09-18 01:37:21',0,'0000-00-00 00:00:00'),(26,12,'2009-2012','Blessed Assurance FC','Ghana',13,8,'Ghanaian Juvenile League',1,'2020-09-22 15:28:48',0,'0000-00-00 00:00:00'),(27,12,'2014-2015','Ghana National U17','Ghana',3,0,'CAF Confederations Qualifiers',1,'2020-09-22 15:28:48',0,'0000-00-00 00:00:00'),(28,12,'2012-2016','Aspire Football Academy','Qatar',8,3,'Academy',1,'2020-09-22 15:28:48',0,'0000-00-00 00:00:00'),(29,12,'2016-2017','Fc FamalicÃ£o U19','Portugal',12,4,'U19',1,'2020-09-22 15:28:48',0,'0000-00-00 00:00:00'),(30,12,'2018 â€“ 2022','Ahafoman Football Club','Ghana',16,9,'Division 2',1,'2020-09-22 15:28:48',0,'0000-00-00 00:00:00'),(31,2,'2018-2024','18yardbox Football Club','Ghana',14,9,'Division 3',1,'2020-09-22 15:50:49',0,'0000-00-00 00:00:00'),(32,7,'2019-2024','18yardbox FC','Ghana',15,0,'Division 3',1,'2020-10-01 14:59:48',0,'0000-00-00 00:00:00'),(33,14,'2014-2015','Real Ebony Fc','Ghana',35,15,'Juvenile',1,'2020-10-19 22:53:20',0,'0000-00-00 00:00:00'),(34,14,'2015-2016','Liberty FC','Ghana',40,9,'U20',1,'2020-10-19 22:53:20',0,'0000-00-00 00:00:00'),(35,14,'2016-2017','Freedom Fighters','Ghana',25,10,'Division 3',1,'2020-10-19 22:53:20',0,'0000-00-00 00:00:00'),(36,14,'2018-2024','18yardbox FC','Ghana',18,9,'Division 3',1,'2020-10-19 22:53:20',0,'0000-00-00 00:00:00'),(37,8,'2013-2015','WAFA','Ghana',0,0,'Division 1',1,'2020-10-28 21:24:13',0,'0000-00-00 00:00:00'),(38,8,'2015-2019','Accra Great Olympics','Ghana',0,0,'Premiership',1,'2020-10-28 21:24:13',0,'0000-00-00 00:00:00'),(39,8,'2012','National Call UP','U-17',0,0,'National Team',1,'2020-10-28 21:24:13',0,'0000-00-00 00:00:00'),(40,8,'2017','National Call UP','Black Stars',0,0,'World cup Qualifiers',1,'2020-10-28 21:24:13',0,'0000-00-00 00:00:00'),(41,8,'2003-2012','Salami Rangers','Ghana',0,0,'Juvenile',1,'2020-10-28 21:24:13',0,'0000-00-00 00:00:00');
/*!40000 ALTER TABLE `unisoft_players_career_path` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unisoft_tactical_eval`
--

DROP TABLE IF EXISTS `unisoft_tactical_eval`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unisoft_tactical_eval` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) DEFAULT NULL,
  `positioning_on_the_field` int(11) DEFAULT NULL,
  `combination_game` int(11) DEFAULT NULL,
  `decision_making` int(11) DEFAULT NULL,
  `offensive_tactics_overall_evaluation` int(11) DEFAULT NULL,
  `defensive_tactics_overall_evaluation` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unisoft_tactical_eval`
--

LOCK TABLES `unisoft_tactical_eval` WRITE;
/*!40000 ALTER TABLE `unisoft_tactical_eval` DISABLE KEYS */;
INSERT INTO `unisoft_tactical_eval` VALUES (1,1,2,2,2,2,3),(2,3,3,2,3,2,3),(3,3,3,2,3,2,3);
/*!40000 ALTER TABLE `unisoft_tactical_eval` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unisoft_technical_eval`
--

DROP TABLE IF EXISTS `unisoft_technical_eval`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unisoft_technical_eval` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) DEFAULT NULL,
  `ball_skills` int(11) DEFAULT '0',
  `heading` int(11) DEFAULT '0',
  `pass` int(11) DEFAULT '0',
  `dribble` int(11) DEFAULT '0',
  `carrying_the_ball` int(11) DEFAULT '0',
  `control` int(11) DEFAULT '0',
  `shooting` int(11) DEFAULT '0',
  `skills` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unisoft_technical_eval`
--

LOCK TABLES `unisoft_technical_eval` WRITE;
/*!40000 ALTER TABLE `unisoft_technical_eval` DISABLE KEYS */;
INSERT INTO `unisoft_technical_eval` VALUES (1,1,3,5,5,3,2,4,4,1),(2,2,1,1,1,1,1,1,1,1),(3,3,1,1,1,1,1,1,1,1);
/*!40000 ALTER TABLE `unisoft_technical_eval` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unisoft_users`
--

DROP TABLE IF EXISTS `unisoft_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unisoft_users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `photo` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sex` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name` (`username`),
  KEY `user_role_id` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unisoft_users`
--

LOCK TABLES `unisoft_users` WRITE;
/*!40000 ALTER TABLE `unisoft_users` DISABLE KEYS */;
INSERT INTO `unisoft_users` VALUES (1,'','Administrator','Male',1,'admin','$2y$10$T1Xs1JmyLyPUbyQs9WzFMef3wLX0OmlgPLqub51RtmKYKiHem0k32',1),(3,'','Administrator','Male',1,'penny','$2y$10$Bv2g4yk2WwnKM0rwqejZweXsk0rWfQB9i4YS59XNPPeAqVJmaNJYC',1);
/*!40000 ALTER TABLE `unisoft_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-20  3:05:14

<?php

$__settings = [];
$__settings['country_details'] = [
	'name' 		   => 'GHANA',
	'country_code' => '+233',
	'nationality'  => 'GHANAIAN',
	'initials' 	   => 'GH',
	'timezone' 	   => 'Africa/Accra'
];

$marital_statuses = [
	'SINGLE',
	'MARRIED',
	'DIVORCED',
	'WIDOWED'
];

$player_statuses = [
	"1" => ["name"=>"Active", "class"=>"success"],
	"2" => ["name"=>"Suspended", "class"=>"warning"],
	"3" => ["name"=>"Sold", "class"=>"danger"],
	"4" => ["name"=>"Terminated", "class"=>"red"],
	"5" => ["name"=>"Inactive", "class"=>"gray"],
];

$agent_player_statuses = [
	"1" => ["name"=>"Active", "class"=>"success"],
	"2" => ["name"=>"Terminated", "class"=>"red"],
	"3" => ["name"=>"Expired", "class"=>"danger"],
];

$user_statuses = [
	"0" => ["name" 	=>"Inactive", "class" =>"danger"],
	"1" => ["name" 	=>"Active", "class" =>"success"]
];

$user_roles = [
	"1" => 'Super Administrator',
	"2" => 'Player'
];
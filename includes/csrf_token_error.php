<?php

/**
 * @ page dependencies
*/
$page_dependencies = new \stdClass;

$page_dependencies->page_title = 'Page Expired';
$page_dependencies->breadcrumb = [];
$page_dependencies->plugins = [];
$page_dependencies->js  = [];
// end of dependencies


?>
<!DOCTYPE html> <html lang="en"> <head> <?php include DOCUMENT_ROOT.'includes/partials/head.php'; ?> <meta http-equiv="refresh" content="5"> </head> <body> <h2 class="text-center mt-5 mx-2"> The page has expired due to inactivity<br> Please refresh and try again </h2> <h5 class="text-center font-weight-lighter">Refreshing ........ <span id="refresh-time">5</span></h5> <script type="text/javascript"> var h5 = document.getElementById('refresh-time'); var t  = document.querySelector('meta[http-equiv="refresh"]').getAttribute('content'); setInterval(function(){t--; h5.innerHTML = t;}, 1000); </script> </body> </html>
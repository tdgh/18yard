<?php

namespace UNISOFT\SMS\VALIDATOR;

class VALIDATOR{
	public $prefix;

	public function __construct($prefix){
		$this->prefix = $prefix;
	}

	public function is_set($name){
		return isset($_POST[$this->prefix.$name]);
	}

	public function ele($name, $trim=true){
		return $trim ? trim($_POST[$this->prefix.$name]) : $_POST[$this->prefix.$name];
	}

	public function value($name, $def='', $trim=true){
		return $this->is_set($name) ? $this->ele($name, $trim) : $def;
	}

	public function error($name, $error_text){
		global $errors;
		
		$errors[$this->prefix.$name] = $error_text;
	}

	// validate

	public function required($name){
		global $errors;

		if( !$this->is_set($name) || ($this->is_set($name) && $this->ele($name) == '') ){
			$this->error($name, 'This field is required');
		}

		return array_key_exists($name, $errors) ? true : false;
	}

	public function name($name, $required=false){
		global $errors;

		if( !$required && $this->value($name) == '' ){
			// do nothing
		}else if( $required && $this->required($name) ){
			// push error
		}else if( !preg_match("/^[a-zA-Z\.\-,& ]{3,}$/", $this->ele($name)) ){
			$this->error($name, 'A valid name is required');
		}

		return array_key_exists($name, $errors) ? true : false;
	}

	public function phone($name, $required=false){
		global $errors;

		if( !$required && $this->value($name) == '' ){
			// do nothing
		}else if( $required && $this->required($name) ){
			// push error
		}else if( !preg_match('/^(\+)?([\d ]){8,15}$/', $this->ele($name)) ){
			$this->error($name, 'A valid phone number is required');
		}

		return array_key_exists($name, $errors) ? true : false;
	}

	public function email($name, $required=false){
		global $errors;

		if( !$required && $this->value($name) == '' ){
			// do nothing
		}else if( $required && $this->required($name) ){
			// push error
		}else if( !filter_var($this->ele($name), FILTER_VALIDATE_EMAIL) ){
			$this->error($name, 'A valid email is required');
		}

		return array_key_exists($name, $errors) ? true : false;
	}

	public function username($name, $required=false, $id=0){
		global $connect, $errors;

		if( !$required && $this->value($name) == '' ){
			// do nothing
		}else if( $required && $this->required($name) ){
			// push error
		}else if( !preg_match("/^[a-zA-Z0-9\.\-,& ]{3,}$/", $this->ele($name)) ){
			$this->error($name, 'A valid username is required');
		}else{
			$id = intval($id);

			$s = $connect->prepare("SELECT COUNT(*) FROM unisoft_users WHERE username=:username AND id!=$id");
			$s->execute(['username'=>$this->ele($name)]);

			if( $s->fetch()[0] > 0 ) $errors['username'] = 'Username already exist';
		}

		return array_key_exists($name, $errors) ? true : false;
	}

	public function password($name, $required=false){
		global $errors;

		if( !$required && $this->value($name) == '' ){
			// do nothing
		}else if( $required && $this->required($name) ){
			// push error
		}else if( !preg_match('/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[?!@#$%\^&*()_\-+=]).{8,}$/', $this->value($name, '', false)) ){
			$this->error($name, 'A strong password is required');
		}

		return array_key_exists($name, $errors) ? true : false;
	}

	public function confirm_password($name, $name_pass){
		global $errors;

		if( $this->value($name, '', false) !== $this->value($name_pass, '', false) ){
			$this->error($name, 'Password does not match');
		}

		return array_key_exists($name, $errors) ? true : false;
	}

	public function if_exist($name, $table, $conditionals=[], $required=false){
		global $connect, $errors;

		if( !$required && $this->value($name) == '' ){
			// do nothing
		}else if( $required && $this->required($name) ){
			// push error
		}else{
			$stmt = query($table, 'COUNT(*)', $conditionals);
			if( $stmt == 0 ) $this->error($name, 'Please select a valid option');
		}

		return array_key_exists($name, $errors) ? true : false;
	}

	public function if_in_array($name, $array, $required=false){
		global $errors;

		if( !$required && $this->value($name) == '' ){
			// do nothing
		}else if( $required && $this->required($name) ){
			// push error
		}else if( !in_array($this->value($name), $array) ){
			$this->error($name, 'Please select a valid option');
		}

		return array_key_exists($name, $errors) ? true : false;
	}

	// file
	public function is_uploaded($name){
		return 
			!empty($_FILES) && 
			isset($_FILES[$this->prefix.$name]['tmp_name']) && 
			is_uploaded_file($_FILES[$this->prefix.$name]['tmp_name']) && 
			file_exists($_FILES[$this->prefix.$name]['tmp_name']);
	}

	public function mime_types($type){
		switch ($type){
			case 'img':
				$value = ['image/jpeg','image/png','image/gif'];
				break;
			case 'csv':
				$value = ['text/csv','application/vnd.ms-excel'];
				break;
			case 'zip':
				$value = ['application/x-zip-compressed'];
				break;
			case 'pdf':
				$value = ['application/pdf'];
				break;
			default:
				$value = [];
				break;
		}

		return $value;
	}

	// image
	public function is_image($name){
		return 
			$this->is_uploaded($name) && 
			in_array($_FILES[$this->prefix.$name]['type'], $this->mime_types('img'));
	}

	public function file_name($name, $dest, $new_fname='', $idx=0){
		$new_fname = preg_replace('/[^a-zA-Z0-9]/', ' ', strtolower($new_fname));
		$new_fname = preg_replace('/\s\s+/', ' ', $new_fname);
		$new_fname = preg_replace('/\s/', '_', $new_fname);
		$new_fname = empty($new_fname) ? uniqid() : substr($new_fname, 0, 200);

		$files = $_FILES[$this->prefix.$name]['name'];
		$file = is_array($files) ? $files[$idx] : $files;

		$fileinfo = pathinfo($file);
	    $filename = empty($new_fname) ? $fileinfo['filename'] : $new_fname;
	    $basename = $filename.'.'.$fileinfo['extension'];
	    $filepath = DOCUMENT_ROOT.$dest.'/'.$basename;

	    while(file_exists($filepath)){
	        $basename = $filename.'-'.uniqid().'.'.$fileinfo['extension'];
	    	$filepath = DOCUMENT_ROOT.$dest.'/'.$basename;
	    }

	    return  $basename;
	}

	public function img_upload($name, $dest, $width, $height, $new_fname='', $auto_crop=''){
		if( $this->is_image($name) ){
			$basename = $this->file_name($name, $dest, $new_fname);
			$filepath = DOCUMENT_ROOT.$dest.'/'.$basename;

			if( isset($_POST[$this->prefix.$auto_crop]) && !empty($auto_crop) ){
				$crop_file = __DIR__.DIRECTORY_SEPARATOR.'crop_image.class.php';

				if( !array_search($crop_file, get_required_files()) ){
					require __DIR__.'/crop_image.class.php';
				}

				$config["size"]  	   = $width;
				$config["size_height"] = $height;
				$config["destination"] = $filepath;
				$config["file_data"]   = $_FILES[$this->prefix.$name];

				$im = new \ImageResize($config);
				$im->resize();
			}else{
				move_uploaded_file($_FILES[$this->prefix.$name]['tmp_name'], $filepath);
			}

			return $basename;
		}else{
			return '';
		}
	}

	// csv
	public function is_csv($name){
		return 
			$this->is_uploaded($name) && 
			in_array($_FILES[$this->prefix.$name]['type'], $this->mime_types('csv'));
	}

	// zip
	public function is_zip($name){
		return 
			$this->is_uploaded($name) && 
			in_array($_FILES[$this->prefix.$name]['type'], $this->mime_types('zip'));
	}

	// pdf
	public function is_pdf($name){
		return 
			$this->is_uploaded($name) && 
			in_array($_FILES[$this->prefix.$name]['type'], $this->mime_types('pdf'));
	}
}
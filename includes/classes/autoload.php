<?php
/**
 * Created by PhpStorm.
 * User: penielarmah
 * Date: 18/12/2020
 * Time: 11:13 AM
 */

spl_autoload_register(function($className) {
    $file = $className . '.php';
    if (file_exists($file)) {
        include $file;
    }
});
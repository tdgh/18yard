<?php
/**
 * Created by PhpStorm.
 * User: penielarmah
 * Date: 18/12/2020
 * Time: 10:03 AM
 */
class evaluation
{
    public $player_id;
    private $databaseConnectionStatus;
    private $conn;

    private $players_table = 'unisoft_players';
    public $evaluation_tables = [
        'attitude'          => 'unisoft_attitude_eval',
        'coexistence'       => 'unisoft_coexistence',
        'header'            => 'unisoft_evaluation_header',
        'technical'         => 'unisoft_technical_eval',
        'tactical'          => 'unisoft_tactical_eval',
        'profile'           => 'unisoft_player_profile',
        'psycho'            => 'unisoft_physical_psycho',
        'notes'             => 'unisoft_notes',
        'player'            => 'unisoft_players'
    ];

    /**
     * evaluation constructor.
     * @param $player_id
     */
    public function __construct($player_id){
        $config = [
            'DB_HOST' 	  	 => '192.168.66.57',
            'DB_NAME'	  	 => 'football4all',
            'DB_USERNAME' 	 => 'root',
            'DB_PASSWORD' 	 => 'root',
        ];
        if($config){
                $this->conn = new PDO("mysql:host={$config['DB_HOST']};dbname={$config['DB_NAME']}", $config['DB_USERNAME'], $config['DB_PASSWORD']);
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }else{
            die('Database was not connected using the config file');
        }

        $this->player_id = $player_id;
    }

    /**
     * @param $dbhost
     * @param $dbusername
     * @param $dbpassword
     * @param $dbname
     * @return array|bool
     */
    private function setDatabaseConnection( $dbhost, $dbusername, $dbpassword, $dbname){
        try {
            $conn = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbusername, $dbpassword);

            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            //echo "Connected successfully";
            return ['db'=>$conn, 'status' =>true];
        } catch(PDOException $e) {
            //echo "Connection failed: " . $e->getMessage();
            return false;
        }
    }

    /**
     * @return string
     */
    public function get_player_details(){
        if($this->conn){
            $sql = 'SELECT * FROM '.$this->players_table.'
        WHERE player_id = :player_id';
            $sth = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $sth->execute(array(':player_id' => $this->player_id));
            $c = $sth->fetchAll();
            return json_encode($c);
        }
    }
    /**
     * @return array
     */
    public function get_player_report(){
        if($this->conn){
            $data = [];
            foreach ($this->evaluation_tables as $key=>$table_name){
                $sql = 'SELECT * FROM '.$table_name.'
        WHERE player_id = :player_id';
                $sth = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $sth->execute(array(':player_id' => $this->player_id));
                $c = $sth->fetchAll();
                $data[$key] = $c;
            }
            return $data;
        }
    }

    /**
     * @param $player_id
     * @param $table_name
     * @param array $data
     * @return int
     */
    public function update_player_evaluation($player_id, $table_name, $data = []){
        $col = [];
        foreach ($data as $key=>$value){
            $col[] = " {$key} = {$value} ";
        }
        $sql = "UPDATE {$table_name} SET ".implode(',', $col) ."WHERE player_id = {$player_id}";
        $sth = $this->conn->prepare($sql);
        return $sth->execute();
//        //return $sth->rowCount();
//        var_dump($sth->execute());
//        exit;

    }

    /**
     * @param $table_name
     * @param array $data
     * @return int
     */
    public function create_player_evaluation($table_name, $data = []){
        $col = [];
        $colVal = [];
        foreach ($data as $key=>$value){
            $col[] = "{$key}";
            $colVal[] = "{$value}";
        }
       $sql = "INSERT INTO {$table_name} (".implode(", ", $col).")
  VALUES (".implode(", ", $colVal).")";
        return $this->conn->exec($sql);
    }


    /**
     * @return bool
     */
    public function get_all_players(){
        if($this->conn){
                $sql = "SELECT * FROM  {$this->evaluation_tables['player']}";;
                $sth = $this->conn->prepare($sql);
                $sth->execute();
                $sth->setFetchMode(PDO::FETCH_ASSOC);
                return $sth->fetchAll();
        }
    }
}
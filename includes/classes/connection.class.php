<?php

namespace UNISOFT\SMS\CONNECTION;

class CONNECTION{
	public function search_cond($column, $name, $type){
		global $v, $and_cond, $execute;

		$value = $type == "in" ? (array)$v->value($name, [], false) : $v->value($name);

		if( (is_string($value) && $value != '') || (is_array($value) && count($value) > 0) ){
			$placeholder = $name.uniqid();
			
			if( $type == "in" ){
				$and_cond .= "AND $column IN (".implode(',', array_map('intval',$value)).") ";
			}else if( $type == "and" ){
				$and_cond .= "AND $column=:$placeholder ";
				$execute[$placeholder] = $value;
			}else{
				$and_cond .= "AND $column LIKE :$placeholder ";
				$execute[$placeholder] = '%'.$value.'%';
			}
		}
	}

	public function date($start_date, $end_date, $column){
		$start_date = !empty($start_date) ? date('Y-m-d', strtotime($start_date)) : '';
		$end_date 	= !empty($end_date) ? date('Y-m-d', strtotime($end_date)) : '';

		if( !empty($start_date) && empty($end_date) ){
		    $date_cond = "AND DATE($column) >= '$start_date' ";
		}else if( empty($start_date) && !empty($end_date) ){
		    $date_cond = "AND DATE($column) <= '$end_date' ";
		}else if( !empty($start_date) && !empty($end_date) ){
		    $date_cond = "AND DATE($column) BETWEEN '$start_date' AND '$end_date' ";
		}else{
			$date_cond = "";
		}

		return $date_cond;
	}

	public function between($start, $end, $column){
		$start = intval($start);
		$end   = intval($end);

		if( !empty($start) && empty($end) ){
		    $cond = "AND $column >= $start ";
		}else if( empty($start) && !empty($end) ){
		    $cond = "AND $column <= $end ";
		}else if( !empty($start) && !empty($end) ){
		    $cond = "AND $column BETWEEN $start AND $end ";
		}else{
			$cond = "";
		}

		return $cond;
	}

}

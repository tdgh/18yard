<?php

class CSRF_TOKEN{
	public static function token($len=32){
		$hash = strtr(base64_encode(bin2hex(random_bytes($len))), '+', '.');
		return substr($hash, 0, strlen($hash)-2);
	}

	public static function exist(){
		return isset($_SESSION[CSRF_TOKEN_KEY],$_SESSION[CSRF_TOKEN_KEY.'-COOKIE'],$_COOKIE[CSRF_TOKEN_KEY]);
	}

	public static function set_token(){
		$_SESSION[CSRF_TOKEN_KEY] = self::token(32);
		$_SESSION[CSRF_TOKEN_KEY.'-COOKIE'] = self::token(64);
		setcookie(CSRF_TOKEN_KEY, $_SESSION[CSRF_TOKEN_KEY.'-COOKIE'], 0, DOMAIN);
	}

	public static function check($type, $meth='get', $disable=true){ // type = `request_method` or `header`
		$_METH = $meth=='get' ? $_GET : $_POST;
		$token = $type=='header' ? ($_SERVER['HTTP_X_CSRF_TOKEN'] ?? '') : ($_METH['_token'] ?? '');
		$result= self::exist() && ($token === $_SESSION[CSRF_TOKEN_KEY]) && ($_SESSION[CSRF_TOKEN_KEY.'-COOKIE'] === $_COOKIE[CSRF_TOKEN_KEY]);

		if( !$result && $disable ){
			$_SESSION['__csrf_token_error'] = true;
		}

		return $result;
	}

	public static function destroy(){
		unset($_SESSION[CSRF_TOKEN_KEY]);
		unset($_SESSION[CSRF_TOKEN_KEY.'-COOKIE']);
		setcookie(CSRF_TOKEN_KEY, false, time()-1, DOMAIN);
	}
}
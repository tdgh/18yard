<?php

class ImageResize {
	private $size;
	private $size_height;
	private $destination;
	private $config;
	private $image_width;
	private $image_height;
	private $image_type;
	private $image_size_info;
	private $image_res;
	private $image_scale;
	private $new_width;
	private $new_height;
	private $new_canvas;
	private $curr_tmp_name;
	private $x_offset; 
	private $y_offset;
	
	function __construct($config) {
		$this->size 	   = $config["size"];
		$this->size_height = $config["size_height"];
		$this->destination = $config["destination"];
		$this->file_data   = $config["file_data"];
	}
	
	//generate cropped and resized thumbnails
	public function resize(){
		$this->curr_tmp_name = $this->file_data['tmp_name'];
		$this->get_image_info();
		
		$this->image_res = $this->get_image_resource();
		
		$this->new_width = $this->size;
		$this->new_height = $this->size_height;	
		
		
		$this->y_offset = 0; $this->x_offset = 0;
		if($this->image_width > $this->image_height){
			$this->x_offset = ($this->image_width - $this->image_height) / 2;
			$this->image_width = $this->image_height = $this->image_width - ($this->x_offset * 2);
		}else{
			$this->y_offset = ($this->image_height - $this->image_width) / 2;
			$this->image_width = $this->image_height  = $this->image_height - ($this->y_offset * 2);
		}
		
		if( $this->image_resampling() ) $this->save_image();
		imagedestroy($this->image_res);
	}
	
	//save image to destination
	private function save_image(){
		switch($this->image_type){//determine mime type
			case 'image/png': 
				imagepng($this->new_canvas, $this->destination);
				imagedestroy($this->new_canvas);
				break;
			case 'image/gif': 
				imagegif($this->new_canvas, $this->destination);
				imagedestroy($this->new_canvas);
				break;
			case 'image/jpeg':
				imagejpeg($this->new_canvas, $this->destination);
				imagedestroy($this->new_canvas);
				break;
			default: 
				imagedestroy($this->new_canvas);
				return false;
		}
	}
	
	//get image info
	private function get_image_info(){
		$this->image_size_info 	= getimagesize($this->curr_tmp_name);
		$this->image_width 		= $this->image_size_info[0]; //image width
		$this->image_height 	= $this->image_size_info[1]; //image height
		$this->image_type 		= $this->image_size_info['mime']; //image type
	}	
	
	//image resample
	private function image_resampling(){
		$this->new_canvas	= imagecreatetruecolor($this->new_width, $this->new_height);
		if(imagecopyresampled($this->new_canvas, $this->image_res, 0, 0, $this->x_offset, $this->y_offset, $this->new_width, $this->new_height, $this->image_width, $this->image_height)){
			return true;
		}	
	}
	
	//create image resource
	private function get_image_resource(){
		switch($this->image_type){
			case 'image/png':
				return imagecreatefrompng($this->curr_tmp_name); 
				break;
			case 'image/gif':
				return imagecreatefromgif($this->curr_tmp_name); 
				break;
			case 'image/jpeg': 
				return imagecreatefromjpeg($this->curr_tmp_name); 
				break;
			default:
				return false;
		}
	}
}
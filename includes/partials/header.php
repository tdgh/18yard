<header class="app-header app-header-dark">
    <div class="top-bar">
        <div class="top-bar-brand <?= isset($page_dependencies->hide_sidebar) ? 'bg-transparent' : ''; ?>"> <?php if (isset($page_dependencies->hide_sidebar)): ?>
                <button class="hamburger hamburger-squeeze mr-2" type="button" data-toggle="aside"><span
                            class="hamburger-box"><span class="hamburger-inner"></span></span></button> <?php endif; ?>
            <a href="<?= DOMAIN; ?>"><img src="<?= DOMAIN; ?>assets/images/logo-brand.png" alt=""
                                          style="width: auto;height: 50px;"></a></div>
        <div class="top-bar-list">
            <div class="top-bar-item px-2 d-md-none">
                <button class="hamburger hamburger-squeeze" type="button" data-toggle="aside"><span
                            class="hamburger-box"><span class="hamburger-inner"></span></span></button>
            </div>
            <div class="top-bar-item top-bar-item-full">
                <form class="top-bar-search">
                    <div class="input-group input-group-search">
                        <div class="input-group-prepend"><span class="input-group-text"> <span
                                        class="oi oi-magnifying-glass"></span> </span></div>
                        <input type="text" class="form-control" placeholder="Search"></div>
                </form>
            </div>
            <div class="top-bar-item top-bar-item-right px-0 d-none d-sm-flex">
                <ul class="header-nav nav">
                    <li class="nav-item dropdown header-nav-dropdown"><a class="nav-link" href="#"
                                                                         data-toggle="dropdown"><span
                                    class="oi oi-grid-three-up"></span></a>
                        <div class="dropdown-arrow"></div>
                        <div class="dropdown-menu dropdown-menu-rich dropdown-menu-right">
                            <div class="dropdown-sheets">
                                <div class="dropdown-sheet-item"><a href="<?= DOMAIN; ?>add_player"
                                                                    class="tile-wrapper"><span
                                                class="tile tile-lg bg-indigo"><i class="fa fa-user-plus"></i></span>
                                        <span class="tile-peek">Add Player</span></a></div>
                                <div class="dropdown-sheet-item"><a href="<?= DOMAIN; ?>players"
                                                                    class="tile-wrapper"><span
                                                class="tile tile-lg bg-teal"><i class="fa fa-users"></i></span> <span
                                                class="tile-peek">Players</span></a></div>
                                <div class="dropdown-sheet-item"><a href="<?= DOMAIN; ?>users"
                                                                    class="tile-wrapper"><span
                                                class="tile tile-lg bg-yellow"><i class="fa fa-user-cog"></i></span>
                                        <span class="tile-peek">Users</span></a></div>
                            </div>
                        </div>
                    </li>
                </ul>
                <div class="dropdown">
                    <button class="btn-account d-none d-md-flex" type="button" data-toggle="dropdown"><span
                                class="user-avatar user-avatar-md"> <img src="<?= $_user_photo; ?>" alt=""> </span>
                        <span class="account-summary pr-lg-4 d-none d-lg-block"> <span
                                    class="account-name"><?= htmlspecialchars($_user_name); ?></span> <span
                                    class="account-description"><?= htmlspecialchars($_user_role_name); ?></span> </span>
                    </button>
                    <div class="dropdown-arrow dropdown-arrow-left"></div>
                    <div class="dropdown-menu"><h6 class="dropdown-header d-lg-none text-truncate"
                                                   style="max-width: 200px;"><?= htmlspecialchars($_user_name); ?></h6>
                        <a class="dropdown-item" href="javascript:void(0);"><i class="dropdown-icon fa fa-user"></i>
                            Profile</a> <a class="dropdown-item" href="javascript:void(0);"><i
                                    class="dropdown-icon fa fa-unlock-alt"></i> Change Password</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="javascript:void(0);"
                           onclick="window.location.href='<?= DOMAIN; ?>logout'"><i
                                    class="dropdown-icon fa fa-power-off"></i> Logout</a></div>
                </div>
            </div>
        </div>
    </div>
</header>
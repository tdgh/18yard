<?php 

namespace TEMPORARY\LOGIN;

session_start();

$__require_config = false;
require dirname(__DIR__, 2).'/includes/config.php';

function temporary_login(){
	return 	isset($_SESSION[USER_DETS_KEY]) && 
			isset($_SESSION[LOG_UNAME_KEY]) && 
			isset($_SESSION[LOG_PWORD_KEY]) && 
			isset($_COOKIE[LOG_LOGIN_KEY]);
}
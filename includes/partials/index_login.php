<?php

require __DIR__.'/config.php';

if( temporary_login() ){
    header('Location: '.DOMAIN);
    die();
}
/**
 * @ page dependencies
*/
$page_dependencies = new \stdClass;

$page_dependencies->page_title = $page_title;
$page_dependencies->breadcrumb = [];
$page_dependencies->plugins = [];
$page_dependencies->js  = [];
// end of dependencies


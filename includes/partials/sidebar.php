<aside class="app-aside app-aside-light <?= isset($page_dependencies->hide_sidebar) ? '' : 'app-aside-expand-md'; ?>">
    <div class="aside-content">
        <header class="aside-header <?= isset($page_dependencies->hide_sidebar) ? '' : 'd-md-none'; ?>">
            <div class="top-bar-brand bg-gradient-blue d-flex"><a href="<?= DOMAIN; ?>"><img
                            src="<?= DOMAIN; ?>assets/images/logo-brand.png" alt=""
                            style="width: auto;height: 50px;"></a></div>
            <button class="hamburger hamburger-squeeze active" type="button" data-toggle="aside"><span
                        class="hamburger-box"><span class="hamburger-inner"></span></span></button>
            <button class="btn-account d-md-none" type="button" data-toggle="collapse" data-target="#dropdown-aside">
                <span class="user-avatar user-avatar-lg"> <img src="<?= $_user_photo; ?>" alt=""> </span> <span
                        class="account-icon"> <span class="fa fa-caret-down fa-lg"></span> </span> <span
                        class="account-summary"> <span class="account-name"><?= htmlspecialchars($_user_name); ?></span> <span
                            class="account-description"><?= htmlspecialchars($_user_role_name); ?></span> </span>
            </button>
            <div id="dropdown-aside" class="dropdown-aside collapse d-md-none">
                <div class="pb-3"><a class="dropdown-item" href=""><i class="dropdown-icon fa fa-user"></i> Profile</a>
                    <a class="dropdown-item" href=""><i class="dropdown-icon fa fa-unlock-alt"></i> Change Password</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="javascript:void(0);"
                       onclick="window.location.href='<?= DOMAIN; ?>logout'"><i
                                class="dropdown-icon fa fa-power-off"></i> Logout</a></div>
            </div>
        </header>
        <div class="aside-menu overflow-hidden">
            <nav id="stacked-menu" class="stacked-menu">
                <ul class="menu">
                    <li class="menu-item <?= $_g_file === '/' || $_g_file === '' ? 'has-active' : ''; ?>"><a
                                href="<?= DOMAIN; ?>" class="menu-link"> <span class="menu-img"><img
                                        src="<?= DOMAIN; ?>assets/images/icons/16.png" alt=""></span> <span
                                    class="menu-text">Dashboard</span> </a></li>
                    <li class="menu-item <?= $_g_file === 'events' ? 'has-active' : ''; ?>"><a
                                href="javascript:void(0);" class="menu-link"> <span class="menu-img"><img
                                        src="<?= DOMAIN; ?>assets/images/icons/9.png" alt=""></span> <span
                                    class="menu-text">Events</span> <span class="badge badge-warning">New</span> </a>
                    </li>
                    <li class="menu-header">Main Navigation</li>
                    <li class="menu-item <?= $_g_file === 'add_player' ? 'has-active' : ''; ?>"><a
                                href="<?= DOMAIN; ?>add_player" class="menu-link"> <span class="menu-img"><img
                                        src="<?= DOMAIN; ?>assets/images/icons/10.png" alt=""></span> <span
                                    class="menu-text">Add Player</span> </a></li>
                    <li class="menu-item <?= $_g_file === 'players' ? 'has-active' : ''; ?>"><a
                                href="<?= DOMAIN; ?>players" class="menu-link"> <span class="menu-img"><img
                                        src="<?= DOMAIN; ?>assets/images/icons/5.png" alt=""></span> <span
                                    class="menu-text">Players</span> </a></li>

                    <li class="menu-item <?= $_g_file === 'evaluation_report' ? 'has-active' : ''; ?>"><a
                                href="<?= DOMAIN; ?>evaluation_report" class="menu-link"> <span class="menu-img"><img
                                        src="<?= DOMAIN; ?>assets/images/icons/3.png" alt=""></span> <span
                                    class="menu-text">Evaluation Report</span> </a></li>

                    <li class="menu-item <?= $_g_file === 'upload_documents' ? 'has-active' : ''; ?>"><a
                                href="<?= DOMAIN; ?>upload_documents" class="menu-link"> <span class="menu-img"><img
                                        src="<?= DOMAIN; ?>assets/images/icons/1.png" alt=""></span> <span
                                    class="menu-text">Upload Other Documents</span> </a></li>

                    <li class="menu-item <?= $_g_file === 'users' ? 'has-active' : ''; ?>"><a href="<?= DOMAIN; ?>users"
                                                                                              class="menu-link"> <span
                                    class="menu-img"><img src="<?= DOMAIN; ?>assets/images/icons/22.png" alt=""></span>
                            <span class="menu-text">Users</span> </a></li>
                    <li class="menu-item <?= $_g_file === 'agents' ? 'has-active' : ''; ?>"><a
                                href="<?= DOMAIN; ?>agents" class="menu-link"> <span class="menu-img"><img
                                        src="<?= DOMAIN; ?>assets/images/icons/22.png" alt=""></span> <span
                                    class="menu-text">Agents</span> </a></li>
                    <li class="menu-item <?= $_g_file === 'income_and_expenses' ? 'has-active' : ''; ?>"><a
                                href="<?= DOMAIN; ?>income_and_expenses" class="menu-link"> <span class="menu-img"><img
                                        src="<?= DOMAIN; ?>assets/images/icons/7.png" alt=""></span> <span
                                    class="menu-text">Income &amp; Expenses</span> </a></li>
                </ul>
            </nav>
        </div>
        <div class="aside-footer border-top">
            <button class="btn btn-light btn-block text-primary px-3 text-left"
                    onclick="window.location.href='<?= DOMAIN; ?>logout'"><i class="fa fa-power-off mr-1"></i> Logout
            </button>
        </div>
    </div>
</aside>
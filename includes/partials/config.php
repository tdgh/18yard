<?php

// ini_set('error_reporting', E_ALL & ~E_STRICT);
// ini_set('display_errors', 1);
// ini_set('log_errors', 1);
// ini_set('ignore_repeated_errors', 1);
// ini_set('error_log', dirname(__DIR__).'/error_logs.txt');

// files to be required
$__required_files = [
	realpath(dirname(__DIR__, 2).'/includes/functions.php'),
	realpath(dirname(__DIR__, 2).'/includes/settings.php'),
	realpath(dirname(__DIR__, 2).'/includes/classes/csrf_token.class.php')
];

// start session if not started
if( !isset($_SESSION) ) session_start();

foreach($__required_files as $__rf){
	require $__rf;
}

if( temporary_login() ){
	$_user_row 		 = $_SESSION[USER_DETS_KEY];
	$_user_id  		 = $_user_row['id'];
	$_user_photo  	 = $_user_row['photo'];
	$_user_name  	 = $_user_row['name'];
	$_user_sex  	 = $_user_row['sex'];
	$_user_role  	 = $_user_row['role'];
	$_user_username  = $_user_row['username'];
	$_user_password  = $_user_row['password'];

	$_user_photo  	  = photo($_user_photo, $_user_sex, 'users', 'url', $_user_name);
	$_user_role_name  = $user_roles[$_user_row['role']];
	$__user_has_login = true;
	$admin_id 		  = $_user_id;
}

if( isset($__ajax) ){
	$xhr_response = ['success'=>'success', 'errors'=>[], 'details'=>[], 'data'=>[]];

	if( isset($__not_ajax_but_use_ajax_config) ){
		// do nothing
	}else if( !isset($_SERVER['HTTP_X_REQUESTED_WITH']) ){
		if( isset($__set_http_response_code) ) http_response_code($__set_http_response_code);
		die();
	}else if( !\CSRF_TOKEN::check('header', '', false) ){
		\CSRF_TOKEN::set_token();
		login_error();
	}

	if( !$connect ){
		login_error();
	}
}else{
	if( !$connect ){
		die('THE SYSTEM COULD NOT CONNECT TO THE DATABASE. PLEASE CONTACT THE DEVELOPER');
	}

	// create csrf token if not created
	if( !\CSRF_TOKEN::exist() ){
		\CSRF_TOKEN::set_token();
	}

	// disable page if csrf token is invalid
	if( sess_or_cook('sess', '__csrf_token_error') === true && !isset($__logout) ){
		\CSRF_TOKEN::set_token();
		include dirname(__DIR__).'/csrf_token_error.php';
		die();
	}
}
<script>if (typeof module === 'object') {
        window.module = module;
        module = undefined;
    }</script>
<script> const DOMAIN = "<?= DOMAIN; ?>"; </script>
<script src="<?= DOMAIN; ?>plugins/jquery/jquery-3.3.1.min.js<?= VER2; ?>"></script>
<script src="<?= DOMAIN; ?>plugins/popper/popper.min.js<?= VER2; ?>"></script>
<script src="<?= DOMAIN; ?>plugins/bootstrap/js/bootstrap.min.js<?= VER2; ?>"></script>
<script src="<?= DOMAIN; ?>plugins/pace/pace.min.js<?= VER2; ?>"></script>
<script src="<?= DOMAIN; ?>plugins/stacked-menu/stacked-menu.min.js<?= VER2; ?>"></script>
<script src="<?= DOMAIN; ?>plugins/perfect-scrollbar/perfect-scrollbar.min.js<?= VER2; ?>"></script> <?php if (isset($page_dependencies->plugins)): ?><?php if (in_array('moment', $page_dependencies->plugins)): ?>
    <script src="<?= DOMAIN; ?>plugins/moment/moment.min.js<?= VER2; ?>"></script> <?php endif; ?><?php if (in_array('jquery-ui', $page_dependencies->plugins)): ?>
    <script src="<?= DOMAIN; ?>plugins/jquery-ui/jquery-ui.min.js<?= VER2; ?>"></script> <?php endif; ?><?php if (in_array('bootstrap-datepicker', $page_dependencies->plugins)): ?>
    <script src="<?= DOMAIN; ?>plugins/bootstrap-datepicker/bootstrap-datepicker.js<?= VER2; ?>"></script> <?php endif; ?><?php if (in_array('contextMenu', $page_dependencies->plugins)): ?>
    <script src="<?= DOMAIN; ?>plugins/contextMenu/jquery.contextMenu.min.js<?= VER2; ?>"></script> <?php endif; ?><?php if (in_array('datatables', $page_dependencies->plugins)): ?>
    <script src="<?= DOMAIN; ?>plugins/datatables/datatables.min.js<?= VER2; ?>"></script> <?php endif; ?><?php if (in_array('dropify', $page_dependencies->plugins)): ?>
    <script src="<?= DOMAIN; ?>plugins/dropify/dropify.min.js<?= VER2; ?>"></script> <?php endif; ?><?php if (in_array('endless-scroll', $page_dependencies->plugins)): ?>
    <script src="<?= DOMAIN; ?>plugins/endless-scroll/endless-scroll.js<?= VER2; ?>"></script> <?php endif; ?><?php if (in_array('fancybox', $page_dependencies->plugins)): ?>
    <script src="<?= DOMAIN; ?>plugins/fancybox/jquery.fancybox.min.js<?= VER2; ?>"></script> <?php endif; ?><?php if (in_array('fancytree', $page_dependencies->plugins)): ?>
    <script src="<?= DOMAIN; ?>plugins/fancytree/jquery.fancytree-all<?= !in_array('jquery-ui', $page_dependencies->plugins) ? '-deps' : ''; ?>.min.js<?= VER2; ?>"></script> <?php endif; ?><?php if (in_array('flatpickr', $page_dependencies->plugins)): ?>
    <script src="<?= DOMAIN; ?>plugins/flatpickr/flatpickr.min.js<?= VER2; ?>"></script> <?php endif; ?><?php if (in_array('highcharts', $page_dependencies->plugins)): ?>
    <script src="<?= DOMAIN; ?>plugins/highcharts/highcharts.js<?= VER2; ?>"></script> <?php endif; ?><?php if (in_array('highcharts-export', $page_dependencies->plugins)): ?>
    <script src="<?= DOMAIN; ?>plugins/highcharts/exporting.js<?= VER2; ?>"></script>
    <script src="<?= DOMAIN; ?>plugins/highcharts/offline-exporting.js<?= VER2; ?>"></script> <?php endif; ?><?php if (in_array('intl-tel-input', $page_dependencies->plugins)): ?>
    <script src="<?= DOMAIN; ?>plugins/intl-tel-input/intlTelInput.min.js<?= VER2; ?>"></script>
    <script src="<?= DOMAIN; ?>plugins/intl-tel-input/utils.js<?= VER2; ?>"></script> <?php endif; ?><?php if (in_array('js.cookie', $page_dependencies->plugins)): ?>
    <script src="<?= DOMAIN; ?>plugins/js.cookie/js.cookie.js<?= VER2; ?>"></script> <?php endif; ?><?php if (in_array('ion.rangeSlider', $page_dependencies->plugins)): ?>
    <script src="<?= DOMAIN; ?>plugins/ion.rangeSlider/ion.rangeSlider.min.js<?= VER2; ?>"></script> <?php endif; ?><?php if (in_array('owlcarousel', $page_dependencies->plugins)): ?>
    <script src="<?= DOMAIN; ?>plugins/owlcarousel/owl.carousel.min.js<?= VER2; ?>"></script> <?php endif; ?><?php if (in_array('repeater', $page_dependencies->plugins)): ?>
    <script src="<?= DOMAIN; ?>plugins/repeater/jquery.repeater.min.js<?= VER2; ?>"></script> <?php endif; ?><?php if (in_array('select2', $page_dependencies->plugins)): ?>
    <script src="<?= DOMAIN; ?>plugins/select2/select2.min.js<?= VER2; ?>"></script> <?php endif; ?><?php if (in_array('shepherd', $page_dependencies->plugins)): ?>
    <script src="<?= DOMAIN; ?>plugins/shepherd/shepherd.min.js<?= VER2; ?>"></script> <?php endif; ?><?php if (in_array('sweetalert2', $page_dependencies->plugins)): ?>
    <script src="<?= DOMAIN; ?>plugins/sweetalert2/sweetalert2.min.js<?= VER2; ?>"></script> <?php endif; ?><?php if (in_array('toast', $page_dependencies->plugins)): ?>
    <script src="<?= DOMAIN; ?>plugins/toast/jquery.toast.min.js<?= VER2; ?>"></script> <?php endif; ?><?php if (in_array('typed', $page_dependencies->plugins)): ?>
    <script src="<?= DOMAIN; ?>plugins/typed/typed.min.js<?= VER2; ?>"></script> <?php endif; ?><?php endif; ?>
<script src="<?= DOMAIN; ?>assets/js/main.js"></script>
<script src="<?= DOMAIN; ?>assets/js/script.js"></script> <?php if (isset($page_dependencies->js) && count($page_dependencies->js) > 0): ?><?php foreach ($page_dependencies->js as $js): ?>
    <script src="<?= DOMAIN; ?>assets/js/<?= $js; ?>"></script> <?php endforeach; ?><?php endif; ?>
<script>if (window.module) {
        module = window.module
    }</script>
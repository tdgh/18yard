<?php

date_default_timezone_set('Africa/Accra');

$config = array(
	'DB_HOST' 	  	 => '192.168.66.57',
	'DB_NAME'	  	 => 'football4all',
	'DB_USERNAME' 	 => 'root',
	'DB_PASSWORD' 	 => 'root',
	'DOMAIN' 		 => '',
	'COMPANY_NAME' 	 => 'FOOTBALL4ALL',
	'COMPANY_SITE' 	 => '',
	'INITIAL' 		 => 'FMS',
	'SOFTWARE_NAME'  => 'FMS',
	'SOFTWARE_VERS'  => '1.0.0',
	'DEV_NAME' 		 => '18yardbox Football Agency Limited',
	'DEV_WEBSITE' 	 => '',
	'__KEY' 		 => 'football_',
	'__ENV__' 		 => 'DEVELOPMENT'
);


if( $config['__ENV__'] == 'PRODUCTION' ){
	define('VER1', '?v='.$config['SOFTWARE_VERS']);
	define('VER2', '?v='.substr($config['SOFTWARE_VERS'], 0, 3));
}else{
	define('VER1', '');
	define('VER2', '');
}

define("DOMAIN", '/'.$config['DOMAIN'].(!empty($config['DOMAIN']) ? '/' : ''));
define("DOCUMENT_ROOT", $_SERVER['DOCUMENT_ROOT'].DOMAIN);

define('USER_DETS_KEY', '____user_data-'.md5($config['__KEY']));
define('LOG_LOGOUT_KEY', hash('sha256', $config['__KEY'].'_logout'));
define('LOG_UNAME_KEY', hash('sha256', $config['__KEY'].'_username'));
define('LOG_PWORD_KEY', hash('sha256', $config['__KEY'].'_password'));
define('LOG_REMME_KEY', hash('sha256', $config['__KEY'].'_rm'));
define('LOG_LOGIN_KEY', hash('sha256', $config['__KEY'].'_login'));
define('LOGIN_ERR_KEY', hash('sha256', $config['__KEY'].'_login_error'));

define('CSRF_TOKEN_KEY', 'CSRF-TOKEN-'.hash('sha256', $config['__KEY']));
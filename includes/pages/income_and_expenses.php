<?php

require dirname(__DIR__, 2).'/includes/partials/index.php';

/**
 * @ page dependencies
*/
$page_dependencies = new \stdClass;

$page_dependencies->page_title = 'Income &amp; Expenses';
$page_dependencies->breadcrumb = [
    'Income &amp; Expenses' => ''
];
$page_dependencies->plugins = [
	'datatables',
	'fancybox',
	'flatpickr',
	'select2',
	'sweetalert2',
    'toast'
];
$page_dependencies->js = ['validator.js', 'income_and_expenses.js'];
// $page_dependencies->hide_sidebar = false;
// end of page dependencies


require __DIR__.'/views/income_and_expenses.view.php';
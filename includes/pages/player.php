<?php

require dirname(__DIR__, 2).'/includes/partials/index.php';

/**
 * @ page dependencies
*/
$page_dependencies = new \stdClass;

$page_dependencies->page_title = 'Player Details';
$page_dependencies->breadcrumb = [
    'Players' => 'players',
    'Player Details' => ''
];
$page_dependencies->plugins = [
	'dropify',
    'fancybox',
    'flatpickr',
    'intl-tel-input',
    'ion.rangeSlider',
    'repeater',
    'select2',
    'sweetalert2',
    'toast'
];
$page_dependencies->js = ['validator.js', 'player.js'];
// $page_dependencies->hide_sidebar = false;
// end of page dependencies

$countries_arr = query('countries', ['nationality'], [['status','1']], 'nationality');

if( isset($_GET['__'], $_GET['h']) && 
    md5($_GET['__']) === $_GET['h'] && 
    \CSRF_TOKEN::check('request_method', 'get') 
){    
    $get_player_dets = $connect->prepare("
        SELECT  unisoft_players.*,
                CONCAT(unisoft_players.first_name, ' ', unisoft_players.last_name) AS name,
                unisoft_users.name AS entry_by 
        FROM unisoft_players 
        INNER JOIN unisoft_users 
        ON unisoft_users.id=unisoft_players.entry_by 
        WHERE player_id=:player_id
    ");
    $get_player_dets->execute(['player_id'=>$_GET['__']]);

    if( $get_player_dets->rowcount() > 0 ){
        $row = $get_player_dets->fetch(\PDO::FETCH_ASSOC);
        $player_dets = [
            'player_id'           => htmlspecialchars($row['player_id']),
            'photo'               => photo($row['photo'], $row['sex'], 'players'),
            'first_name'          => htmlspecialchars($row['first_name']),
            'last_name'           => htmlspecialchars($row['last_name']),
            'name'                => htmlspecialchars($row['name']),
            'date_of_birth'       => c_date('d-m-Y', $row['date_of_birth']),
            'sex'                 => htmlspecialchars($row['sex']),
            'nationality'         => htmlspecialchars($row['nationality']),
            'marital_status'      => htmlspecialchars($row['marital_status']),
            'weight'              => htmlspecialchars($row['weight']),
            'height'              => htmlspecialchars($row['height']),
            'passport_no'         => htmlspecialchars($row['passport_no']),
            'preferred_position'  => htmlspecialchars($row['preferred_position']),
            'sub_position'        => htmlspecialchars($row['sub_position']),
            'current_club'        => htmlspecialchars($row['current_club']),
            'contract_expiration' => htmlspecialchars($row['contract_expiration']),
            'level_played'        => htmlspecialchars($row['level_played']),
            'registration_type'   => htmlspecialchars($row['registration_type']),
            'playing_foot'        => htmlspecialchars($row['playing_foot']),
            'health'              => htmlspecialchars($row['health']),
            'player_profile'      => htmlspecialchars($row['player_profile']),
            'right_foot'          => htmlspecialchars($row['right_foot']),
            'left_foot'           => htmlspecialchars($row['left_foot']),
            'speed'               => htmlspecialchars($row['speed']),
            'athleticism'         => htmlspecialchars($row['athleticism']),
            'tactics'             => htmlspecialchars($row['tactics']),
            'hard_work'           => htmlspecialchars($row['hard_work']),
            'teamwork'            => htmlspecialchars($row['teamwork']),
            'adaptability'        => htmlspecialchars($row['adaptability']),
            'personality'         => htmlspecialchars($row['personality']),
            'phone'               => htmlspecialchars($row['phone']),
            'other_phone'         => htmlspecialchars($row['other_phone']),
            'email'               => htmlspecialchars($row['email']),
            'address'             => htmlspecialchars($row['address']),
            'player_number'       => htmlspecialchars($row['player_number']),
            'contract'            => htmlspecialchars($row['contract']),
            'date_joined'         => c_date('d-m-Y', $row['date_joined']),
            'status'              => htmlspecialchars($row['status']),
            'status_date'         => c_date('d-m-Y', $row['status_date']),
            'status_comment'      => htmlspecialchars($row['status_comment']),
            'entry_by'            => htmlspecialchars($row['entry_by']),
            'entry_date'          => c_date('d-m-Y h:ia', $row['entry_date']),
            'hash'                => md5($row['player_id'])
        ];

        // achievements and career path
        $player_id    = $player_dets['player_id'];
        $achievements = query('players_achievements', [], [['player_id', $player_id]]);
        $career_path  = query('players_career_path', [], [['player_id', $player_id]]);

        // career path
        function disp_n_a($txt, $def='N/A'){
            return empty($txt) ? '<span class="n_a">'.$def.'</span>' : $txt;
        }
    }else{
        header('Location: '.DOMAIN.'players');
        die();
    }
}else{
    header('Location: '.DOMAIN.'players');
    die();
}

require __DIR__.'/views/player.view.php';
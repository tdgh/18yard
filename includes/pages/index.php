<?php

require dirname(__DIR__, 2).'/includes/partials/index.php';

/**
 * @ page dependencies
*/
$page_dependencies = new \stdClass;

$page_dependencies->page_title = 'Dashboard';
$page_dependencies->breadcrumb = [
    '' => ''
];
$page_dependencies->plugins = [
    'highcharts'
];
$page_dependencies->js = [];
$page_dependencies->hide_sidebar = true;
// end of page dependencies


$get_five_players = $connect->query("
    SELECT  player_id,
            CONCAT(first_name, ' ', last_name) AS name,
            sex,
            photo,
            SUM(
                right_foot+
                left_foot+
                speed+
                athleticism+
                tactics+
                hard_work+
                teamwork+
                adaptability+
                personality
            )/9 AS average_skills 
    FROM unisoft_players 
    GROUP BY player_id 
    ORDER BY name ASC 
    LIMIT 5
");
$five_payers = $get_five_players->fetchAll(\PDO::FETCH_ASSOC);

// get players group by statuses
$data = [];
$get_players_by_status = $connect->query("
    SELECT  status, COUNT(*) AS players 
    FROM unisoft_players 
    GROUP BY status
");
$players_by_status = $get_players_by_status->fetchAll(\PDO::FETCH_KEY_PAIR);

foreach($player_statuses as $status=>$status_dets){
    $data[] = ['name'=>$status_dets['name'], 'y'=>intval($players_by_status[$status] ?? 0)];
}

// total number of players
$total_players = array_sum(array_values($players_by_status));

require __DIR__.'/views/index.view.php';
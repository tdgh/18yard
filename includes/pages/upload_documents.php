<?php

require dirname(__DIR__, 2).'/includes/partials/index.php';

/**
 * @ page dependencies
*/
$page_dependencies = new \stdClass;

$page_dependencies->page_title = 'Upload Documents';
$page_dependencies->breadcrumb = [
    'Upload Documents' => ''
];
$page_dependencies->plugins = [
    'dropify',
    'flatpickr',
    'intl-tel-input',
    'ion.rangeSlider',
    'repeater',
    'select2',
    'sweetalert2',
    'toast'
];
$page_dependencies->js = ['validator.js', 'upload_documents.js'];
// $page_dependencies->hide_sidebar = false;
// end of page dependencies

$countries_arr = query('countries', ['nationality'], [['status','1']], 'nationality');

require __DIR__.'/views/upload_documents.view.php';
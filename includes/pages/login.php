<?php

$page_title = 'Login';
require dirname(__DIR__, 2) . '/includes/partials/index_login.php';

if (isset($_POST[LOG_LOGIN_KEY], $_POST[LOG_UNAME_KEY], $_POST[LOG_PWORD_KEY])) {
    if (!\CSRF_TOKEN::check('request_method', 'post')) {
        header('Location: ' . $_SERVER['REQUEST_URI']);
        die();
    }

    \CSRF_TOKEN::set_token();

    $username = $_POST[LOG_UNAME_KEY];
    $password = $_POST[LOG_PWORD_KEY];

    $stmt = $connect->prepare("
		SELECT * 
		FROM unisoft_users 
		WHERE username=:username 
		AND status=1
	");
    $stmt->execute(['username' => $username]);

    $user_details = $stmt->fetch(\PDO::FETCH_ASSOC);
    $hashed = $user_details['password'] ?? '';

    if (password_verify($password, $hashed)) {
        if (isset($_POST[LOG_REMME_KEY])) {
            setcookie(LOG_REMME_KEY, $username, time() + 60 * 60 * 24 * 365);
        } else {
            setcookie(LOG_REMME_KEY, false, time() - 1);
        }

        $_SESSION[USER_DETS_KEY] = $user_details;
        $_SESSION[LOG_UNAME_KEY] = $username;
        $_SESSION[LOG_PWORD_KEY] = $hashed;
        setcookie(LOG_LOGIN_KEY, true, 0, DOMAIN);
        setcookie(LOG_LOGOUT_KEY, false, time() - 1, DOMAIN);

        header('Location: ' . DOMAIN);
    } else {
        $_SESSION[LOGIN_ERR_KEY] = true;
        header('Location: ' . $_SERVER['REQUEST_URI']);
    }

    die();
}

$__login_error = sess_or_cook('sess', LOGIN_ERR_KEY);

?>
<!DOCTYPE html>
<html>
<head> <?php include DOCUMENT_ROOT . '/includes/partials/head.php'; ?>
    <style type="text/css"> .form-group .position-relative .position-absolute {
            left: 20px;
            top: 10px;
            line-height: 1.5rem;
        }

        .form-group .position-relative .form-control {
            padding-left: 45px;
        }

        .pace .pace-progress {
            top: 0;
        } </style>
</head>
</head>
<body>
<main class="auth auth-floated">
    <form action="" method="post" class="auth-form"><input type="hidden" name="_token"
                                                           value="<?= $_SESSION[CSRF_TOKEN_KEY]; ?>">
        <div class="mb-4">
            <div class="mb-3"><img class="rounded" src="<?= DOMAIN; ?>assets/images/logo-brand.png" alt=""
                                   style="max-height: 80px;"></div>
            <h1 class="h3">Sign In</h1></div> <?php if ($__login_error !== false): ?>
            <div class="alert alert-danger" style="cursor: pointer;">Invalid Username Or Password
                <button type="button" class="close" data-dismiss="alert" style="font-size: 1.2rem;"><i
                            class="fa fa-times"></i></button>
            </div> <?php endif; ?>
        <div class="form-group mb-4"><label class="d-block text-left" for="username">Username</label>
            <div class="position-relative"><i class="fa fa-user text-muted position-absolute" tabindex="-1"
                                              onclick="document.getElementById('username').focus();"></i> <input
                        type="text" class="form-control form-control-lg" name="<?= LOG_UNAME_KEY; ?>" id="username"
                        spellcheck="false" placeholder="Username" <?php if (isset($_COOKIE[LOG_REMME_KEY])) {
                    echo 'value="' . $_COOKIE[LOG_REMME_KEY] . '"';
                } ?>></div>
        </div>
        <div class="form-group mb-4"><label class="d-block text-left" for="password">Password</label>
            <div class="position-relative"><i class="fa fa-lock text-muted position-absolute" tabindex="-1"
                                              onclick="document.getElementById('password').focus();"></i> <input
                        type="password" name="<?= LOG_PWORD_KEY; ?>" id="password" spellcheck="false"
                        placeholder="Password" class="form-control form-control-lg" name="<?= LOG_PWORD_KEY; ?>"
                        id="password"></div>
        </div>
        <div class="form-group mb-4">
            <button type="submit" class="btn btn-lg btn-primary btn-block" name="<?= LOG_LOGIN_KEY; ?>"
                    id="login_button">Sign In
            </button>
        </div>
        <div class="form-group d-flex justify-content-between">
            <div class="custom-control custom-control-inline custom-checkbox"><input type="checkbox"
                                                                                     class="custom-control-input"
                                                                                     name="<?= LOG_REMME_KEY; ?>"
                                                                                     id="remember-me" <?php if (isset($_COOKIE[LOG_REMME_KEY])) {
                    echo 'checked';
                } ?>> <label class="custom-control-label" for="remember-me">Keep me sign in</label></div>
            <a href="" class="link">Forgot Password?</a>
        </div> <?php include DOCUMENT_ROOT . 'includes/partials/footer.php'; ?> </form>
    <div class="auth-announcement"
         style="background-image: url(<?= DOMAIN; ?>assets/images/login-bg.png);background-size: cover;background-position: center center;"></div>
</main>
<script type="text/javascript"> var form = document.querySelector('form');
    form.addEventListener('submit', (e) => {
        var username = document.getElementById('username');
        var password = document.getElementById('password');
        var button = document.getElementById('login_button');
        if (username.value.trim() == '') {
            e.preventDefault();
            username.focus();
        } else if (password.value.trim() == '') {
            e.preventDefault();
            password.focus();
        } else {
            setTimeout(() => {
                button.setAttribute('disabled', true)
            }, 10);
        }
    }); </script> <?php include DOCUMENT_ROOT . 'includes/partials/script.php'; ?> </body>
</html>
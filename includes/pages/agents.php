<?php

require dirname(__DIR__, 2).'/includes/partials/index.php';

/**
 * @ page dependencies
*/
$page_dependencies = new \stdClass;

$page_dependencies->page_title = 'Agents';
$page_dependencies->breadcrumb = [
	'Agents' => ''
];
$page_dependencies->plugins = [
	'datatables',
	'fancybox',
	'flatpickr',
	'intl-tel-input',
	// 'js.cookie',
    'select2',
    'sweetalert2',
    'toast'
];
$page_dependencies->js = ['validator.js', 'agents.js'];
// $page_dependencies->hide_sidebar = false;
// end of page dependencies


$agents = query('agents');

// print_r($agents);die();

require __DIR__.'/views/agents.view.php';
<?php

require dirname(__DIR__, 2).'/includes/partials/index.php';
require __DIR__.'/includes/players.global.inc.php';

/**
 * @ page dependencies
*/
$page_dependencies = new \stdClass;

$page_dependencies->page_title = 'Players';
$page_dependencies->breadcrumb = [
    'Players' => ''
];
$page_dependencies->plugins = [
	'datatables',
	'fancybox',
	'js.cookie',
	'sweetalert2',
    'toast'
];
$page_dependencies->js = ['validator.js', 'dt-columns-toggler.js', 'players.js'];
// $page_dependencies->hide_sidebar = false;
// end of page dependencies


$dt_columns = [];
foreach($db_table_cols as $c_col=>$c_name){
	$dt_columns[] = ['data'=>$c_col, 'name'=>$c_col];
}

require __DIR__.'/views/players.view.php';
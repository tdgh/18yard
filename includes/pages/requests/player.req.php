<?php

require dirname(__DIR__, 3).'/includes/partials/index_ajax.php';
require DOCUMENT_ROOT.'includes/classes/validator.class.php';

use UNISOFT\SMS\VALIDATOR;
if( isset($_POST[sha1('__edit_player')]) ){
	$errors = [];
	$v = new VALIDATOR\VALIDATOR('edit_player_');

	$p_exist  = query('players', ['photo','contract'],[['player_id',$v->value('player_id')]]);
	$curr_det = $p_exist[0] ?? [];

	if( count($curr_det) == 0 ){
		$errors['error'] = 'Player does not exist';
	}else{
		// bio data
		$v->required('first_name');
		$v->required('last_name');
		$v->if_in_array('sex', ['Male', 'Female'], true);
		$v->if_exist('nationality', 'countries', 
			[['nationality', $v->value('nationality')], ['status', 1]], false);
		$v->if_in_array('marital_status', $marital_statuses, false);

		// contact details
		$v->phone('phone');
		$v->phone('other_phone');
		$v->email('email');

		// agency details
		if( !$v->required('player_number') ){
			$pn = query('players', 'COUNT(*)',
				[['player_number', $v->value('player_number')],
				['player_id', $v->value('player_id'), '!=']]);
			if( $pn > 0 ) $v->error('player_number', 'Player ID already exist');
		}

		if( $v->is_uploaded('contract') ){
		    if( !in_array($_FILES[$v->prefix.'contract']['type'], $v->mime_types('pdf')) ){
		    	$v->error('contract', "Error: Invalid file, Only PDF files allowed");
		    }else if( $_FILES[$v->prefix.'contract']['size'] > (10 * 1048576) ){
		    	$v->error('contract', "Error: Maximum size allowed 10MB, Try smaller file!");
		    }
	    }

		$v->if_in_array('status', array_keys($player_statuses), true);
	}

	if( count($errors) == 0 ){
		$player_id 			 = $v->value('player_id');
        $first_name 		 = $v->value('first_name');
        $last_name 			 = $v->value('last_name');        
        $date_of_birth 		 = c_date('Y-m-d', $v->value('date_of_birth'));
	    $sex 				 = $v->value('sex');
        $nationality 		 = $v->value('nationality');
        $marital_status 	 = $v->value('marital_status');
        $weight 			 = $v->value('weight');
        $height 			 = $v->value('height');
        $passport_no 		 = $v->value('passport_no');
        $photo 		 		 = $curr_det['photo'];
        $preferred_position  = $v->value('preferred_position');
        $sub_position 		 = $v->value('sub_position');
        $current_club 		 = $v->value('current_club');
        $contract_expiration = $v->value('contract_expiration');
        $level_played 		 = $v->value('level_played');
        $registration_type 	 = $v->value('registration_type');
        $playing_foot 		 = $v->value('playing_foot');
        $health 			 = $v->value('health');
        $player_profile 	 = $v->value('player_profile');
        $right_foot 		 = $v->value('right_foot');
        $left_foot 			 = $v->value('left_foot');
        $speed 				 = $v->value('speed');
        $athleticism 		 = $v->value('athleticism');
        $tactics 			 = $v->value('tactics');
        $hard_work 			 = $v->value('hard_work');
        $teamwork 			 = $v->value('teamwork');
        $adaptability 		 = $v->value('adaptability');
        $personality 		 = $v->value('personality');
        $phone 				 = $v->value('phone');
        $other_phone 		 = $v->value('other_phone');
        $email 				 = $v->value('email');
        $address 			 = $v->value('address');
        $player_number 		 = $v->value('player_number');
        $contract 		 	 = $curr_det['contract'];
        $date_joined 		 = c_date('Y-m-d', $v->value('date_joined'));
        $status 			 = $v->value('status');
        $status_date 		 = c_date('Y-m-d', $v->value('status_date'));
        $status_comment 	 = $v->value('status_comment');
	    $entry_by 			 = $admin_id;
		$entry_date 		 = date('Y-m-d H:i:s');

		// photo
		if( $v->is_set('change_photo') ){
			$photo_path = realpath(DOCUMENT_ROOT.'assets/images/players/'.$photo);
			if( !empty($photo) && file_exists($photo_path) ) @unlink($photo_path);

			$photo = $v->img_upload(
				'photo', 'assets/images/players', 150, 150, 
				'player-'.uniqid().rand(1000, 9999), 'photo_auto_resize'
			);
		}

	    // contract
		if( $v->is_set('change_contract') ){
			$contract_path = realpath(DOCUMENT_ROOT.'assets/documents/contracts/'.$contract);
			if( !empty($contract) && file_exists($contract_path) ) @unlink($contract_path);

		    if( $v->is_pdf('contract') ){
		    	$new_fname = $config['COMPANY_NAME'].' AGREEMENT for '.$first_name.' '.$last_name;
		    	$basename  = $v->file_name('contract', 'assets/documents/contracts', $new_fname);

		    	list($filename, $extension) = explode('.', $basename, 2);

		    	$basename  = strtoupper(str_replace('_', ' ', $filename)).'.'.$extension;
				$filepath  = DOCUMENT_ROOT.'assets/documents/contracts/'.$basename;

				if(@move_uploaded_file($_FILES[$v->prefix.'contract']['tmp_name'],$filepath)){
					$contract = $basename;
				}
		    }else{
		    	$contract = '';
		    }
		}

	    $update_player = $connect->prepare("
	    	UPDATE unisoft_players SET 
		        first_name 			= :first_name,
		        last_name 			= :last_name,
		        date_of_birth 		= :date_of_birth,
			    sex 				= :sex,
		        nationality 		= :nationality,
		        marital_status 		= :marital_status,
		        weight 				= :weight,
		        height 				= :height,
		        passport_no 		= :passport_no,
		        photo 				= :photo,
		        preferred_position 	= :preferred_position,
		        sub_position 		= :sub_position,
		        current_club 		= :current_club,
		        contract_expiration = :contract_expiration,
		        level_played 		= :level_played,
		        registration_type 	= :registration_type,
		        playing_foot 		= :playing_foot,
		        health 				= :health,
		        player_profile 		= :player_profile,
		        right_foot 			= :right_foot,
		        left_foot 			= :left_foot,
		        speed 				= :speed,
		        athleticism 		= :athleticism,
		        tactics 			= :tactics,
		        hard_work 			= :hard_work,
		        teamwork 			= :teamwork,
		        adaptability 		= :adaptability,
		        personality 		= :personality,
		        phone 				= :phone,
		        other_phone 		= :other_phone,
		        email 				= :email,
		        address 			= :address,
		        player_number 		= :player_number,
		        contract 			= :contract,
		        date_joined 		= :date_joined,
		        status 				= :status,
		        status_date 		= :status_date,
		        status_comment 		= :status_comment,
			    modified_by 		= :modified_by,
				modified_date 		= :modified_date 
			WHERE player_id 		= :player_id
	    ");
		$update_player->execute([
			'first_name' 			=> $first_name,
	        'last_name' 			=> $last_name,
	        'date_of_birth' 		=> $date_of_birth,
		    'sex' 					=> $sex,
	        'nationality' 			=> $nationality,
	        'marital_status' 		=> $marital_status,
	        'weight' 				=> $weight,
	        'height' 				=> $height,
	        'passport_no' 			=> $passport_no,
	        'photo' 				=> $photo,
	        'preferred_position' 	=> $preferred_position,
	        'sub_position' 			=> $sub_position,
	        'current_club' 			=> $current_club,
	        'contract_expiration' 	=> $contract_expiration,
	        'level_played' 			=> $level_played,
	        'registration_type' 	=> $registration_type,
	        'playing_foot' 			=> $playing_foot,
	        'health' 				=> $health,
	        'player_profile' 		=> $player_profile,
	        'right_foot' 			=> $right_foot,
	        'left_foot' 			=> $left_foot,
	        'speed' 				=> $speed,
	        'athleticism' 			=> $athleticism,
	        'tactics' 				=> $tactics,
	        'hard_work' 			=> $hard_work,
	        'teamwork' 				=> $teamwork,
	        'adaptability' 			=> $adaptability,
	        'personality' 			=> $personality,
	        'phone' 				=> $phone,
	        'other_phone' 			=> $other_phone,
	        'email' 				=> $email,
	        'address' 				=> $address,
	        'player_number' 		=> $player_number,
	        'contract' 				=> $contract,
	        'date_joined' 			=> $date_joined,
	        'status' 				=> $status,
	        'status_date' 			=> $status_date,
	        'status_comment' 		=> $status_comment,
		    'modified_by' 			=> $entry_by,
			'modified_date' 		=> $entry_date,
			'player_id' 			=> $player_id
		]);

		// achievements
		$g_achievements = (array)$v->value('achievements', [], false);

		if( count($g_achievements) > 0 ){
			$insert_achievements = $connect->prepare("
				INSERT INTO unisoft_players_achievements(
					player_id,
					description,
					entry_by,
					entry_date
				) VALUES(
					:player_id,
					:description,
					:entry_by,
					:entry_date
				)
			");
			$insert_achievements->bindParam(':player_id', $player_id);
			$insert_achievements->bindParam(':description', $description);
			$insert_achievements->bindParam(':entry_by', $entry_by);
			$insert_achievements->bindParam(':entry_date', $entry_date);

			for($i=0; $i<count($g_achievements); $i++){
				$description = trim($g_achievements[$i]['description']);

				if( !empty($description) ) $insert_achievements->execute();
			}
		}

		// career_path
		$g_career_path = (array)$v->value('career_path', [], false);

		if( count($g_career_path) > 0 ){
			$insert_career_path = $connect->prepare("
				INSERT INTO unisoft_players_career_path(
					player_id,
					period,
					club,
					country,
					matches,
					goals,
					club_level,
					entry_by,
					entry_date
				) VALUES(
					:player_id,
					:period,
					:club,
					:country,
					:matches,
					:goals,
					:club_level,
					:entry_by,
					:entry_date
				)
			");
			$insert_career_path->bindParam(':player_id', $player_id);
			$insert_career_path->bindParam(':period', $period);
			$insert_career_path->bindParam(':club', $club);
			$insert_career_path->bindParam(':country', $country);
			$insert_career_path->bindParam(':matches', $matches);
			$insert_career_path->bindParam(':goals', $goals);
			$insert_career_path->bindParam(':club_level', $club_level);
			$insert_career_path->bindParam(':entry_by', $entry_by);
			$insert_career_path->bindParam(':entry_date', $entry_date);

			for($i=0; $i<count($g_career_path); $i++){
				$period 	= trim($g_career_path[$i]['period']);
				$club 		= trim($g_career_path[$i]['club']);
				$country 	= trim($g_career_path[$i]['country']);
				$matches 	= trim($g_career_path[$i]['matches']);
				$goals 		= trim($g_career_path[$i]['goals']);
				$club_level = trim($g_career_path[$i]['club_level']);

				if( !empty($period) ) $insert_career_path->execute();
			}
		}

		$xhr_response['success'] = 'success';
	}else{
		$xhr_response['errors']  = $errors;
		$xhr_response['success'] = 'error';
	}

	echo json_encode($xhr_response);
	die();
}

<?php

require dirname(__DIR__, 3).'/includes/partials/index_ajax.php';
require DOCUMENT_ROOT.'includes/classes/validator.class.php';

use UNISOFT\SMS\VALIDATOR;


function duration($entry_date, $expiry_date){
	$date1 = new DateTime($entry_date);
	$date2 = new DateTime($expiry_date);
	$diff  = $date1->diff($date2);

	$dates = [];
	!empty($diff->y) ? array_push($dates, $diff->y.' year'.($diff->y > 1 ? 's' : '')) : null;
	!empty($diff->m) ? array_push($dates, $diff->m.' month'.($diff->m > 1 ? 's' : '')) : null;
	!empty($diff->d) ? array_push($dates, $diff->d.' day'.($diff->d > 1 ? 's' : '')) : null;

	return implode(' ', $dates);
}


if( isset($_POST['__get_agent_details'], $_POST['id']) ){
	$errors   = [];
	$agent_id = intval($_POST['id'] ?? 0);

	$g_agent_details = $connect->prepare("SELECT * FROM unisoft_agents WHERE id=:agent_id");
	$g_agent_details->execute(['agent_id'=>$agent_id]);
	
	if( $g_agent_details->rowcount() > 0 ){
		$row = $g_agent_details->fetch(\PDO::FETCH_ASSOC);
		$agent_details = [
			'name' 		  => htmlspecialchars($row['name']),
			'phone' 	  => htmlspecialchars(!empty($row['phone']) ? $row['phone'] : 'N/A'),
			'passport_no' => htmlspecialchars(!empty($row['passport_no']) ? $row['passport_no'] : 'N/A')
		];

		$g_agent_players = $connect->prepare("
			SELECT 	unisoft_agents_players.id,
					unisoft_players.player_id,
					CONCAT(unisoft_players.first_name,' ',unisoft_players.last_name) AS name,
					unisoft_players.sex,
					unisoft_players.photo,
					unisoft_players.passport_no,
					unisoft_agents_players.mandate_file,
					unisoft_agents_players.status,
					unisoft_agents_players.status_date,
					unisoft_agents_players.status_comment,
					unisoft_agents_players.entry_date,
					unisoft_agents_players.expiry_date 
			FROM unisoft_agents_players 
			INNER JOIN unisoft_players 
			ON unisoft_players.player_id=unisoft_agents_players.player_id 
			WHERE unisoft_agents_players.agent_id=:agent_id
		");
		$g_agent_players->execute(['agent_id'=>$agent_id]);

		if( $g_agent_players->rowcount() > 0 ){
			while($row=$g_agent_players->fetch(\PDO::FETCH_ASSOC)){
				$agent_players[] = [
					'id' 			 => $row['id'],
					'player_id' 	 => $row['player_id'],
					'photo' 		 => photo($row['photo'], $row['sex'], 'players'),
					'name'			 => htmlspecialchars($row['name']),
					'passport_no'	 => htmlspecialchars($row['passport_no']),
					'duration' 		 => duration($row['entry_date'], $row['expiry_date']),
					'mandate_file' 	 => $row['mandate_file'],
					'entry_date' 	 => c_date('d-m-Y', $row['entry_date']),
					'expiry_date' 	 => c_date('d-m-Y', $row['expiry_date']),
					'status' 		 => $row['status'],
					'status_date' 	 => c_date('d-m-Y', $row['status_date']),
					'status_comment' => htmlspecialchars($row['status_comment']),
					'hash' 			 => md5($row['player_id'])
				];
			}
		}else{
			$agent_players = [];
		}

		$xhr_response['success'] = 'success';
		$xhr_response['details'] = [
			'details' => $agent_details,
			'players' => $agent_players
		];
	}else{
		$xhr_response['errors']  = ['error'=>'Selected agent does not exist'];
		$xhr_response['success'] = 'error';
	}

	echo json_encode($xhr_response);
	die();
}

if( isset($_POST[sha1('__add_agent')]) ){
	$errors = [];
	$v = new VALIDATOR\VALIDATOR('add_agent_');

	$v->required('name');
	$v->phone('phone');

	if( count($errors) == 0 ){
        $name 		 = $v->value('name');
        $phone 	 	 = $v->value('phone');
        $passport_no = $v->value('passport_no');
		$backgrounds = [
			'primary',
			'success',
			'info',
			'warning',
			'danger',
			// 'dark',
			'blue',
			// 'indigo',
			'purple',
			// 'pink',
			'red',
			'orange',
			// 'yellow',
			'green',
			'teal',
			'cyan',
			'white',
			'gray',
			// 'gray-dark',
			// 'black'
		];

	    $insert_agent = $connect->prepare("
	    	INSERT INTO unisoft_agents(
		        name,
		        phone,
		        passport_no,
		        background
	    	) VALUES(
				:name,
				:phone,
				:passport_no,
				:background
	    	)
	    ");
		$insert_agent->execute([
			'name' 		  => $name,
	        'phone' 	  => $phone,
	        'passport_no' => $passport_no,
	        'background'  => $backgrounds[rand(0, count($backgrounds)-1)]
		]);

		$agent_id = $connect->lastInsertId();

		$xhr_response['success'] = 'success';
		$xhr_response['details'] = ['__'=>$agent_id, 'camo'=>md5($agent_id)];
	}else{
		$xhr_response['errors']  = $errors;
		$xhr_response['success'] = 'error';
	}

	echo json_encode($xhr_response);
	die();
}

//
if( isset($_POST['__get_select2_players'], $_POST['search']) ){
	$data = [];
	$search = trim($_POST['search']);

	$get_players = $connect->prepare("
		SELECT player_id, CONCAT(first_name, ' ', last_name) AS name, photo, sex, player_number 
		FROM unisoft_players 
		HAVING name 
		LIKE :name 
		ORDER BY name 
		ASC 
		LIMIT 15
	");
	$get_players->execute(['name'=>'%'.$search.'%']);

	while($row=$get_players->fetch(PDO::FETCH_ASSOC)){
		$data[] = [
			'id' 			=> $row['player_id'],
			'player_id' 	=> htmlspecialchars($row['player_id']),
			'photo' 		=> photo($row['photo'], $row['sex'], 'players'),
		    'name' 			=> htmlspecialchars($row['name']),
		    'player_number' => htmlspecialchars($row['player_number']),
		];
	}

	$xhr_response['success'] = true;
	$xhr_response['details'] = [
		'total' => count($data),
		'players' => $data
	];

	echo json_encode($xhr_response);
	die();
}

if( isset($_POST[sha1('__tie_player_to_agent')]) ){
	$errors = [];
	$v = new VALIDATOR\VALIDATOR('tie_player_to_agent_');


	if( !$v->required('player') ){
		$a_exist = query('agents', ['name'], [['id', $v->value('agent')]]);
		if( count($a_exist) == 0 ) $v->error('agent', 'Selected agent does not exist');
	}

	if( !$v->required('player') ){
		$p_exist = query('players', [
			'player_id',
			'CONCAT(first_name, \' \', last_name) AS name',
			'sex',
			'photo',
			'passport_no'
		], [['player_id', $v->value('player')]]);

		if( count($p_exist) == 0 ){
			$v->error('player', 'Selected player does not exist');
		}else{
			$curr_has_agent = query('agents_players', 'COUNT(*)', 
				[['player_id', $v->value('player')], ['status', '1']]);

			if( $curr_has_agent > 0 ){
				$v->error('player', 'Selected player is currently tied to an agent');
			}
		}
	}

	if( !$v->required('entry_date') && !$v->required('expiry_date') ){
		if( strtotime($v->value('expiry_date')) <= strtotime($v->value('entry_date')) ){
			$v->error('expiry_date', "Expiry date should be greater than entry date");
		}
	}

	if( $v->is_uploaded('mandate_file') ){
	    if( !in_array($_FILES[$v->prefix.'mandate_file']['type'], $v->mime_types('pdf')) ){
	    	$v->error('mandate_file', "Error: Invalid file, Only PDF files allowed");
	    }else if( $_FILES[$v->prefix.'mandate_file']['size'] > (10 * 1048576) ){
	    	$v->error('mandate_file', "Error: Maximum size allowed 10MB, Try smaller file!");
	    }
    }

    $v->if_in_array('status', array_keys($agent_player_statuses), true);


	if( count($errors) == 0 ){
        $agent_id 		= $v->value('agent');
        $player_id 		= $v->value('player');
        $mandate_file 	= '';
        $entry_date 	= c_date('Y-m-d', $v->value('entry_date'));
        $expiry_date 	= c_date('Y-m-d', $v->value('expiry_date'));
        $status 		= $v->value('status');
        $status_date 	= c_date('Y-m-d', $v->value('status_date'));
        $status_comment = $v->value('status_comment');

        // mandate_file
	    if( $v->is_pdf('mandate_file') ){
	    	$new_fname = 'Agreement Between '.$a_exist[0]['name'].' And '.$p_exist[0]['name'];
	    	$basename  = $v->file_name('mandate_file','assets/documents/mandates',$new_fname);

	    	list($filename, $extension) = explode('.', $basename, 2);

	    	$basename  = strtoupper(str_replace('_', ' ', $filename)).'.'.$extension;
			$filepath  = DOCUMENT_ROOT.'assets/documents/mandates/'.$basename;

			if(@move_uploaded_file($_FILES[$v->prefix.'mandate_file']['tmp_name'],$filepath)){
				$mandate_file = $basename;
			}
	    }

	    $insert_agent_player = $connect->prepare("
	    	INSERT INTO unisoft_agents_players(
		        agent_id,
		        player_id,
		        mandate_file,
		        entry_date,
		        expiry_date,
		        status,
		        status_date,
		        status_comment
	    	) VALUES(
				:agent_id,
				:player_id,
		        :mandate_file,
		        :entry_date,
		        :expiry_date,
		        :status,
				:status_date,
				:status_comment
	    	)
	    ");
		$insert_agent_player->execute([
			'agent_id' 	 	 => $agent_id,
			'player_id' 	 => $player_id,
	        'mandate_file' 	 => $mandate_file,
	        'entry_date' 	 => $entry_date,
	        'expiry_date' 	 => $expiry_date,
	        'status' 		 => $status,
	        'status_date' 	 => $status_date,
	        'status_comment' => $status_comment
		]);

		$id  = $connect->lastInsertId();
		$row = $p_exist[0];

		$agent_player = [
			'id' 			 => $id,
			'player_id' 	 => $row['player_id'],
			'photo' 		 => photo($row['photo'], $row['sex'], 'players'),
			'name'			 => htmlspecialchars($row['name']),
			'passport_no'	 => htmlspecialchars($row['passport_no']),
			'mandate_file' 	 => $mandate_file,
			'entry_date' 	 => c_date('d-m-Y', $entry_date),
			'expiry_date' 	 => c_date('d-m-Y', $expiry_date),
			'duration' 		 => duration($entry_date, $expiry_date),
			'status' 		 => $status,
			'status_date' 	 => $status_date,
			'status_comment' => $status_comment,
			'hash' 			 => md5($row['player_id'])
		];

		$xhr_response['success'] = 'success';
		$xhr_response['details'] = ['player'=>[$agent_player]];
	}else{
		$xhr_response['errors']  = $errors;
		$xhr_response['success'] = 'error';
	}

	echo json_encode($xhr_response);
	die();
}

// delete record
if( isset($_POST['__delete_record']) ){
	$errors = [];
	$id = intval($_POST['id'] ?? 0);

	if( count($errors) == 0 ){
		$g_file = $connect->query("
			SELECT mandate_file 
			FROM unisoft_agents_players 
			WHERE id=$id
		");

		if( $g_file->rowcount() > 0 ){
			$row  = $g_file->fetch(\PDO::FETCH_ASSOC);
			$file = realpath(DOCUMENT_ROOT.'assets/documents/mandates/'.$row['mandate_file']);

			if( !empty($row['mandate_file']) && file_exists($file) ) @unlink($file);
			$connect->query("DELETE FROM unisoft_agents_players WHERE id=$id");
		}

		$xhr_response['success'] = 'success';
	}else{
		$xhr_response['errors']  = $errors;
		$xhr_response['success'] = 'error';
	}

	echo json_encode($xhr_response);
	die();
}
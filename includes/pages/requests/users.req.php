<?php

require dirname(__DIR__, 3).'/includes/partials/index_ajax.php';
require DOCUMENT_ROOT.'includes/classes/validator.class.php';

use UNISOFT\SMS\VALIDATOR;

if( ($_POST[sha1('__add_user')] ?? '') == 'add new' ){
	$errors = [];
	$v = new VALIDATOR\VALIDATOR('add_user_');

	$v->required('name');
	$v->if_in_array('sex', ['Male', 'Female'], true);
	$v->if_in_array('role', array_keys($user_roles), true);
	$v->if_in_array('status', ['1', '0'], true);

	if( !$v->required('username') ){
		$user_exist = query('users', 'COUNT(*)', [['username', $v->value('username')]]);
		if( $user_exist > 0 ) $errors['username'] = 'Username already exist';
	}

	$v->password('password', true);
    $v->confirm_password('confirm_password', 'password');

	if( count($errors) == 0 ){
		$name 	  = $v->value('name');
        $sex 	  = $v->value('sex');
        $role 	  = $v->value('role');
	    $username = $v->value('username');
        $password = password_hash($v->value('password'), PASSWORD_DEFAULT);
        $status   = $v->value('status');

	    $insert_user = $connect->prepare("
	    	INSERT INTO unisoft_users(
				name,
				sex,
				role,
				username,
				password,
				status
	    	) VALUES(
				:name,
				:sex,
				:role,
				:username,
				:password,
				:status
	    	)
	    ");
		$insert_user->execute([
			'name' 	   => $name,
			'sex' 	   => $sex,
			'role' 	   => $role,
			'username' => $username,
			'password' => $password,
			'status'   => $status
		]);

		$xhr_response['success'] = 'success';
	}else{
		$xhr_response['errors']  = $errors;
		$xhr_response['success'] = 'error';
	}

	echo json_encode($xhr_response);
	die();
}

if( ($_POST[sha1('__add_user')] ?? '') == 'edit' ){
	$errors = [];
	$v = new VALIDATOR\VALIDATOR('add_user_');

	$v->required('name');
	$v->if_in_array('sex', ['Male', 'Female'], true);
	$v->if_in_array('role', array_keys($user_roles), true);
	$v->if_in_array('status', ['1', '0'], true);

    if( $v->is_set('change_password') ){
	    $v->password('password', true);
	    $v->confirm_password('confirm_password', 'password');
    	$password = password_hash($v->value('password'), PASSWORD_DEFAULT);
    }else{
    	$password = $_user_password;
    }

	if( count($errors) == 0 ){
		$id 	= $v->value('id');
		$name 	= $v->value('name');
        $sex 	= $v->value('sex');
        $role 	= $v->value('role');
        $status = $v->value('status');

	    $insert_user = $connect->prepare("
	    	UPDATE unisoft_users SET 
				name 	 = :name,
				sex 	 = :sex,
				role 	 = :role,
				password = :password,
				status 	 = :status
	    	WHERE id=:id
	    ");
		$insert_user->execute([
			'name' 	   => $name,
			'sex' 	   => $sex,
			'role' 	   => $role,
			'password' => $password,
			'status'   => $status,
			'id'   	   => $id
		]);

		$xhr_response['success'] = 'success';
	}else{
		$xhr_response['errors']  = $errors;
		$xhr_response['success'] = 'error';
	}

	echo json_encode($xhr_response);
	die();
}
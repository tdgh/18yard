<?php

require dirname(__DIR__, 3).'/includes/partials/index_ajax.php';
require DOCUMENT_ROOT.'includes/classes/validator.class.php';

use UNISOFT\SMS\VALIDATOR;

if( isset($_POST['__get_income_and_expenses']) ){
	$data = [];

	// get data
	$get_income_and_expenses = $connect->query("
		SELECT 	unisoft_income_and_expenses.*,
				COALESCE(CONCAT(p.first_name, ' ', p.last_name), '') AS player 
		FROM unisoft_income_and_expenses 
		LEFT JOIN unisoft_players p 
		ON p.player_id=unisoft_income_and_expenses.player_id
	");
	
	while($row=$get_income_and_expenses->fetch(\PDO::FETCH_ASSOC)){
		$data[] = [
			'id' 				=> htmlspecialchars($row['id']),
		    'transaction_date' 	=> c_date('d-m-Y', $row['transaction_date']),
			'description' 		=> htmlspecialchars($row['description']),
			'player' 			=> htmlspecialchars($row['player']),
			'income' 			=> $row['type'] == 'income' ? $row['amount'] : '',
			'expenses' 			=> $row['type'] == 'expenses' ? $row['amount'] : '',
			'comment' 			=> htmlspecialchars($row['comment']),
			'timestamp' 		=> strtotime($row['transaction_date'])
		];
	}

	$xhr_response['success'] = 'success';
	$xhr_response['data'] 	 = $data;

	echo json_encode($xhr_response);
	die();
}

if( isset($_POST['__get_select2_players'], $_POST['search']) ){
	$data = [];
	$search = trim($_POST['search']);

	$get_income_and_expenses = $connect->prepare("
		SELECT player_id, CONCAT(first_name, ' ', last_name) AS name, photo, sex, player_number 
		FROM unisoft_players 
		HAVING name 
		LIKE :name 
		ORDER BY name 
		ASC 
		LIMIT 15
	");
	$get_income_and_expenses->execute(['name'=>'%'.$search.'%']);

	while($row=$get_income_and_expenses->fetch(PDO::FETCH_ASSOC)){
		$data[] = [
			'id' 			=> $row['player_id'],
			'player_id' 	=> htmlspecialchars($row['player_id']),
			'photo' 		=> photo($row['photo'], $row['sex'], 'players'),
		    'name' 			=> htmlspecialchars($row['name']),
		    'player_number' => htmlspecialchars($row['player_number']),
		];
	}

	$xhr_response['success'] = true;
	$xhr_response['details'] = [
		'total' => count($data),
		'players' => $data
	];

	echo json_encode($xhr_response);
	die();
}

if( isset($_POST[sha1('__add_income_or_expenses')]) ){
	$errors = [];
	$v = new VALIDATOR\VALIDATOR('add_income_or_expenses_');


	$v->required('transaction_date');
    $v->if_in_array('type', ['income', 'expenses'], true);
	$v->required('description');
	$v->if_exist('player', 'players', [['player_id', $v->value('player')]], false);
	$v->required('amount');


	if( count($errors) == 0 ){
        $transaction_date = c_date('Y-m-d', $v->value('transaction_date'));
        $type 			  = $v->value('type');
        $description 	  = $v->value('description');
        $player_id 		  = $v->value('player');
        $amount 		  = floatval($v->value('amount'));
        $comment 		  = $v->value('comment');

	    $insert_transaction = $connect->prepare("
	    	INSERT INTO unisoft_income_and_expenses(
		        transaction_date,
		        type,
		        description,
		        player_id,
		        amount,
		        comment
	    	) VALUES(
		        :transaction_date,
				:type,
		        :description,
				:player_id,
		        :amount,
				:comment
	    	)
	    ");
		$insert_transaction->execute([
	        'transaction_date' => $transaction_date,
			'type' 	 	 	   => $type,
	        'description' 	   => $description,
			'player_id' 	   => $player_id,
	        'amount' 	 	   => $amount,
	        'comment' 		   => $comment
		]);

		$xhr_response['success'] = 'success';
	}else{
		$xhr_response['errors']  = $errors;
		$xhr_response['success'] = 'error';
	}

	echo json_encode($xhr_response);
	die();
}

// delete record
if( isset($_POST['__delete_record']) ){
	$errors = [];
	$id = intval($_POST['id'] ?? 0);

	if( count($errors) == 0 ){
		$connect->query("DELETE FROM unisoft_income_and_expenses WHERE id=$id");

		$xhr_response['success'] = 'success';
	}else{
		$xhr_response['errors']  = $errors;
		$xhr_response['success'] = 'error';
	}

	echo json_encode($xhr_response);
	die();
}
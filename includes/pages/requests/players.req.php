<?php

require dirname(__DIR__, 3).'/includes/partials/index_ajax.php';

if( isset($_POST['__get_players']) ){
	$data = [];

	// get data
	$get_players = $connect->query("
		SELECT 	unisoft_players.*,
				CONCAT(unisoft_players.first_name, ' ', unisoft_players.last_name) AS name,
				unisoft_users.name AS entry_by 
		FROM unisoft_players 
		INNER JOIN unisoft_users 
		ON unisoft_users.id=unisoft_players.entry_by
	");
	
	while($row=$get_players->fetch(\PDO::FETCH_ASSOC)){
		$data[] = [
			'player_id' 	  	  => htmlspecialchars($row['player_id']),
			'photo' 		 	  => photo($row['photo'], $row['sex'], 'players'),
		    'name' 			 	  => htmlspecialchars($row['name']),
		    'date_of_birth' 	  => c_date('d-m-Y', $row['date_of_birth']),
		    'sex' 				  => htmlspecialchars($row['sex']),
		    'nationality' 		  => htmlspecialchars($row['nationality']),
		    'marital_status' 	  => htmlspecialchars($row['marital_status']),
		    'weight' 			  => htmlspecialchars($row['weight']),
		    'height' 			  => htmlspecialchars($row['height']),
		    'passport_no' 		  => htmlspecialchars($row['passport_no']),
		    'preferred_position'  => htmlspecialchars($row['preferred_position']),
		    'sub_position' 		  => htmlspecialchars($row['sub_position']),
		    'current_club' 		  => htmlspecialchars($row['current_club']),
		    'contract_expiration' => htmlspecialchars($row['contract_expiration']),
		    'level_played' 		  => htmlspecialchars($row['level_played']),
		    'registration_type'   => htmlspecialchars($row['registration_type']),
		    'playing_foot' 		  => htmlspecialchars($row['playing_foot']),
		    'health' 			  => htmlspecialchars($row['health']),
		    'player_profile' 	  => htmlspecialchars($row['player_profile']),
		    'right_foot' 		  => htmlspecialchars($row['right_foot'].'%'),
		    'left_foot' 		  => htmlspecialchars($row['left_foot'].'%'),
		    'speed' 			  => htmlspecialchars($row['speed'].'%'),
		    'athleticism' 		  => htmlspecialchars($row['athleticism'].'%'),
		    'tactics' 			  => htmlspecialchars($row['tactics'].'%'),
		    'hard_work' 		  => htmlspecialchars($row['hard_work'].'%'),
		    'teamwork' 			  => htmlspecialchars($row['teamwork'].'%'),
		    'adaptability' 		  => htmlspecialchars($row['adaptability'].'%'),
		    'personality' 		  => htmlspecialchars($row['personality'].'%'),
		    'phone' 			  => htmlspecialchars($row['phone']),
		    'other_phone' 		  => htmlspecialchars($row['other_phone']),
		    'email' 			  => htmlspecialchars($row['email']),
		    'address' 			  => htmlspecialchars($row['address']),
		    'player_number' 	  => htmlspecialchars($row['player_number']),
		    'contract' 		 	  => htmlspecialchars($row['contract']),
		    'date_joined'   	  => c_date('d-m-Y', $row['date_joined']),
		    'status' 			  => htmlspecialchars($row['status']),
		    'status_date'   	  => c_date('d-m-Y', $row['status_date']),
		    'status_comment' 	  => htmlspecialchars($row['status_comment']),
		    'entry_by' 			  => htmlspecialchars($row['entry_by']),
			'entry_date' 	  	  => c_date('d-m-Y h:ia', $row['entry_date']),
			'hash' 			  	  => md5($row['player_id'])
		];
	}

	$xhr_response['success'] = 'success';
	$xhr_response['data'] 	 = $data;

	echo json_encode($xhr_response);
	die();
}

// delete player
if( isset($_POST['__delete_player']) ){
	$errors = [];
	$player_id = intval($_POST['player_id'] ?? 0);

	// check if records are in use
	$g_record_in_use = $connect->query("
		SELECT COUNT(*) 
		FROM unisoft_income_and_expenses 
		WHERE player_id=$player_id
	");

	if( $g_record_in_use->fetch()[0] > 0 ){
		$errors['error'] = 'Couldn\'t delete seleted player. Player\'s records in use by part of the system';
	}

	if( count($errors) == 0 ){
		$g_player_files = $connect->query("
			SELECT photo, contract 
			FROM unisoft_players 
			WHERE player_id=$player_id
		");

		if( $g_player_files->rowcount() > 0 ){
			$row 	 = $g_player_files->fetch(\PDO::FETCH_ASSOC);
			$photo 	 = realpath(DOCUMENT_ROOT.'assets/images/players/'.$row['photo']);
			$contract= realpath(DOCUMENT_ROOT.'assets/documents/contracts/'.$row['contract']);

			if( !empty($row['photo']) && file_exists($photo) ) @unlink($photo);
			if( !empty($row['contract']) && file_exists($contract) ) @unlink($contract);
			$connect->query("DELETE FROM unisoft_players WHERE player_id=$player_id");
			$connect->query("DELETE FROM unisoft_players_achievements WHERE player_id=$player_id");
			$connect->query("DELETE FROM unisoft_players_career_path WHERE player_id=$player_id");
		}

		$xhr_response['success'] = 'success';
	}else{
		$xhr_response['errors']  = $errors;
		$xhr_response['success'] = 'error';
	}

	echo json_encode($xhr_response);
	die();
}
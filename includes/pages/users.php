<?php

require dirname(__DIR__, 2).'/includes/partials/index.php';

/**
 * @ page dependencies
*/
$page_dependencies = new \stdClass;

$page_dependencies->page_title = 'Users';
$page_dependencies->breadcrumb = [
    'Users' => ''
];
$page_dependencies->plugins = [
	'fancybox',
    'select2',
    'toast'
];
$page_dependencies->js = ['validator.js', 'users.js'];
// end of page dependencies

$users_arr = query(
	'users', 
	['id',
	 'photo',
	 'name',
	 'sex',
	 'role',
	 'username',
	 'status'
	], [], 'name');

require __DIR__.'/views/users.view.php';
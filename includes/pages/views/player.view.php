<!DOCTYPE html>
<html>
<head> <?php include DOCUMENT_ROOT . '/includes/partials/head.php'; ?>
    <style type="text/css"> .n_a {
            color: #888c9b !important;
            font-size: 70%;
            font-weight: 400;
            vertical-align: text-top;
        }

        .table.table-borderless th {
            width: 130px;
            padding: 0.3rem 0.3rem 0.3rem 0;
        }

        .table.table-borderless td {
            padding: 0.3rem 0 0.3rem 0.3rem;
        }

        .table.table-borderless td .progress {
            max-width: 300px;
        }

        .table.table-bordered th, .table.table-bordered td {
            vertical-align: middle !important;
        }

        .btn-group .btn {
            width: 40px;
        }

        .btn-group .btn[data-disabled] {
            cursor: not-allowed;
            opacity: .65;
        } </style>
    <style type="text/css"> .iti {
            width: 100%;
        }

        .iti__selected-flag {
            background-color: #fff !important;
            outline: 0 !important;
            font-size: 0.875rem;
            padding-right: 0;
        }

        .iti--separate-dial-code .iti__arrow {
            display: none;
        }

        .dropify-wrapper {
            height: 150px;
        }

        .dropify-wrapper ~ .dropify-errors-container ul li {
            color: #F34141;
            font-weight: 400;
            font-size: 12px;
        }

        .form-control.is-invalid, .form-control.is-valid, .was-validated .form-control:invalid, .was-validated .form-control:valid {
            z-index: 3;
        }

        #nav-content .nav-link {
            color: #888c9b !important;
        }

        #nav-content .error-nav {
            color: #f35958 !important;
        }

        #career-path .table td input {
            width: 150px;
        }

        #career-path .table th, #career-path .table td {
            vertical-align: middle !important;
            padding: .55rem;
        } </style>
</head>
<body>
<div class="app"> <?php // include DOCUMENT_ROOT.'includes/partials/preloader.php'; ?> <?php include DOCUMENT_ROOT . 'includes/partials/header.php'; ?> <?php include DOCUMENT_ROOT . 'includes/partials/sidebar.php'; ?>
    <main class="app-main">
        <div class="wrapper">
            <div class="page has-sidebar has-sidebar-expand-xl">
                <div class="page-inner">
                    <div class="page-title-bar mb-3"> <?php include DOCUMENT_ROOT . 'includes/partials/breadcrumb.php'; ?>
                        <div class="row text-center text-sm-left">
                            <div class="col-sm-auto col-12 mb-2 mb-sm-0"><a href="javascript:void(0);" data-fancybox
                                                                            data-type="image"
                                                                            data-src="<?= $player_dets["photo"]; ?>"
                                                                            data-caption="<?= $player_dets["name"]; ?>"><img
                                            src="<?= $player_dets["photo"]; ?>" alt=""
                                            class="img-thumbnail rounded-circle border-primary"
                                            style="width: 5rem;height: 5rem;border-width: 2px;"></a></div>
                            <div class="col"><h1 class="page-title"><?= $player_dets["name"]; ?></h1>
                                <p class="text-muted"><?= $player_dets["player_number"]; ?>, <span
                                            class="badge px-2 rounded-0 text-white text-uppercase bg-<?= $player_statuses[$player_dets['status']]['class']; ?>"><?= $player_statuses[$player_dets['status']]['name']; ?></span>
                                </p></div>
                        </div>
                    </div>
                    <div class="page-section">
                        <div class="d-xl-none">
                            <button class="btn btn-danger btn-floated" type="button" data-toggle="sidebar"><i
                                        class="fa fa-th-list"></i></button>
                        </div>
                        <ul class="nav nav-tabs d-none">
                            <li class="nav-item"><a href="#player-details-main-tab" class="nav-link active"
                                                    data-toggle="tab">Player Details</a></li>
                            <li class="nav-item"><a href="#edit-player-main-tab" class="nav-link" data-toggle="tab">Edit
                                    Player</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="player-details-main-tab">
                                <div class="section-block d-flex justify-content-between align-items-baseline"><h1
                                            class="section-title mb-0 p-0">Player Details</h1>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-info btn-gradient rounded-0"
                                                data-toggle="tooltip" title="Edit Details" id="edit-trigger"><i
                                                    class="fa fa-pencil-alt"></i></button>
                                        <button type="button" class="btn btn-info btn-gradient rounded-0" data-fancybox
                                                data-type="iframe"
                                                data-src="<?= DOMAIN; ?>cv?h=<?= $player_dets['hash']; ?>&__=<?= $player_dets['player_id']; ?>&_token=<?= $_SESSION[CSRF_TOKEN_KEY]; ?>"
                                                data-iframe='{"preload":false, "css":{"width":"900px", "height":"100%", "maxWidth":"90%", "maxHeight":"100%"}}'
                                                data-toggle="tooltip" title="View CV"><i class="fa fa-file-pdf"></i>
                                        </button>
                                        <button type="button"
                                                class="btn btn-info btn-gradient rounded-0" <?= $player_dets['contract'] == '' ? 'data-disabled' : 'data-fancybox data-type="iframe" data-src="' . DOMAIN . 'assets/documents/contracts/' . $player_dets['contract'] . '" data-iframe=\'{"preload":false, "css":{"width":"900px", "height":"100%", "maxWidth":"90%", "maxHeight":"100%"}}\''; ?>
                                                data-toggle="tooltip" title="View Contract"><i
                                                    class="fa fa-file-alt"></i></button>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body" id="bio-data-0">
                                        <div class="log-divider text-left"><span class="bg-light"><i
                                                        class="fa fa-fw fa-comment-alt"></i> Bio Data</span></div>
                                        <table class="table table-borderless mb-0">
                                            <tbody>
                                            <tr>
                                                <th>Name</th>
                                                <td><?= disp_n_a($player_dets['name']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Date Of Birth</th>
                                                <td><?= disp_n_a($player_dets['date_of_birth']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Sex</th>
                                                <td><?= disp_n_a($player_dets['sex']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Nationality</th>
                                                <td><?= disp_n_a($player_dets['nationality']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Marital Status</th>
                                                <td><?= disp_n_a($player_dets['marital_status']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Weight</th>
                                                <td><?= disp_n_a($player_dets['weight']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Height</th>
                                                <td><?= disp_n_a($player_dets['height']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Passport No.</th>
                                                <td><?= disp_n_a($player_dets['passport_no']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Preferred Position</th>
                                                <td><?= disp_n_a($player_dets['preferred_position']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Sub Position</th>
                                                <td><?= disp_n_a($player_dets['sub_position']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Current Club</th>
                                                <td><?= disp_n_a($player_dets['current_club']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Contract Expiration</th>
                                                <td><?= disp_n_a($player_dets['contract_expiration']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Level Played</th>
                                                <td><?= disp_n_a($player_dets['level_played']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Registration Type</th>
                                                <td><?= disp_n_a($player_dets['registration_type']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Playing Foot</th>
                                                <td><?= disp_n_a($player_dets['playing_foot']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Health</th>
                                                <td><?= disp_n_a($player_dets['health']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Player Profile</th>
                                                <td><?= disp_n_a($player_dets['player_profile']); ?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="card-body" id="personal-skills-0">
                                        <div class="log-divider text-left mt-0"><span class="bg-light"><i
                                                        class="fa fa-fw fa-comment-alt"></i> Personal Skills</span>
                                        </div>
                                        <table class="table table-borderless mb-0">
                                            <tbody>
                                            <tr>
                                                <th>Right Foot</th>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-striped bg-red"
                                                             role="progressbar"
                                                             style="width: <?= $player_dets['right_foot']; ?>%;"
                                                             aria-valuenow="<?= $player_dets['right_foot']; ?>"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100"> <?= $player_dets['right_foot']; ?>%
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Left Foot</th>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-striped bg-red"
                                                             role="progressbar"
                                                             style="width: <?= $player_dets['left_foot']; ?>%;"
                                                             aria-valuenow="<?= $player_dets['left_foot']; ?>"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100"> <?= $player_dets['left_foot']; ?>%
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Speed</th>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-striped bg-red"
                                                             role="progressbar"
                                                             style="width: <?= $player_dets['speed']; ?>%;"
                                                             aria-valuenow="<?= $player_dets['speed']; ?>"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100"> <?= $player_dets['speed']; ?>%
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Athleticism</th>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-striped bg-red"
                                                             role="progressbar"
                                                             style="width: <?= $player_dets['athleticism']; ?>%;"
                                                             aria-valuenow="<?= $player_dets['athleticism']; ?>"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100"> <?= $player_dets['athleticism']; ?>%
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Tactics</th>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-striped bg-red"
                                                             role="progressbar"
                                                             style="width: <?= $player_dets['tactics']; ?>%;"
                                                             aria-valuenow="<?= $player_dets['tactics']; ?>"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100"> <?= $player_dets['tactics']; ?>%
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Hard Work</th>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-striped bg-red"
                                                             role="progressbar"
                                                             style="width: <?= $player_dets['hard_work']; ?>%;"
                                                             aria-valuenow="<?= $player_dets['hard_work']; ?>"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100"> <?= $player_dets['hard_work']; ?>%
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Teamwork</th>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-striped bg-red"
                                                             role="progressbar"
                                                             style="width: <?= $player_dets['teamwork']; ?>%;"
                                                             aria-valuenow="<?= $player_dets['teamwork']; ?>"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100"> <?= $player_dets['teamwork']; ?>%
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Adaptability</th>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-striped bg-red"
                                                             role="progressbar"
                                                             style="width: <?= $player_dets['adaptability']; ?>%;"
                                                             aria-valuenow="<?= $player_dets['adaptability']; ?>"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100"> <?= $player_dets['adaptability']; ?>%
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Personality</th>
                                                <td>
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-striped bg-red"
                                                             role="progressbar"
                                                             style="width: <?= $player_dets['personality']; ?>%;"
                                                             aria-valuenow="<?= $player_dets['personality']; ?>"
                                                             aria-valuemin="0"
                                                             aria-valuemax="100"> <?= $player_dets['personality']; ?>%
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="card-body" id="achievements-0">
                                        <div class="log-divider text-left mt-0"><span class="bg-light"><i
                                                        class="fa fa-fw fa-comment-alt"></i> Achievements</span></div>
                                        <div class="table-responsive">
                                            <table class="table table-bordered text-nowrap">
                                                <thead class="thead-light">
                                                <tr>
                                                    <th style="width: 70px;">Delete</th>
                                                    <th>Description</th>
                                                </tr>
                                                </thead>
                                                <tbody> <?php if (count($achievements) > 0): ?><?php for ($i = 0; $i < count($achievements); $i++): ?>
                                                    <tr>
                                                        <td class="text-center">
                                                            <button type="button" class="btn btn-sm btn-icon btn-danger"
                                                                    data-id="<?= $achievements[$i]['id']; ?>"
                                                                    data-hash="<?= md5($achievements[$i]['id']); ?>"><i
                                                                        class="far fa-trash-alt"></i></button>
                                                        </td>
                                                        <td><?= htmlspecialchars($achievements[$i]['description']); ?></td>
                                                    </tr> <?php endfor; ?><?php else: ?>
                                                    <tr>
                                                        <td colspan="2">No record found</td>
                                                    </tr> <?php endif; ?> </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="card-body" id="career-path-0">
                                        <div class="log-divider text-left mt-0"><span class="bg-light"><i
                                                        class="fa fa-fw fa-comment-alt"></i> Career Path</span></div>
                                        <div class="table-responsive">
                                            <table class="table table-bordered text-nowrap">
                                                <thead class="thead-light">
                                                <tr>
                                                    <th>Delete</th>
                                                    <th>Period</th>
                                                    <th>Club</th>
                                                    <th>Country</th>
                                                    <th>Matches</th>
                                                    <th>Goals</th>
                                                    <th>Club Level</th>
                                                </tr>
                                                </thead>
                                                <tbody> <?php if (count($career_path) > 0): ?><?php for ($i = 0; $i < count($career_path); $i++): ?>
                                                    <tr>
                                                        <td class="text-center">
                                                            <button type="button" class="btn btn-sm btn-icon btn-danger"
                                                                    data-id="<?= $career_path[$i]['id']; ?>"
                                                                    data-hash="<?= md5($career_path[$i]['id']); ?>"><i
                                                                        class="far fa-trash-alt"></i></button>
                                                        </td>
                                                        <td><?= htmlspecialchars($career_path[$i]['period']); ?></td>
                                                        <td><?= htmlspecialchars($career_path[$i]['club']); ?></td>
                                                        <td><?= htmlspecialchars($career_path[$i]['country']); ?></td>
                                                        <td><?= htmlspecialchars($career_path[$i]['matches']); ?></td>
                                                        <td><?= htmlspecialchars($career_path[$i]['goals']); ?></td>
                                                        <td><?= htmlspecialchars($career_path[$i]['club_level']); ?></td>
                                                    </tr> <?php endfor; ?><?php else: ?>
                                                    <tr>
                                                        <td colspan="7">No record found</td>
                                                    </tr> <?php endif; ?> </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="card-body" id="contact-details-0">
                                        <div class="log-divider text-left mt-0"><span class="bg-light"><i
                                                        class="fa fa-fw fa-comment-alt"></i> Contact Details</span>
                                        </div>
                                        <table class="table table-borderless mb-0">
                                            <tbody>
                                            <tr>
                                                <th>Phone</th>
                                                <td><?= disp_n_a($player_dets['phone']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Other Phone</th>
                                                <td><?= disp_n_a($player_dets['other_phone']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Email</th>
                                                <td><?= disp_n_a($player_dets['email']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Address</th>
                                                <td><?= disp_n_a($player_dets['address']); ?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="card-body" id="agency-details-0">
                                        <div class="log-divider text-left mt-0"><span class="bg-light"><i
                                                        class="fa fa-fw fa-comment-alt"></i> Agency Details</span></div>
                                        <table class="table table-borderless mb-0">
                                            <tbody>
                                            <tr>
                                                <th>Player ID</th>
                                                <td><?= disp_n_a($player_dets['player_number']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Date Joined</th>
                                                <td><?= disp_n_a($player_dets['date_joined']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Status</th>
                                                <td>
                                                    <span class="badge px-2 rounded-0 text-white text-uppercase bg-<?= $player_statuses[$player_dets['status']]['class']; ?>"><?= $player_statuses[$player_dets['status']]['name']; ?></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Status Date</th>
                                                <td><?= disp_n_a($player_dets['status_date']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Status Comment</th>
                                                <td><?= disp_n_a($player_dets['status_comment']); ?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="edit-player-main-tab">
                                <div class="section-block d-flex justify-content-between align-items-baseline"><h1
                                            class="section-title mb-0 p-0">Edit Player Details</h1>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger btn-gradient rounded-0"
                                                data-toggle="tooltip" title="Cancel Edit" id="cancel-edit"><i
                                                    class="fa fa-times-circle"></i></button>
                                    </div>
                                </div>
                                <div class="card">
                                    <form action="" method="post" autocomplete="off" id="edit_player_form"
                                          enctype="multipart/form-data"><input type="hidden"
                                                                               name="<?= sha1('__edit_player'); ?>">
                                        <input type="hidden" name="edit_player_player_id"
                                               value="<?= $player_dets['player_id']; ?>">
                                        <div class="card-body" id="bio-data"><h4 class="card-title mb-4">Bio Data</h4>
                                            <div class="form-row">
                                                <div class="col-lg-6 form-group"><label for="edit_player_first_name">First
                                                        Name <span class="text-danger">*</span></label> <input
                                                            type="text" class="form-control"
                                                            name="edit_player_first_name" id="edit_player_first_name"
                                                            spellcheck="false"
                                                            value="<?= $player_dets['first_name']; ?>">
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                                <div class="col-lg-6 form-group"><label for="edit_player_last_name">Last
                                                        Name <span class="text-danger">*</span></label> <input
                                                            type="text" class="form-control"
                                                            name="edit_player_last_name" id="edit_player_last_name"
                                                            spellcheck="false"
                                                            value="<?= $player_dets['last_name']; ?>">
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-lg-6 form-group"><label for="edit_player_date_of_birth">Date
                                                        Of Birth</label>
                                                    <div class="input-group input-group-alt" data-toggle="flatpickr"
                                                         data-wrap="true" data-date-format="d-m-Y"><input type="text"
                                                                                                          class="form-control"
                                                                                                          name="edit_player_date_of_birth"
                                                                                                          id="edit_player_date_of_birth"
                                                                                                          spellcheck="false"
                                                                                                          value="<?= $player_dets['date_of_birth']; ?>"
                                                                                                          placeholder="DD-MM-YYYY"
                                                                                                          data-input="">
                                                        <div class="input-group-append">
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-toggle=""><i class="far fa-calendar"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-clear=""><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="mr-4 mb-0">Gender <span
                                                            class="text-danger">*</span></label>
                                                <div class="custom-control custom-control-inline custom-radio"><input
                                                            type="radio" class="custom-control-input"
                                                            name="edit_player_sex" id="edit_player_sex_male"
                                                            value="Male" <?= $player_dets['sex'] == 'Male' ? 'checked' : ''; ?>>
                                                    <label class="custom-control-label"
                                                           for="edit_player_sex_male">Male</label></div>
                                                <div class="custom-control custom-control-inline custom-radio"><input
                                                            type="radio" class="custom-control-input"
                                                            name="edit_player_sex" id="edit_player_sex_female"
                                                            value="Female" <?= $player_dets['sex'] == 'Female' ? 'checked' : ''; ?>>
                                                    <label class="custom-control-label" for="edit_player_sex_female">Female</label>
                                                </div>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                            <div class="form-group"><label
                                                        for="edit_player_nationality">Nationality</label> <select
                                                        class="form-control" name="edit_player_nationality"
                                                        id="edit_player_nationality" data-toggle="select2">
                                                    <option value=""></option> <?php foreach ($countries_arr as $country): ?>
                                                        <option value="<?= $country['nationality']; ?>" <?= $player_dets['nationality'] == $country['nationality'] ? 'selected' : ''; ?>><?= $country['nationality']; ?></option> <?php endforeach; ?>
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                            <div class="form-group"><label for="edit_player_marital_status">Marital
                                                    Status</label> <select class="form-control"
                                                                           name="edit_player_marital_status"
                                                                           id="edit_player_marital_status"
                                                                           data-toggle="select2">
                                                    <option value=""></option> <?php foreach ($marital_statuses as $marital_status): ?>
                                                        <option value="<?= $marital_status; ?>" <?= $player_dets['marital_status'] == $marital_status ? 'selected' : ''; ?>><?= $marital_status; ?></option> <?php endforeach; ?>
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-lg-6 form-group"><label
                                                            for="edit_player_weight">Weight</label> <input type="text"
                                                                                                           class="form-control"
                                                                                                           name="edit_player_weight"
                                                                                                           id="edit_player_weight"
                                                                                                           spellcheck="false"
                                                                                                           value="<?= $player_dets['weight']; ?>"
                                                                                                           placeholder="Eg. 74kg">
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                                <div class="col-lg-6 form-group"><label
                                                            for="edit_player_height">Height</label> <input type="text"
                                                                                                           class="form-control"
                                                                                                           name="edit_player_height"
                                                                                                           id="edit_player_height"
                                                                                                           spellcheck="false"
                                                                                                           value="<?= $player_dets['height']; ?>"
                                                                                                           placeholder="Eg. 1m 86cm">
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="form-group"><label for="edit_player_passport_no">Passport
                                                    No.</label> <input type="text" class="form-control"
                                                                       name="edit_player_passport_no"
                                                                       id="edit_player_passport_no" spellcheck="false"
                                                                       value="<?= $player_dets['passport_no']; ?>">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                            <div class="form-group">
                                                <div class="custom-control custom-checkbox"><input type="checkbox"
                                                                                                   class="custom-control-input"
                                                                                                   name="edit_player_change_photo"
                                                                                                   id="edit_player_change_photo"
                                                                                                   value="1"
                                                                                                   data-change-target="#passport-picture-holder">
                                                    <label class="custom-control-label" for="edit_player_change_photo">Change
                                                        Passport Picture</label></div>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                            <div class="form-group clearfix" id="passport-picture-holder"
                                                 style="display: none;"><p class="text-red font-weight-bold mb-0">Please
                                                    note that if no passport picture is selected, current passport
                                                    picture will be deleted.</p> <label for="edit_player_photo"
                                                                                        class="d-block">Passport
                                                    Picture</label>
                                                <div class="float-lg-left mr-lg-3" style="max-width: 150px;"><input
                                                            type="file" name="edit_player_photo" id="edit_player_photo"
                                                            accept="image/*" data-toggle="dropify"
                                                            data-max-file-size="2M"
                                                            data-allowed-file-extensions="gif jpg png jpeg"
                                                            data-errors-position="outside">
                                                    <div class="custom-control custom-checkbox mt-1 text-center"><input
                                                                type="checkbox" class="custom-control-input"
                                                                name="edit_player_photo_auto_resize"
                                                                id="edit_player_photo_auto_resize" checked> <label
                                                                class="custom-control-label"
                                                                for="edit_player_photo_auto_resize">Auto Resize</label>
                                                    </div>
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                                <div class="alert alert-danger has-icon d-lg-inline-block mt-3 mt-lg-0 shadow-none"
                                                     style="border-width: 3px;">
                                                    <div class="alert-icon"><i class="fa fa-info"></i></div>
                                                    <ul class="text-uppercase small pl-3 mb-0">
                                                        <li>Accepts .jpg, .jpeg, png, .gif Only</li>
                                                        <li>A maximum size of 2MB allowed</li>
                                                        <li>Recommended Dimensions 150<em class="text-lowercase">px</em>
                                                            X 150<em class="text-lowercase">px</em></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-lg-6 form-group"><label
                                                            for="edit_player_preferred_position">Preferred
                                                        position</label> <input type="text" class="form-control"
                                                                                name="edit_player_preferred_position"
                                                                                id="edit_player_preferred_position"
                                                                                spellcheck="false"
                                                                                value="<?= $player_dets['preferred_position']; ?>"
                                                                                placeholder="Eg. Striker">
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                                <div class="col-lg-6 form-group"><label for="edit_player_sub_position">Sub
                                                        Position</label> <input type="text" class="form-control"
                                                                                name="edit_player_sub_position"
                                                                                id="edit_player_sub_position"
                                                                                spellcheck="false"
                                                                                value="<?= $player_dets['sub_position']; ?>"
                                                                                placeholder="Eg. 9">
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-lg-6 form-group"><label for="edit_player_current_club">Current
                                                        Club</label> <input type="text" class="form-control"
                                                                            name="edit_player_current_club"
                                                                            id="edit_player_current_club"
                                                                            spellcheck="false"
                                                                            value="<?= $player_dets['current_club']; ?>"
                                                                            placeholder="Eg. Free Agent">
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                                <div class="col-lg-6 form-group"><label
                                                            for="edit_player_contract_expiration">Contract
                                                        Expiration</label> <input type="text" class="form-control"
                                                                                  name="edit_player_contract_expiration"
                                                                                  id="edit_player_contract_expiration"
                                                                                  spellcheck="false"
                                                                                  value="<?= $player_dets['contract_expiration']; ?>"
                                                                                  placeholder="Eg. None">
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-lg-6 form-group"><label for="edit_player_level_played">Level
                                                        of League Played</label> <input type="text" class="form-control"
                                                                                        name="edit_player_level_played"
                                                                                        id="edit_player_level_played"
                                                                                        spellcheck="false"
                                                                                        value="<?= $player_dets['level_played']; ?>"
                                                                                        placeholder="Eg. Division 1">
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                                <div class="col-lg-6 form-group"><label
                                                            for="edit_player_registration_type">Type of Player
                                                        registration</label> <input type="text" class="form-control"
                                                                                    name="edit_player_registration_type"
                                                                                    id="edit_player_registration_type"
                                                                                    spellcheck="false"
                                                                                    value="<?= $player_dets['registration_type']; ?>"
                                                                                    placeholder="Eg. Professional">
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="form-group"><label for="edit_player_playing_foot">Playing
                                                    Foot</label> <input type="text" class="form-control"
                                                                        name="edit_player_playing_foot"
                                                                        id="edit_player_playing_foot" spellcheck="false"
                                                                        value="<?= $player_dets['playing_foot']; ?>"
                                                                        placeholder="Eg. Both Feet but right foot preferred">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                            <div class="form-group"><label for="edit_player_health">Health
                                                    Condition</label> <input type="text" class="form-control"
                                                                             name="edit_player_health"
                                                                             id="edit_player_health" spellcheck="false"
                                                                             value="<?= $player_dets['health']; ?>"
                                                                             placeholder="Eg. No history of Injury">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                            <div class="form-group"><label for="edit_player_player_profile">Player
                                                    Profile</label> <textarea class="form-control"
                                                                              name="edit_player_player_profile"
                                                                              id="edit_player_player_profile"
                                                                              spellcheck="false"
                                                                              placeholder="Eg. Short description of the player"
                                                                              rows="4"><?= $player_dets['player_profile']; ?></textarea>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="card-body border-top" id="personal-skills"><h4
                                                    class="card-title mb-4">Personal Skills</h4>
                                            <div class="row">
                                                <div class="col-lg-4 form-group mb-4"><label
                                                            for="edit_player_right_foot">Right Foot</label> <input
                                                            type="text" class="form-control"
                                                            name="edit_player_right_foot" id="edit_player_right_foot"
                                                            spellcheck="false"
                                                            value="<?= $player_dets['right_foot']; ?>"
                                                            data-toggle="ionRangeSlider" data-min="0" data-max="100"
                                                            data-grid="true" data-step="1" data-skin="flat">
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                                <div class="col-lg-4 form-group mb-4"><label
                                                            for="edit_player_left_foot">Left Foot</label> <input
                                                            type="text" class="form-control"
                                                            name="edit_player_left_foot" id="edit_player_left_foot"
                                                            spellcheck="false" value="<?= $player_dets['left_foot']; ?>"
                                                            data-toggle="ionRangeSlider" data-min="0" data-max="100"
                                                            data-grid="true" data-step="1" data-skin="flat">
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                                <div class="col-lg-4 form-group mb-4"><label for="edit_player_speed">Speed</label>
                                                    <input type="text" class="form-control" name="edit_player_speed"
                                                           id="edit_player_speed" spellcheck="false"
                                                           value="<?= $player_dets['speed']; ?>"
                                                           data-toggle="ionRangeSlider" data-min="0" data-max="100"
                                                           data-grid="true" data-step="1" data-skin="flat">
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                                <div class="col-lg-4 form-group mb-4"><label
                                                            for="edit_player_athleticism">Athleticism</label> <input
                                                            type="text" class="form-control"
                                                            name="edit_player_athleticism" id="edit_player_athleticism"
                                                            spellcheck="false"
                                                            value="<?= $player_dets['athleticism']; ?>"
                                                            data-toggle="ionRangeSlider" data-min="0" data-max="100"
                                                            data-grid="true" data-step="1" data-skin="flat">
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                                <div class="col-lg-4 form-group mb-4"><label for="edit_player_tactics">Tactics</label>
                                                    <input type="text" class="form-control" name="edit_player_tactics"
                                                           id="edit_player_tactics" spellcheck="false"
                                                           value="<?= $player_dets['tactics']; ?>"
                                                           data-toggle="ionRangeSlider" data-min="0" data-max="100"
                                                           data-grid="true" data-step="1" data-skin="flat">
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                                <div class="col-lg-4 form-group mb-4"><label
                                                            for="edit_player_hard_work">Hard work</label> <input
                                                            type="text" class="form-control"
                                                            name="edit_player_hard_work" id="edit_player_hard_work"
                                                            spellcheck="false" value="<?= $player_dets['hard_work']; ?>"
                                                            data-toggle="ionRangeSlider" data-min="0" data-max="100"
                                                            data-grid="true" data-step="1" data-skin="flat">
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                                <div class="col-lg-4 form-group mb-4"><label for="edit_player_teamwork">Teamwork</label>
                                                    <input type="text" class="form-control" name="edit_player_teamwork"
                                                           id="edit_player_teamwork" spellcheck="false"
                                                           value="<?= $player_dets['teamwork']; ?>"
                                                           data-toggle="ionRangeSlider" data-min="0" data-max="100"
                                                           data-grid="true" data-step="1" data-skin="flat">
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                                <div class="col-lg-4 form-group mb-4"><label
                                                            for="edit_player_adaptability">Adaptability</label> <input
                                                            type="text" class="form-control"
                                                            name="edit_player_adaptability"
                                                            id="edit_player_adaptability" spellcheck="false"
                                                            value="<?= $player_dets['adaptability']; ?>"
                                                            data-toggle="ionRangeSlider" data-min="0" data-max="100"
                                                            data-grid="true" data-step="1" data-skin="flat">
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                                <div class="col-lg-4 form-group mb-4"><label
                                                            for="edit_player_personality">Personality</label> <input
                                                            type="text" class="form-control"
                                                            name="edit_player_personality" id="edit_player_personality"
                                                            spellcheck="false"
                                                            value="<?= $player_dets['personality']; ?>"
                                                            data-toggle="ionRangeSlider" data-min="0" data-max="100"
                                                            data-grid="true" data-step="1" data-skin="flat">
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body border-top" id="achievements"><h4 class="card-title mb-4">
                                                Achievements</h4>
                                            <div id="achievements-repeater">
                                                <div data-repeater-list="edit_player_achievements">
                                                    <div class="d-block" data-repeater-item="achievements">
                                                        <div class="form-group">
                                                            <div class="input-group input-group-alt"><input type="text"
                                                                                                            class="form-control"
                                                                                                            name="description"
                                                                                                            spellcheck="false"
                                                                                                            placeholder="Eg. Top goals scorer in GPL 2018-2019">
                                                                <span class="input-group-append"> <button type="button"
                                                                                                          class="btn btn-danger"
                                                                                                          data-repeater-delete="achievements"><i
                                                                                class="far fa-trash-alt"></i></button> </span>
                                                            </div>
                                                            <div class="invalid-feedback"></div>
                                                        </div>
                                                        <hr>
                                                    </div>
                                                </div>
                                                <p class="mt-2 mb-0">
                                                    <button type="button" class="btn btn-link btn-sm"
                                                            data-repeater-create="achievements"><i
                                                                class="fas fa-plus text-small mr-2"></i> Add New
                                                    </button>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="card-body border-top" id="career-path"><h4 class="card-title mb-4">
                                                Career Path</h4>
                                            <div id="career-path-repeater">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered text-nowrap">
                                                        <thead class="thead-light">
                                                        <tr>
                                                            <th>Delete</th>
                                                            <th>Period</th>
                                                            <th>Club</th>
                                                            <th>Country</th>
                                                            <th>Matches</th>
                                                            <th>Goals</th>
                                                            <th>Club Level</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody data-repeater-list="edit_player_career_path">
                                                        <tr class="d-table-row" data-repeater-item="career-path">
                                                            <td class="text-center">
                                                                <button type="button"
                                                                        class="btn btn-sm btn-icon btn-danger"
                                                                        data-repeater-delete="career-path"><i
                                                                            class="far fa-trash-alt"></i></button>
                                                            </td>
                                                            <td><input type="text" class="form-control form-control-sm"
                                                                       name="period" spellcheck="false"
                                                                       placeholder="Eg. 2014-2016"></td>
                                                            <td><input type="text" class="form-control form-control-sm"
                                                                       name="club" spellcheck="false"
                                                                       placeholder="Eg. Barcelona"></td>
                                                            <td><input type="text" class="form-control form-control-sm"
                                                                       name="country" spellcheck="false"
                                                                       placeholder="Eg. Spain"></td>
                                                            <td><input type="text" class="form-control form-control-sm"
                                                                       name="matches" spellcheck="false"
                                                                       placeholder="Eg. 15"></td>
                                                            <td><input type="text" class="form-control form-control-sm"
                                                                       name="goals" spellcheck="false"
                                                                       placeholder="Eg. 10"></td>
                                                            <td><input type="text" class="form-control form-control-sm"
                                                                       name="club_level" spellcheck="false"
                                                                       placeholder="Eg. Division 1"></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <small class="form-text text-red"><strong>Note!</strong> Rows without
                                                    <em style="text-decoration: underline;">Period</em> will be ignored
                                                </small>
                                                <p class="mt-2 mb-0">
                                                    <button type="button" class="btn btn-link btn-sm"
                                                            data-repeater-create="career-path"><i
                                                                class="fas fa-plus text-small mr-2"></i> Add New
                                                    </button>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="card-body border-top" id="contact-details"><h4
                                                    class="card-title mb-4">Contact Details</h4>
                                            <div class="form-group"><label for="edit_player_phone">Phone</label> <input
                                                        type="text" class="form-control" name="edit_player_phone"
                                                        id="edit_player_phone" spellcheck="false"
                                                        value="<?= $player_dets['phone']; ?>" data-toggle="intltelinput"
                                                        data-auto-placeholder="aggressive"
                                                        data-separate-dial-code="true"
                                                        data-preferred-countries='["<?= strtolower($__settings['country_details']['initials']); ?>"]'>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                            <div class="form-group"><label for="edit_player_other_phone">Other Phone
                                                    No.</label> <input type="text" class="form-control"
                                                                       name="edit_player_other_phone"
                                                                       id="edit_player_other_phone" spellcheck="false"
                                                                       value="<?= $player_dets['other_phone']; ?>"
                                                                       data-toggle="intltelinput"
                                                                       data-auto-placeholder="aggressive"
                                                                       data-separate-dial-code="true"
                                                                       data-preferred-countries='["<?= strtolower($__settings['country_details']['initials']); ?>"]'>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                            <div class="form-group"><label for="edit_player_email">Email</label> <input
                                                        type="text" class="form-control" name="edit_player_email"
                                                        id="edit_player_email" spellcheck="false"
                                                        value="<?= $player_dets['email']; ?>">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                            <div class="form-group"><label for="edit_player_address">Address</label>
                                                <textarea class="form-control" name="edit_player_address"
                                                          id="edit_player_address" spellcheck="false"
                                                          placeholder="Eg. House No. 123, Awoshie, Near the market"
                                                          rows="4"><?= $player_dets['address']; ?></textarea>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="card-body border-top" id="agency-details"><h4
                                                    class="card-title mb-4">Agency Details</h4>
                                            <div class="form-group"><label for="edit_player_player_number">Player ID
                                                    <span class="text-danger">*</span></label> <input type="text"
                                                                                                      class="form-control"
                                                                                                      name="edit_player_player_number"
                                                                                                      id="edit_player_player_number"
                                                                                                      spellcheck="false"
                                                                                                      value="<?= $player_dets['player_number']; ?>"
                                                                                                      placeholder="Eg. <?= $config['INITIAL'] . date('my') . '001'; ?>">
                                                <div class="invalid-feedback"></div>
                                            </div>
                                            <div class="form-group">
                                                <div class="custom-control custom-checkbox"><input type="checkbox"
                                                                                                   class="custom-control-input"
                                                                                                   name="edit_player_change_contract"
                                                                                                   id="edit_player_change_contract"
                                                                                                   value="1"
                                                                                                   data-change-target="#contract-holder">
                                                    <label class="custom-control-label"
                                                           for="edit_player_change_contract">Change Contract</label>
                                                </div>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                            <div class="form-group clearfix" id="contract-holder"
                                                 style="display: none;"><p class="text-red font-weight-bold mb-0">Please
                                                    note that if no contract is uploaded, current contract will be
                                                    deleted.</p> <label for="edit_player_contract">Upload
                                                    Contract</label>
                                                <div class="custom-file"><input type="file" class="custom-file-input"
                                                                                name="edit_player_contract"
                                                                                id="edit_player_contract" accept=".pdf">
                                                    <label class="custom-file-label" for="edit_player_contract">Choose a
                                                        PDF file</label></div>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-lg-6 form-group"><label for="edit_player_date_joined">Date
                                                        Joined</label>
                                                    <div class="input-group input-group-alt" data-toggle="flatpickr"
                                                         data-wrap="true" data-date-format="d-m-Y"><input type="text"
                                                                                                          class="form-control"
                                                                                                          name="edit_player_date_joined"
                                                                                                          id="edit_player_date_joined"
                                                                                                          spellcheck="false"
                                                                                                          value="<?= $player_dets['date_joined']; ?>"
                                                                                                          placeholder="DD-MM-YYYY"
                                                                                                          data-input="">
                                                        <div class="input-group-append">
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-toggle=""><i class="far fa-calendar"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-clear=""><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="form-group"><label for="edit_player_status">Status <span
                                                            class="text-danger">*</span></label> <select
                                                        class="form-control" name="edit_player_status"
                                                        id="edit_player_status" data-toggle="select2">
                                                    <option value=""></option> <?php foreach ($player_statuses as $status => $status_dets): ?>
                                                        <option value="<?= $status; ?>" <?= $player_dets['status'] == $status ? 'selected' : ''; ?>><?= strtoupper($status_dets['name']); ?></option> <?php endforeach; ?>
                                                </select>
                                                <div class="invalid-feedback"></div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-lg-6 form-group"><label for="edit_player_status_date">Status
                                                        Date</label>
                                                    <div class="input-group input-group-alt" data-toggle="flatpickr"
                                                         data-wrap="true" data-date-format="d-m-Y"><input type="text"
                                                                                                          class="form-control"
                                                                                                          name="edit_player_status_date"
                                                                                                          id="edit_player_status_date"
                                                                                                          spellcheck="false"
                                                                                                          value="<?= $player_dets['status_date']; ?>"
                                                                                                          placeholder="DD-MM-YYYY"
                                                                                                          data-input="">
                                                        <div class="input-group-append">
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-toggle=""><i class="far fa-calendar"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-clear=""><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                    <small class="form-text text-muted">Eg. Date sold, suspended, etc
                                                    </small>
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="form-group"><label for="edit_player_status_comment">Status
                                                    Comment</label> <textarea class="form-control"
                                                                              name="edit_player_status_comment"
                                                                              id="edit_player_status_comment"
                                                                              spellcheck="false"
                                                                              placeholder="Eg. Reason why the player was suspended"
                                                                              rows="4"><?= $player_dets['status_comment']; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="card-body border-top" id="save-or-reset">
                                            <button type="submit" class="btn btn-primary px-4">Save</button>
                                            <button type="reset" class="btn btn-secondary px-3">Reset</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="page-sidebar page-sidebar-fixed">
                    <header class="sidebar-header d-xl-none">
                        <ol class="breadcrumb mb-0">
                            <li class="breadcrumb-item"><a class="prevent-default" href="#"
                                                           onclick="Looper.toggleSidebar()"><i
                                            class="breadcrumb-icon fa fa-angle-left mr-2"></i>Back</a></li>
                        </ol>
                    </header>
                    <nav id="nav-content" class="nav flex-column mt-4"><h5 class="px-3 mt-0">Quick Scroll Links</h5>
                        <ul class="nav nav-tabs d-none">
                            <li class="nav-item"><a href="#player-details-sidebar-tab" class="nav-link active"
                                                    data-toggle="tab">Player Details</a></li>
                            <li class="nav-item"><a href="#edit-player-sidebar-tab" class="nav-link" data-toggle="tab">Edit
                                    Player</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="player-details-sidebar-tab"><a href="#bio-data-0"
                                                                                                      class="nav-link smooth-scroll">Bio
                                    Data</a> <a href="#personal-skills-0" class="nav-link smooth-scroll">Personal
                                    Skills</a> <a href="#achievements-0" class="nav-link smooth-scroll">Achievements</a>
                                <a href="#career-path-0" class="nav-link smooth-scroll">Career Path</a> <a
                                        href="#contact-details-0" class="nav-link smooth-scroll">Contact Details</a> <a
                                        href="#agency-details-0" class="nav-link smooth-scroll">Agency Details</a></div>
                            <div class="tab-pane fade" id="edit-player-sidebar-tab"><a href="#bio-data"
                                                                                       class="nav-link smooth-scroll">Bio
                                    Data</a> <a href="#personal-skills" class="nav-link smooth-scroll">Personal
                                    Skills</a> <a href="#achievements" class="nav-link smooth-scroll">Achievements</a>
                                <a href="#career-path" class="nav-link smooth-scroll">Career Path</a> <a
                                        href="#contact-details" class="nav-link smooth-scroll">Contact Details</a> <a
                                        href="#agency-details" class="nav-link smooth-scroll">Agency Details</a> <a
                                        href="#save-or-reset" class="nav-link smooth-scroll">Save / Reset</a></div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </main>
</div> <?php include DOCUMENT_ROOT . 'includes/partials/script.php'; ?> </body>
</html>
<!DOCTYPE html>
<html>
<head> <?php include DOCUMENT_ROOT . '/includes/partials/head.php'; ?>
    <style type="text/css">.form-control[disabled] {
            cursor: not-allowed
        }</style>
</head>
<body>
<div class="app"> <?php // include DOCUMENT_ROOT.'includes/partials/preloader.php'; ?> <?php include DOCUMENT_ROOT . 'includes/partials/header.php'; ?> <?php include DOCUMENT_ROOT . 'includes/partials/sidebar.php'; ?>
    <main class="app-main">
        <div class="wrapper">
            <div class="page">
                <div class="page-inner">
                    <div class="page-title-bar">
                        <div class="d-sm-flex justify-content-sm-between align-items-sm-center">
                            <div> <?php include DOCUMENT_ROOT . 'includes/partials/breadcrumb.php'; ?> <h1
                                        class="page-title"><?= $page_dependencies->page_title; ?></h1></div>
                            <button type="button" class="btn btn-secondary px-3 rounded-0" id="add-new-user"><i
                                        class="fa fa-plus mr-2"></i> Add New
                            </button>
                        </div>
                    </div>
                    <div class="page-section">
                        <div class="card card-fluid rounded-0"
                             style="border-top: 3px solid #346cb0;"> <?php for ($i = 0; $i < count($users_arr); $i++): ?><?php $user = $users_arr[$i]; ?>
                                <div class="list-group list-group-flush list-group-divider">
                                    <div class="list-group-item">
                                        <div class="list-group-item-figure"> <?php $photo = photo($user['photo'], $user['sex'], 'users', 'url', $user['name']); ?>
                                            <a href="javascript:void(0);" class="user-avatar user-avatar-md"
                                               data-src="<?= $photo; ?>" data-fancybox data-type="image"
                                               class="tile tile-img mr-1"
                                               data-caption="<?= htmlspecialchars($user['name']); ?>"><img
                                                        src="<?= $photo; ?>" alt=""></a></div>
                                        <div class="list-group-item-body">
                                            <div class="d-sm-flex justify-content-sm-between align-items-sm-center">
                                                <div class="team"><h4
                                                            class="list-group-item-title"><?= htmlspecialchars($user['name']); ?>
                                                        -
                                                        <span class="small"><?= htmlspecialchars($user['username']); ?></span>
                                                    </h4>
                                                    <p class="list-group-item-text"><?= $user_roles[$user['role']]; ?></p>
                                                </div>
                                                <ul class="list-inline text-muted mb-0">
                                                    <li class="list-inline-item"><span
                                                                class="badge px-2 text-uppercase rounded-0 badge-<?= $user_statuses[$user['status']]['class']; ?>"><?= $user_statuses[$user['status']]['name']; ?></span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="list-group-item-figure">
                                            <button type="button" class="btn btn-sm btn-icon btn-secondary edit-user"
                                                    data-idx="<?= $i; ?>"><i class="fa fa-pencil-alt"></i></button>
                                        </div>
                                    </div>
                                </div> <?php endfor; ?> </div>
                    </div>
                    <div class="modal fade" id="add-user-modal" tabindex="-1" role="dialog"
                         aria-labelledby="addUserModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                <div class="modal-header rounded-0 bg-blue text-white"><h6 class="modal-title"
                                                                                           id="addUserModalLabel"><i
                                                class="fa fa-user-circle mr-2"></i> <span class="text-capitalize"
                                                                                          id="action-title">Add New</span>
                                        User</h6>
                                    <button type="button" class="close" data-dismiss="modal"
                                            style="text-shadow: 0 1px 0 #fff;font-size: 1.3rem;"><i
                                                class="fa fa-times"></i></button>
                                </div>
                                <div class="modal-body">
                                    <div class="card card-shadow m-0">
                                        <form action="" method="post" autocomplete="off" id="add_user_form"><input
                                                    type="hidden" name="<?= sha1('__add_user'); ?>" id="__add_user"
                                                    value="add new"> <input type="hidden" name="add_user_id"
                                                                            id="add_user_id">
                                            <div class="card-body">
                                                <div class="form-group"><label for="add_user_name">Name <span
                                                                class="text-danger">*</span></label> <input type="text"
                                                                                                            class="form-control"
                                                                                                            name="add_user_name"
                                                                                                            id="add_user_name"
                                                                                                            spellcheck="false">
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                                <div class="form-group"><label class="mr-3 mb-0">Gender <span
                                                                class="text-danger">*</span></label>
                                                    <div class="custom-control custom-control-inline custom-radio">
                                                        <input type="radio" class="custom-control-input"
                                                               name="add_user_sex" id="add_user_sex_male" value="Male">
                                                        <label class="custom-control-label"
                                                               for="add_user_sex_male">Male</label></div>
                                                    <div class="custom-control custom-control-inline custom-radio">
                                                        <input type="radio" class="custom-control-input"
                                                               name="add_user_sex" id="add_user_sex_female"
                                                               value="Female"> <label class="custom-control-label"
                                                                                      for="add_user_sex_female">Female</label>
                                                    </div>
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                                <div class="form-group"><label for="add_user_role">Role <span
                                                                class="text-danger">*</span></label> <select
                                                            class="form-control" name="add_user_role" id="add_user_role"
                                                            data-toggle="select2"
                                                            data-dropdown-parent="#add-user-modal">
                                                        <option></option> <?php foreach ($user_roles as $r_id => $r_name): ?>
                                                            <option value="<?= $r_id; ?>"><?= $r_name; ?></option> <?php endforeach; ?>
                                                    </select>
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                                <div class="form-group"><label class="mr-3 mb-0">Status <span
                                                                class="text-danger">*</span></label>
                                                    <div class="custom-control custom-control-inline custom-radio">
                                                        <input type="radio" class="custom-control-input"
                                                               name="add_user_status" id="add_user_status_1" value="1"
                                                               checked> <label class="custom-control-label"
                                                                               for="add_user_status_1">Active</label>
                                                    </div>
                                                    <div class="custom-control custom-control-inline custom-radio">
                                                        <input type="radio" class="custom-control-input"
                                                               name="add_user_status" id="add_user_status_0" value="0">
                                                        <label class="custom-control-label" for="add_user_status_0">Inactive</label>
                                                    </div>
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                                <div class="form-group"><label for="add_user_username">Username <span
                                                                class="text-danger">*</span></label> <input type="text"
                                                                                                            class="form-control"
                                                                                                            name="add_user_username"
                                                                                                            id="add_user_username"
                                                                                                            spellcheck="false">
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                                <label class="w-100 mb-3 btn-link" style="display: none;"><input
                                                            type="checkbox" name="add_user_change_password"
                                                            id="add_user_change_password" checked
                                                            style="display: none;"> Change Password</label>
                                                <div id="password-holder">
                                                    <div class="form-group"><label for="add_user_password">Password
                                                            <span class="text-danger">*</span></label> <input
                                                                type="password" class="form-control"
                                                                name="add_user_password" id="add_user_password"
                                                                spellcheck="false">
                                                        <div class="invalid-feedback"></div>
                                                    </div>
                                                    <div class="form-group"><label for="add_user_confirm_password">Confirm
                                                            Password <span class="text-danger">*</span></label> <input
                                                                type="password" class="form-control"
                                                                name="add_user_confirm_password"
                                                                id="add_user_confirm_password" spellcheck="false">
                                                        <div class="invalid-feedback"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="modal-footer bg-light justify-content-between">
                                    <div>
                                        <button type="submit" class="btn btn-primary" form="add_user_form">Save</button>
                                        <button type="reset" class="btn btn-danger" form="add_user_form"
                                                id="action-reset">Reset
                                        </button>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <?php include DOCUMENT_ROOT . 'includes/partials/footer.php'; ?> </div>
            </div>
        </div>
    </main>
</div>
<script type="text/javascript">var users = <?= json_encode($users_arr); ?>;</script> <?php include DOCUMENT_ROOT . 'includes/partials/script.php'; ?>
</body>
</html>
<!DOCTYPE html>
<html>
<head> <?php include DOCUMENT_ROOT . '/includes/partials/head.php'; ?>
    <style type="text/css"> .iti {
            width: 100%;
        }

        .iti__selected-flag {
            background-color: #fff !important;
            outline: 0 !important;
            font-size: 0.875rem;
            padding-right: 0;
        }

        .iti--separate-dial-code .iti__arrow {
            display: none;
        }

        .dropify-wrapper {
            height: 150px;
        }

        .dropify-wrapper ~ .dropify-errors-container ul li {
            color: #F34141;
            font-weight: 400;
            font-size: 12px;
        }

        .form-control.is-invalid, .form-control.is-valid, .was-validated .form-control:invalid, .was-validated .form-control:valid {
            z-index: 3;
        }

        .error-nav {
            color: #f35958;
        }

        #career-path .table td input {
            width: 150px;
        }

        #career-path .table th, #career-path .table td {
            vertical-align: middle !important;
            padding: .55rem;
        } </style>
</head>
<body>
<div class="app"> <?php // include DOCUMENT_ROOT.'includes/partials/preloader.php'; ?> <?php include DOCUMENT_ROOT . 'includes/partials/header.php'; ?> <?php include DOCUMENT_ROOT . 'includes/partials/sidebar.php'; ?>
    <main class="app-main">
        <div class="wrapper">
            <div class="page has-sidebar has-sidebar-expand-xl">
                <div class="page-inner">
                    <div class="page-title-bar"> <?php include DOCUMENT_ROOT . 'includes/partials/breadcrumb.php'; ?>
                        <h1 class="page-title"><?= $page_dependencies->page_title; ?></h1></div>
                    <div class="page-section">
                        <div class="d-xl-none">
                            <button class="btn btn-danger btn-floated" type="button" data-toggle="sidebar"><i
                                        class="fa fa-th-list"></i></button>
                        </div>
                        <div class="card">
                            <form action="" method="post" autocomplete="off" id="add_player_form"
                                  enctype="multipart/form-data"><input type="hidden"
                                                                       name="<?= sha1('__add_player'); ?>">
                                <div class="card-body" id="bio-data"><h4 class="card-title mb-4">Bio Data</h4>
                                    <div class="form-row">
                                        <div class="col-lg-6 form-group"><label for="add_player_first_name">First Name
                                                <span class="text-danger">*</span></label> <input type="text"
                                                                                                  class="form-control"
                                                                                                  name="add_player_first_name"
                                                                                                  id="add_player_first_name"
                                                                                                  spellcheck="false">
                                            <div class="invalid-feedback"></div>
                                        </div>
                                        <div class="col-lg-6 form-group"><label for="add_player_last_name">Last Name
                                                <span class="text-danger">*</span></label> <input type="text"
                                                                                                  class="form-control"
                                                                                                  name="add_player_last_name"
                                                                                                  id="add_player_last_name"
                                                                                                  spellcheck="false">
                                            <div class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-lg-6 form-group"><label for="add_player_date_of_birth">Date Of
                                                Birth</label>
                                            <div class="input-group input-group-alt" data-toggle="flatpickr"
                                                 data-wrap="true" data-date-format="d-m-Y"><input type="text"
                                                                                                  class="form-control"
                                                                                                  name="add_player_date_of_birth"
                                                                                                  id="add_player_date_of_birth"
                                                                                                  spellcheck="false"
                                                                                                  placeholder="DD-MM-YYYY"
                                                                                                  data-input="">
                                                <div class="input-group-append">
                                                    <button type="button" class="btn btn-secondary" data-toggle=""><i
                                                                class="far fa-calendar"></i></button>
                                                    <button type="button" class="btn btn-secondary" data-clear=""><i
                                                                class="fa fa-times"></i></button>
                                                </div>
                                            </div>
                                            <div class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label class="mr-4 mb-0">Gender <span
                                                    class="text-danger">*</span></label>
                                        <div class="custom-control custom-control-inline custom-radio"><input
                                                    type="radio" class="custom-control-input" name="add_player_sex"
                                                    id="add_player_sex_male" value="Male"> <label
                                                    class="custom-control-label" for="add_player_sex_male">Male</label>
                                        </div>
                                        <div class="custom-control custom-control-inline custom-radio"><input
                                                    type="radio" class="custom-control-input" name="add_player_sex"
                                                    id="add_player_sex_female" value="Female"> <label
                                                    class="custom-control-label"
                                                    for="add_player_sex_female">Female</label></div>
                                        <div class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group"><label for="add_player_nationality">Nationality</label>
                                        <select class="form-control" name="add_player_nationality"
                                                id="add_player_nationality" data-toggle="select2">
                                            <option value=""></option> <?php foreach ($countries_arr as $country): ?>
                                                <option value="<?= $country['nationality']; ?>"><?= $country['nationality']; ?></option> <?php endforeach; ?>
                                        </select>
                                        <div class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group"><label for="add_player_marital_status">Marital
                                            Status</label> <select class="form-control" name="add_player_marital_status"
                                                                   id="add_player_marital_status" data-toggle="select2">
                                            <option value=""></option> <?php foreach ($marital_statuses as $marital_status): ?>
                                                <option value="<?= $marital_status; ?>"><?= $marital_status; ?></option> <?php endforeach; ?>
                                        </select>
                                        <div class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-lg-6 form-group"><label for="add_player_weight">Weight</label>
                                            <input type="text" class="form-control" name="add_player_weight"
                                                   id="add_player_weight" spellcheck="false" placeholder="Eg. 74kg">
                                            <div class="invalid-feedback"></div>
                                        </div>
                                        <div class="col-lg-6 form-group"><label for="add_player_height">Height</label>
                                            <input type="text" class="form-control" name="add_player_height"
                                                   id="add_player_height" spellcheck="false" placeholder="Eg. 1m 86cm">
                                            <div class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label for="add_player_passport_no">Passport No.</label>
                                        <input type="text" class="form-control" name="add_player_passport_no"
                                               id="add_player_passport_no" spellcheck="false">
                                        <div class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group clearfix"><label for="add_player_photo" class="d-block">Passport
                                            Picture</label>
                                        <div class="float-lg-left mr-lg-3" style="max-width: 150px;"><input type="file"
                                                                                                            name="add_player_photo"
                                                                                                            id="add_player_photo"
                                                                                                            accept="image/*"
                                                                                                            data-toggle="dropify"
                                                                                                            data-max-file-size="2M"
                                                                                                            data-allowed-file-extensions="gif jpg png jpeg"
                                                                                                            data-errors-position="outside">
                                            <div class="custom-control custom-checkbox mt-1 text-center"><input
                                                        type="checkbox" class="custom-control-input"
                                                        name="add_player_photo_auto_resize"
                                                        id="add_player_photo_auto_resize" checked> <label
                                                        class="custom-control-label" for="add_player_photo_auto_resize">Auto
                                                    Resize</label></div>
                                            <div class="invalid-feedback"></div>
                                        </div>
                                        <div class="alert alert-danger has-icon d-lg-inline-block mt-3 mt-lg-0 shadow-none"
                                             style="border-width: 3px;">
                                            <div class="alert-icon"><i class="fa fa-info"></i></div>
                                            <ul class="text-uppercase small pl-3 mb-0">
                                                <li>Accepts .jpg, .jpeg, png, .gif Only</li>
                                                <li>A maximum size of 2MB allowed</li>
                                                <li>Recommended Dimensions 150<em class="text-lowercase">px</em> X
                                                    150<em class="text-lowercase">px</em></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-lg-6 form-group"><label for="add_player_preferred_position">Preferred
                                                position</label> <input type="text" class="form-control"
                                                                        name="add_player_preferred_position"
                                                                        id="add_player_preferred_position"
                                                                        spellcheck="false" placeholder="Eg. Striker">
                                            <div class="invalid-feedback"></div>
                                        </div>
                                        <div class="col-lg-6 form-group"><label for="add_player_sub_position">Sub
                                                Position</label> <input type="text" class="form-control"
                                                                        name="add_player_sub_position"
                                                                        id="add_player_sub_position" spellcheck="false"
                                                                        placeholder="Eg. 9">
                                            <div class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-lg-6 form-group"><label for="add_player_current_club">Current
                                                Club</label> <input type="text" class="form-control"
                                                                    name="add_player_current_club"
                                                                    id="add_player_current_club" spellcheck="false"
                                                                    placeholder="Eg. Free Agent">
                                            <div class="invalid-feedback"></div>
                                        </div>
                                        <div class="col-lg-6 form-group"><label for="add_player_contract_expiration">Contract
                                                Expiration</label> <input type="text" class="form-control"
                                                                          name="add_player_contract_expiration"
                                                                          id="add_player_contract_expiration"
                                                                          spellcheck="false" placeholder="Eg. None">
                                            <div class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-lg-6 form-group"><label for="add_player_level_played">Level of
                                                League Played</label> <input type="text" class="form-control"
                                                                             name="add_player_level_played"
                                                                             id="add_player_level_played"
                                                                             spellcheck="false"
                                                                             placeholder="Eg. Division 1">
                                            <div class="invalid-feedback"></div>
                                        </div>
                                        <div class="col-lg-6 form-group"><label for="add_player_registration_type">Type
                                                of Player registration</label> <input type="text" class="form-control"
                                                                                      name="add_player_registration_type"
                                                                                      id="add_player_registration_type"
                                                                                      spellcheck="false"
                                                                                      placeholder="Eg. Professional">
                                            <div class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label for="add_player_playing_foot">Playing Foot</label>
                                        <input type="text" class="form-control" name="add_player_playing_foot"
                                               id="add_player_playing_foot" spellcheck="false"
                                               placeholder="Eg. Both Feet but right foot preferred">
                                        <div class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group"><label for="add_player_health">Health Condition</label>
                                        <input type="text" class="form-control" name="add_player_health"
                                               id="add_player_health" spellcheck="false"
                                               placeholder="Eg. No history of Injury">
                                        <div class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group"><label for="add_player_player_profile">Player
                                            Profile</label> <textarea class="form-control"
                                                                      name="add_player_player_profile"
                                                                      id="add_player_player_profile" spellcheck="false"
                                                                      placeholder="Eg. Short description of the player"
                                                                      rows="4"></textarea>
                                        <div class="invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="card-body border-top" id="personal-skills"><h4 class="card-title mb-4">
                                        Personal Skills</h4>
                                    <div class="row">
                                        <div class="col-lg-4 form-group mb-4"><label for="add_player_right_foot">Right
                                                Foot</label> <input type="text" class="form-control"
                                                                    name="add_player_right_foot"
                                                                    id="add_player_right_foot" spellcheck="false"
                                                                    data-toggle="ionRangeSlider" data-min="0"
                                                                    data-max="100" data-grid="true" data-step="1"
                                                                    data-skin="flat">
                                            <div class="invalid-feedback"></div>
                                        </div>
                                        <div class="col-lg-4 form-group mb-4"><label for="add_player_left_foot">Left
                                                Foot</label> <input type="text" class="form-control"
                                                                    name="add_player_left_foot"
                                                                    id="add_player_left_foot" spellcheck="false"
                                                                    data-toggle="ionRangeSlider" data-min="0"
                                                                    data-max="100" data-grid="true" data-step="1"
                                                                    data-skin="flat">
                                            <div class="invalid-feedback"></div>
                                        </div>
                                        <div class="col-lg-4 form-group mb-4"><label
                                                    for="add_player_speed">Speed</label> <input type="text"
                                                                                                class="form-control"
                                                                                                name="add_player_speed"
                                                                                                id="add_player_speed"
                                                                                                spellcheck="false"
                                                                                                data-toggle="ionRangeSlider"
                                                                                                data-min="0"
                                                                                                data-max="100"
                                                                                                data-grid="true"
                                                                                                data-step="1"
                                                                                                data-skin="flat">
                                            <div class="invalid-feedback"></div>
                                        </div>
                                        <div class="col-lg-4 form-group mb-4"><label for="add_player_athleticism">Athleticism</label>
                                            <input type="text" class="form-control" name="add_player_athleticism"
                                                   id="add_player_athleticism" spellcheck="false"
                                                   data-toggle="ionRangeSlider" data-min="0" data-max="100"
                                                   data-grid="true" data-step="1" data-skin="flat">
                                            <div class="invalid-feedback"></div>
                                        </div>
                                        <div class="col-lg-4 form-group mb-4"><label
                                                    for="add_player_tactics">Tactics</label> <input type="text"
                                                                                                    class="form-control"
                                                                                                    name="add_player_tactics"
                                                                                                    id="add_player_tactics"
                                                                                                    spellcheck="false"
                                                                                                    data-toggle="ionRangeSlider"
                                                                                                    data-min="0"
                                                                                                    data-max="100"
                                                                                                    data-grid="true"
                                                                                                    data-step="1"
                                                                                                    data-skin="flat">
                                            <div class="invalid-feedback"></div>
                                        </div>
                                        <div class="col-lg-4 form-group mb-4"><label for="add_player_hard_work">Hard
                                                work</label> <input type="text" class="form-control"
                                                                    name="add_player_hard_work"
                                                                    id="add_player_hard_work" spellcheck="false"
                                                                    data-toggle="ionRangeSlider" data-min="0"
                                                                    data-max="100" data-grid="true" data-step="1"
                                                                    data-skin="flat">
                                            <div class="invalid-feedback"></div>
                                        </div>
                                        <div class="col-lg-4 form-group mb-4"><label
                                                    for="add_player_teamwork">Teamwork</label> <input type="text"
                                                                                                      class="form-control"
                                                                                                      name="add_player_teamwork"
                                                                                                      id="add_player_teamwork"
                                                                                                      spellcheck="false"
                                                                                                      data-toggle="ionRangeSlider"
                                                                                                      data-min="0"
                                                                                                      data-max="100"
                                                                                                      data-grid="true"
                                                                                                      data-step="1"
                                                                                                      data-skin="flat">
                                            <div class="invalid-feedback"></div>
                                        </div>
                                        <div class="col-lg-4 form-group mb-4"><label for="add_player_adaptability">Adaptability</label>
                                            <input type="text" class="form-control" name="add_player_adaptability"
                                                   id="add_player_adaptability" spellcheck="false"
                                                   data-toggle="ionRangeSlider" data-min="0" data-max="100"
                                                   data-grid="true" data-step="1" data-skin="flat">
                                            <div class="invalid-feedback"></div>
                                        </div>
                                        <div class="col-lg-4 form-group mb-4"><label for="add_player_personality">Personality</label>
                                            <input type="text" class="form-control" name="add_player_personality"
                                                   id="add_player_personality" spellcheck="false"
                                                   data-toggle="ionRangeSlider" data-min="0" data-max="100"
                                                   data-grid="true" data-step="1" data-skin="flat">
                                            <div class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body border-top" id="achievements"><h4 class="card-title mb-4">
                                        Achievements</h4>
                                    <div id="achievements-repeater">
                                        <div data-repeater-list="add_player_achievements">
                                            <div class="d-block" data-repeater-item="achievements">
                                                <div class="form-group">
                                                    <div class="input-group input-group-alt"><input type="text"
                                                                                                    class="form-control"
                                                                                                    name="description"
                                                                                                    spellcheck="false"
                                                                                                    placeholder="Eg. Top goals scorer in GPL 2018-2019">
                                                        <span class="input-group-append"> <button type="button"
                                                                                                  class="btn btn-danger"
                                                                                                  data-repeater-delete="achievements"><i
                                                                        class="far fa-trash-alt"></i></button> </span>
                                                    </div>
                                                    <div class="invalid-feedback"></div>
                                                </div>
                                                <hr>
                                            </div>
                                        </div>
                                        <p class="mt-2 mb-0">
                                            <button type="button" class="btn btn-link btn-sm"
                                                    data-repeater-create="achievements"><i
                                                        class="fas fa-plus text-small mr-2"></i> Add New
                                            </button>
                                        </p>
                                    </div>
                                </div>
                                <div class="card-body border-top" id="career-path"><h4 class="card-title mb-4">Career
                                        Path</h4>
                                    <div id="career-path-repeater">
                                        <div class="table-responsive">
                                            <table class="table table-bordered text-nowrap">
                                                <thead class="thead-light">
                                                <tr>
                                                    <th>Delete</th>
                                                    <th>Period</th>
                                                    <th>Club</th>
                                                    <th>Country</th>
                                                    <th>Matches</th>
                                                    <th>Goals</th>
                                                    <th>Club Level</th>
                                                </tr>
                                                </thead>
                                                <tbody data-repeater-list="add_player_career_path">
                                                <tr class="d-table-row" data-repeater-item="career-path">
                                                    <td class="text-center">
                                                        <button type="button" class="btn btn-sm btn-icon btn-danger"
                                                                data-repeater-delete="career-path"><i
                                                                    class="far fa-trash-alt"></i></button>
                                                    </td>
                                                    <td><input type="text" class="form-control form-control-sm"
                                                               name="period" spellcheck="false"
                                                               placeholder="Eg. 2014-2016"></td>
                                                    <td><input type="text" class="form-control form-control-sm"
                                                               name="club" spellcheck="false"
                                                               placeholder="Eg. Barcelona"></td>
                                                    <td><input type="text" class="form-control form-control-sm"
                                                               name="country" spellcheck="false"
                                                               placeholder="Eg. Spain"></td>
                                                    <td><input type="text" class="form-control form-control-sm"
                                                               name="matches" spellcheck="false" placeholder="Eg. 15">
                                                    </td>
                                                    <td><input type="text" class="form-control form-control-sm"
                                                               name="goals" spellcheck="false" placeholder="Eg. 10">
                                                    </td>
                                                    <td><input type="text" class="form-control form-control-sm"
                                                               name="club_level" spellcheck="false"
                                                               placeholder="Eg. Division 1"></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <small class="form-text text-red"><strong>Note!</strong> Rows without <em
                                                    style="text-decoration: underline;">Period</em> will be ignored
                                        </small>
                                        <p class="mt-2 mb-0">
                                            <button type="button" class="btn btn-link btn-sm"
                                                    data-repeater-create="career-path"><i
                                                        class="fas fa-plus text-small mr-2"></i> Add New
                                            </button>
                                        </p>
                                    </div>
                                </div>
                                <div class="card-body border-top" id="contact-details"><h4 class="card-title mb-4">
                                        Contact Details</h4>
                                    <div class="form-group"><label for="add_player_phone">Phone</label> <input
                                                type="text" class="form-control" name="add_player_phone"
                                                id="add_player_phone" spellcheck="false" data-toggle="intltelinput"
                                                data-auto-placeholder="aggressive" data-separate-dial-code="true"
                                                data-preferred-countries='["<?= strtolower($__settings['country_details']['initials']); ?>"]'>
                                        <div class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group"><label for="add_player_other_phone">Other Phone No.</label>
                                        <input type="text" class="form-control" name="add_player_other_phone"
                                               id="add_player_other_phone" spellcheck="false" data-toggle="intltelinput"
                                               data-auto-placeholder="aggressive" data-separate-dial-code="true"
                                               data-preferred-countries='["<?= strtolower($__settings['country_details']['initials']); ?>"]'>
                                        <div class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group"><label for="add_player_email">Email</label> <input
                                                type="text" class="form-control" name="add_player_email"
                                                id="add_player_email" spellcheck="false">
                                        <div class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group"><label for="add_player_address">Address</label> <textarea
                                                class="form-control" name="add_player_address" id="add_player_address"
                                                spellcheck="false"
                                                placeholder="Eg. House No. 123, Awoshie, Near the market"
                                                rows="4"></textarea>
                                        <div class="invalid-feedback"></div>
                                    </div>
                                </div>
                                <div class="card-body border-top" id="agency-details"><h4 class="card-title mb-4">Agency
                                        Details</h4>
                                    <div class="form-group"><label for="add_player_player_number">Player ID <span
                                                    class="text-danger">*</span></label> <input type="text"
                                                                                                class="form-control"
                                                                                                name="add_player_player_number"
                                                                                                id="add_player_player_number"
                                                                                                spellcheck="false"
                                                                                                placeholder="Eg. <?= $config['INITIAL'] . date('my') . '001'; ?>">
                                        <div class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group"><label for="add_player_contract">Upload Contract</label>
                                        <div class="custom-file"><input type="file" class="custom-file-input"
                                                                        name="add_player_contract"
                                                                        id="add_player_contract" accept=".pdf"> <label
                                                    class="custom-file-label" for="add_player_contract">Choose a PDF
                                                file</label></div>
                                        <div class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-lg-6 form-group"><label for="add_player_date_joined">Date
                                                Joined</label>
                                            <div class="input-group input-group-alt" data-toggle="flatpickr"
                                                 data-wrap="true" data-date-format="d-m-Y"><input type="text"
                                                                                                  class="form-control"
                                                                                                  name="add_player_date_joined"
                                                                                                  id="add_player_date_joined"
                                                                                                  spellcheck="false"
                                                                                                  placeholder="DD-MM-YYYY"
                                                                                                  data-input="">
                                                <div class="input-group-append">
                                                    <button type="button" class="btn btn-secondary" data-toggle=""><i
                                                                class="far fa-calendar"></i></button>
                                                    <button type="button" class="btn btn-secondary" data-clear=""><i
                                                                class="fa fa-times"></i></button>
                                                </div>
                                            </div>
                                            <div class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label for="add_player_status">Status <span
                                                    class="text-danger">*</span></label> <select class="form-control"
                                                                                                 name="add_player_status"
                                                                                                 id="add_player_status"
                                                                                                 data-toggle="select2">
                                            <option value=""></option> <?php foreach ($player_statuses as $status => $status_dets): ?>
                                                <option value="<?= $status; ?>"><?= strtoupper($status_dets['name']); ?></option> <?php endforeach; ?>
                                        </select>
                                        <div class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-lg-6 form-group"><label for="add_player_status_date">Status
                                                Date</label>
                                            <div class="input-group input-group-alt" data-toggle="flatpickr"
                                                 data-wrap="true" data-date-format="d-m-Y"><input type="text"
                                                                                                  class="form-control"
                                                                                                  name="add_player_status_date"
                                                                                                  id="add_player_status_date"
                                                                                                  spellcheck="false"
                                                                                                  placeholder="DD-MM-YYYY"
                                                                                                  data-input="">
                                                <div class="input-group-append">
                                                    <button type="button" class="btn btn-secondary" data-toggle=""><i
                                                                class="far fa-calendar"></i></button>
                                                    <button type="button" class="btn btn-secondary" data-clear=""><i
                                                                class="fa fa-times"></i></button>
                                                </div>
                                            </div>
                                            <small class="form-text text-muted">Eg. Date sold, suspended, etc</small>
                                            <div class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="form-group"><label for="add_player_status_comment">Status
                                            Comment</label> <textarea class="form-control"
                                                                      name="add_player_status_comment"
                                                                      id="add_player_status_comment" spellcheck="false"
                                                                      placeholder="Eg. Reason why the player was suspended"
                                                                      rows="4"></textarea></div>
                                </div>
                                <div class="card-body border-top" id="save-or-reset">
                                    <button type="submit" class="btn btn-primary px-4">Save</button>
                                    <button type="reset" class="btn btn-secondary px-3">Reset</button>
                                </div>
                            </form>
                        </div>
                    </div> <?php include DOCUMENT_ROOT . 'includes/partials/footer.php'; ?> </div>
                <div class="page-sidebar page-sidebar-fixed">
                    <header class="sidebar-header d-xl-none">
                        <ol class="breadcrumb mb-0">
                            <li class="breadcrumb-item"><a class="prevent-default" href="#"
                                                           onclick="Looper.toggleSidebar()"><i
                                            class="breadcrumb-icon fa fa-angle-left mr-2"></i>Back</a></li>
                        </ol>
                    </header>
                    <nav id="nav-content" class="nav flex-column mt-4"><h5 class="px-3 mt-0">Quick Scroll Links</h5> <a
                                href="#bio-data" class="nav-link smooth-scroll">Bio Data</a> <a href="#personal-skills"
                                                                                                class="nav-link smooth-scroll">Personal
                            Skills</a> <a href="#achievements" class="nav-link smooth-scroll">Achievements</a> <a
                                href="#career-path" class="nav-link smooth-scroll">Career Path</a> <a
                                href="#contact-details" class="nav-link smooth-scroll">Contact Details</a> <a
                                href="#agency-details" class="nav-link smooth-scroll">Agency Details</a> <a
                                href="#save-or-reset" class="nav-link smooth-scroll">Save / Reset</a></nav>
                </div>
            </div>
        </div>
    </main>
</div> <?php include DOCUMENT_ROOT . 'includes/partials/script.php'; ?> </body>
</html>
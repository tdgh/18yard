<!DOCTYPE html>
<html>
<head> <?php include DOCUMENT_ROOT . '/includes/partials/head.php'; ?>
    <style type="text/css"> .table th, .table td {
            vertical-align: middle !important;
        }

        .table td:not(.dataTables_empty) {
            padding: .55rem;
        }

        .card-shadow {
            box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
        }

        /* flatpickr */
        .modal .flatpickr-wrapper {
            display: block;
        }

        /*.modal .flatpickr-calendar{width: 100%; }*/
        .form-control.is-invalid, .form-control.is-valid, .was-validated .form-control:invalid, .was-validated .form-control:valid {
            z-index: 3;
        }

        .select2-player-thumb {
            width: 40px;
            height: 40px;
        } </style>
</head>
<body>
<div class="app"> <?php // include DOCUMENT_ROOT.'includes/partials/preloader.php'; ?> <?php include DOCUMENT_ROOT . 'includes/partials/header.php'; ?> <?php include DOCUMENT_ROOT . 'includes/partials/sidebar.php'; ?>
    <main class="app-main">
        <div class="wrapper">
            <div class="page">
                <div class="page-inner">
                    <div class="page-title-bar"> <?php include DOCUMENT_ROOT . 'includes/partials/breadcrumb.php'; ?>
                        <h1 class="page-title"><?= $page_dependencies->page_title; ?></h1></div>
                    <div class="page-section">
                        <div class="card card-fluid border-top border-primary">
                            <div class="card-header d-flex justify-content-between">
                                <div>
                                    <button type="button" class="btn btn-success btn-gradient px-3 rounded-0"
                                            data-toggle="modal" data-target="#addIncomeOrExpensesModal"><i
                                                class="fa fa-plus"></i></button>
                                    <button type="button" class="btn btn-warning btn-gradient px-3 rounded-0"
                                            id="reload-table"><i class="fa fa-spinner"></i></button>
                                    <button type="button" class="btn btn-dark btn-gradient px-3 rounded-0"
                                            id="export-modal-trigger"><i class="fa fa-download"></i></button>
                                </div>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-secondary scroll-btn" data-scroll="-=500px"
                                            data-ctarget="#income-and-expenses-table-wrapper"><i
                                                class="fa fa-angle-left"></i></button>
                                    <button type="button" class="btn btn-secondary scroll-btn" data-scroll="+=500px"
                                            data-ctarget="#income-and-expenses-table-wrapper"><i
                                                class="fa fa-angle-right"></i></button>
                                </div>
                            </div>
                            <div class="card-body border-bottom">
                                <div class="row no-gutters">
                                    <div class="col">
                                        <div class="p-3 text-center border border-right-0"><p class="mb-0 text-muted">
                                                Total Income</p>
                                            <h3>
                                                <small>GH&#8373;</small>
                                                <span id="total-income">0.00</span></h3>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="p-3 text-center border border-right-0"><p class="mb-0 text-muted">
                                                Total Expenses</p>
                                            <h3>
                                                <small>GH&#8373;</small>
                                                <span id="total-expenses">0.00</span></h3>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="p-3 text-center border"><p class="mb-0 text-muted">Gross Profit</p>
                                            <h3>
                                                <small>GH&#8373;</small>
                                                <span id="gross-profit">0.00</span></h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered table-hover border-bottom m-0 text-nowrap"
                                       id="income-and-expenses-table">
                                    <thead class="thead-light">
                                    <tr>
                                        <th style="width: 70px;">Action</th>
                                        <th>Transaction Date</th>
                                        <th>Description</th>
                                        <th>Player</th>
                                        <th style="text-align: left!important;">Income</th>
                                        <th style="text-align: left!important;">Expenses</th>
                                        <th>Comment</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div> <?php include dirname(__DIR__) . '/includes/add_income_or_expenses.inc.php'; ?> <?php include DOCUMENT_ROOT . 'includes/partials/footer.php'; ?>
                </div>
            </div>
        </div>
    </main>
</div> <?php include DOCUMENT_ROOT . 'includes/partials/script.php'; ?> </body>
</html>
<!DOCTYPE html>
<html>
<head> <?php include DOCUMENT_ROOT . '/includes/partials/head.php'; ?>
    <style type="text/css"> .table th, .table td {
            vertical-align: middle !important;
        }

        .table td:not(.dataTables_empty) {
            padding: .55rem;
        }

        .player-thumb {
            width: 2rem;
            height: 2rem;
        }

        .table-responsive.hide-scroll::-webkit-scrollbar {
            display: none;
        }

        .table-responsive.hide-scroll {
            -ms-overflow-style: none;
        }

        td .badge {
            width: 70px;
        }

        /*.btn[data-disabled] {cursor: not-allowed; background: linear-gradient(180deg,#f6f7f9,#f6f7f9); border-color: #d7dce5; opacity: .65; box-shadow: none; }*/ </style>
</head>
<body>
<div class="app"> <?php // include DOCUMENT_ROOT.'includes/partials/preloader.php'; ?> <?php include DOCUMENT_ROOT . 'includes/partials/header.php'; ?> <?php include DOCUMENT_ROOT . 'includes/partials/sidebar.php'; ?>
    <main class="app-main">
        <div class="wrapper">
            <div class="page">
                <div class="page-inner">
                    <div class="page-title-bar">
                        <div class="d-sm-flex justify-content-sm-between align-items-sm-center">
                            <div> <?php include DOCUMENT_ROOT . 'includes/partials/breadcrumb.php'; ?> <h1
                                        class="page-title"><?= $page_dependencies->page_title; ?></h1></div>
                            <a href="<?= DOMAIN; ?>add_player" class="btn btn-secondary px-3 rounded-0"><i
                                        class="fa fa-plus mr-2"></i> Add Player</a></div>
                    </div>
                    <div class="page-section">
                        <div class="card card-fluid border-top border-primary">
                            <div class="card-header d-flex justify-content-between">
                                <div>
                                    <button type="button" class="btn btn-warning btn-gradient px-3 rounded-0"
                                            id="reload-table"><i class="fa fa-spinner"></i></button>
                                    <button type="button" class="btn bg-blue text-white btn-gradient px-3 rounded-0"
                                            data-toggle="modal" data-target="#columns-modal"><i class="fa fa-list"></i>
                                    </button>
                                    <button type="button" class="btn btn-dark btn-gradient px-3 rounded-0"
                                            id="export-modal-trigger"><i class="fa fa-download"></i></button>
                                </div>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-secondary scroll-btn" data-scroll="-=500px"
                                            data-ctarget="#players-table-wrapper"><i class="fa fa-angle-left"></i>
                                    </button>
                                    <button type="button" class="btn btn-secondary scroll-btn" data-scroll="+=500px"
                                            data-ctarget="#players-table-wrapper"><i class="fa fa-angle-right"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered table-hover border-bottom m-0 text-nowrap"
                                       id="players-table">
                                    <thead class="thead-light">
                                    <tr> <?php foreach ($db_table_cols as $c_id => $c_name): ?>
                                            <th><?= $c_name; ?></th> <?php endforeach; ?> </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div> <?php include dirname(__DIR__) . '/includes/toggle_columns_modal.inc.php'; ?> <?php include DOCUMENT_ROOT . 'includes/partials/footer.php'; ?>
                </div>
            </div>
        </div>
    </main>
</div>
<script type="text/javascript"> var $dt_columns = <?= json_encode($dt_columns); ?>;
    var $db_columns_default_hidden = "<?= implode(',', $db_table_cols_default_hidden); ?>";
    var $player_statuses = <?= json_encode($player_statuses); ?>; </script> <?php include DOCUMENT_ROOT . 'includes/partials/script.php'; ?>
</body>
</html>
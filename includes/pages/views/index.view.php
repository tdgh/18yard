<!DOCTYPE html>
<html>
<head> <?php include DOCUMENT_ROOT . '/includes/partials/head.php'; ?> </head>
<body>
<div class="app"> <?php // include DOCUMENT_ROOT.'includes/partials/preloader.php'; ?> <?php include DOCUMENT_ROOT . 'includes/partials/header.php'; ?> <?php include DOCUMENT_ROOT . 'includes/partials/sidebar.php'; ?>
    <main class="app-main">
        <div class="wrapper">
            <div class="page">
                <div class="page-inner">
                    <header class="page-title-bar">
                        <div class="d-sm-flex justify-content-sm-between align-items-sm-center">
                            <div> <?php include DOCUMENT_ROOT . 'includes/partials/breadcrumb.php'; ?> <h1
                                        class="page-title"><?= $page_dependencies->page_title; ?></h1></div>
                            <button type="button" class="btn btn-secondary px-3 rounded-0" data-toggle="aside"><i
                                        class="fa fa-list mr-2"></i> Show Sidebar
                            </button>
                        </div>
                    </header>
                    <div class="page-section">
                        <div class="section-block">
                            <div class="metric-row metric-flush">
                                <div class="col">
                                    <div class="metric metric-bordered align-items-center"><h2 class="metric-label">
                                            Players</h2>
                                        <p class="metric-value h3"><sub><i class="fa fa-users"></i></sub> <span
                                                    class="value"><?= $total_players; ?></span></p></div>
                                </div>
                                <div class="col">
                                    <div class="metric metric-bordered align-items-center"><h2
                                                class="metric-label"><?= $data[0]['name']; ?></h2>
                                        <p class="metric-value h3"><sub><i class="fa fa-user-check"></i></sub> <span
                                                    class="value"><?= $data[0]['y']; ?></span></p></div>
                                </div>
                                <div class="col">
                                    <div class="metric metric-bordered align-items-center"><h2
                                                class="metric-label"><?= $data[1]['name']; ?></h2>
                                        <p class="metric-value h3"><sub><i class="fa fa-user-times"></i></sub> <span
                                                    class="value"><?= $data[1]['y']; ?></span></p></div>
                                </div>
                                <div class="col">
                                    <div class="metric metric-bordered align-items-center"><h2
                                                class="metric-label"><?= $data[2]['name']; ?></h2>
                                        <p class="metric-value h3"><sub><i class="fa fa-shopping-cart"></i></sub> <span
                                                    class="value"><?= $data[2]['y']; ?></span></p></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <div class="card card-fluid h-100">
                                    <div class="card-body"><h3 class="card-title mb-1">Players</h3>
                                        <p class="text-muted mb-0">Grouped by status</p>
                                        <div class="text-center pt-3">
                                            <div id="players-chart" style="height: 250px;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="card card-fluid h-100">
                                    <div class="p-3"><h3 class="card-title d-flex justify-content-between mb-1"><span>Players</span>
                                            <a href="<?= DOMAIN; ?>players" class="small text-gray"><i
                                                        class="fa fa-fw fa-circle text-purple small font-weight-bold"></i>
                                                View All</a></h3>
                                        <p class="text-muted mb-0">Players average personal skills</p></div>
                                    <div class="list-group list-group-flush"> <?php $bgs = ['purple', 'danger', 'teal', 'yellow', 'indigo'];
                                        $i = 0;
                                        foreach ($five_payers as $player): ?>
                                            <div class="list-group-item">
                                                <div class="list-group-item-figure"><a
                                                            href="<?= DOMAIN; ?>players/player?h=<?= md5($player['player_id']); ?>&__=<?= $player['player_id']; ?>&_token=<?= $_SESSION[CSRF_TOKEN_KEY]; ?>"
                                                            class="user-avatar" data-toggle="tooltip"
                                                            title="<?= htmlspecialchars($player['name']); ?>"><img
                                                                src="<?= photo($player['photo'], $player['sex'], 'players'); ?>"
                                                                alt=""></a></div>
                                                <div class="list-group-item-body">
                                                    <div class="progress progress-animated rounded-0">
                                                        <div class="progress-bar bg-<?= $bgs[$i]; ?>" role="progressbar"
                                                             aria-valuenow="<?= $player['average_skills']; ?>"
                                                             aria-valuemin="0" aria-valuemax="100"
                                                             style="width: <?= round($player['average_skills'], 2); ?>%"> <?= round($player['average_skills'], 2); ?>
                                                            %
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> <?php $i++; endforeach; ?> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <?php include DOCUMENT_ROOT . 'includes/partials/footer.php'; ?> </main>
</div> <?php include DOCUMENT_ROOT . 'includes/partials/script.php'; ?>
<script type="text/javascript"> $(document).ready(function () {
        Highcharts.chart('players-chart', {
            credits: false,
            chart: {type: 'column', style: {fontFamily: 'sanserif'}},
            title: {text: null},
            xAxis: {type: 'category'},
            yAxis: {title: {text: null}},
            legend: {enabled: false},
            plotOptions: {series: {borderWidth: 0, dataLabels: {enabled: true}}},
            tooltip: {
                headerFormat: '',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b>'
            },
            series: [{name: 'Brand', colorByPoint: true, data: <?= json_encode($data); ?> }]
        });
    }); </script>
</body>
</html>
<!DOCTYPE html>
<html>
<head> <?php include DOCUMENT_ROOT . '/includes/partials/head.php'; ?>
    <style type="text/css"> .table th, .table td {
            vertical-align: middle !important;
        }

        .card-shadow {
            box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
        }

        .iti {
            width: 100%;
        }

        .iti__selected-flag {
            background-color: #fff !important;
            outline: 0 !important;
            font-size: 0.875rem;
            padding-right: 0;
        }

        .iti--separate-dial-code .iti__arrow {
            display: none;
        }

        /*div.dataTables_wrapper div.dataTables_info{margin-bottom: .5rem; }*/ /* flatpickr */
        .modal .flatpickr-wrapper {
            display: block;
        }

        /*.modal .flatpickr-calendar{width: 100%; }*/
        .form-control.is-invalid, .form-control.is-valid, .was-validated .form-control:invalid, .was-validated .form-control:valid {
            z-index: 3;
        }

        .popup-shadow {
            box-shadow: 0 0 10px rgba(0, 0, 0, .4);
        }

        .player-thumb {
            width: 2rem;
            height: 2rem;
        }

        .select2-player-thumb {
            width: 40px;
            height: 40px;
        }

        .btn.disabled, .btn:disabled {
            cursor: not-allowed;
        } </style>
</head>
<body>
<div class="app"> <?php // include DOCUMENT_ROOT.'includes/partials/preloader.php'; ?> <?php include DOCUMENT_ROOT . 'includes/partials/header.php'; ?> <?php include DOCUMENT_ROOT . 'includes/partials/sidebar.php'; ?>
    <main class="app-main">
        <div class="wrapper">
            <div class="page has-sidebar has-sidebar-fluid has-sidebar-expand-xl">
                <div class="page-inner page-inner-fill position-relative">
                    <div class="page-navs bg-light shadow-sm">
                        <div class="input-group has-clearable">
                            <button type="button" class="close" aria-label="Close"><span aria-hidden="true"><i
                                            class="fa fa-times-circle"></i></span></button>
                            <label class="input-group-prepend" for="search-class"><span class="input-group-text"><span
                                            class="oi oi-magnifying-glass"></span></span></label> <input type="text"
                                                                                                         class="form-control"
                                                                                                         id="search-class"
                                                                                                         data-filter=".board .list-group-item"
                                                                                                         placeholder="Search Agent">
                        </div>
                    </div>
                    <button type="button" class="btn btn-primary btn-floated position-absolute" data-toggle="modal"
                            data-target="#addNewAgentModal" title="Add new client"><i class="fa fa-plus"></i></button>
                    <div class="board p-0 perfect-scrollbar"> <?php if (count($agents) > 0): ?>
                            <div class="list-group list-group-divider border-top"
                                 id="agents"> <?php foreach ($agents as $agent): ?>
                                    <div class="list-group-item cursor-pointer" data-id="<?= $agent['id']; ?>"
                                         data-name="<?= htmlspecialchars($agent['name']); ?>">
                                        <div class="list-group-item-figure">
                                            <div class="tile tile-circle bg-<?= $agent['background']; ?> text-uppercase"><?= $agent['name'][0]; ?></div>
                                        </div>
                                        <div class="list-group-item-body"><h4
                                                    class="list-group-item-title text-truncate"><?= htmlspecialchars($agent['name']); ?></h4>
                                            <p class="list-group-item-text text-muted"><?= htmlspecialchars(!empty($agent['phone']) ? $agent['phone'] : 'N/A'); ?></p>
                                        </div>
                                    </div> <?php endforeach; ?> </div> <?php else: ?> <h4
                                class="px-4 mt-3 font-weight-light text-center">No agent available</h4> <?php endif; ?>
                    </div>
                </div>
                <div class="page-sidebar bg-light">
                    <div class="sidebar-header d-xl-none">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active"><a href="javascript:void(0);"
                                                                      onclick="Looper.toggleSidebar()"><i
                                                class="breadcrumb-icon fa fa-angle-left mr-2"></i>Back</a></li>
                            </ol>
                        </nav>
                    </div>
                    <div class="sidebar-section sidebar-section-fill position-relative">
                        <div style="position: absolute;top: 0;left: 0;width: 100%;height: 100%;z-index: 1;background: rgba(255, 255, 255, 0.5);cursor: not-allowed;"
                             id="no-agent-selected"></div>
                        <h1 class="page-title" data-agent-details="name">No Agent Selected</h1>
                        <p><span data-toggle="tooltip" title="Phone No."><i class="fa fa-phone-square text-warning"></i> <span
                                        class="text-muted mr-2" data-agent-details="phone">N/A</span></span> <span
                                    data-toggle="tooltip" title="Passport No."><i
                                        class="fa fa-passport text-warning"></i> <span class="text-muted"
                                                                                       data-agent-details="passport_no">N/A</span></span>
                        </p>
                        <div class="card mt-4">
                            <div class="card-header py-2 bg-primary text-white rounded-0">
                                <div class="d-flex justify-content-between align-items-center"><h3
                                            class="card-title mb-0"><i class="fa fa-users mr-2"></i> Players</h3>
                                    <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal"
                                            id="tie-player-to-agent">Add New
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table text-nowrap" id="agents-table">
                                    <thead>
                                    <tr>
                                        <th style="width: 70px;">Action</th>
                                        <th style="width: 50px;">Photo</th>
                                        <th>Name</th>
                                        <th>Passport No.</th>
                                        <th>Entry Date</th>
                                        <th>Expiry Date</th>
                                        <th>Duration</th>
                                        <th>Status</th>
                                        <th>Status Date</th>
                                        <th>Status Comment</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div> <?php include dirname(__DIR__) . '/includes/add_agent_modal.inc.php'; ?> <?php include dirname(__DIR__) . '/includes/tie_player_to_agent.inc.php'; ?>
            </div>
        </div>
    </main>
</div>
<script type="text/javascript"> var $agent_player_statuses = <?= json_encode($agent_player_statuses); ?>; </script> <?php include DOCUMENT_ROOT . 'includes/partials/script.php'; ?>
</body>
</html>
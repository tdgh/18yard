<?php

require dirname(__DIR__, 2).'/includes/partials/index.php';
require dirname(__DIR__, 2).'/includes/classes/evaluation.class.php';

/**
 * @ page dependencies
*/
$page_dependencies = new \stdClass;

$page_dependencies->page_title = 'Evaluation Report';
$page_dependencies->breadcrumb = [
    'Evaluation Report' => ''
];
$page_dependencies->plugins = [
    'dropify',
    'flatpickr',
    'intl-tel-input',
    'ion.rangeSlider',
    'repeater',
    'select2',
    'sweetalert2',
    'toast'
];
$page_dependencies->js = ['validator.js', 'evaluation_report.js'];
// $page_dependencies->hide_sidebar = false;
// end of page dependencies

$countries_arr = query('countries', ['nationality'], [['status','1']], 'nationality');


$eval = new evaluation(Null);
$allplayers = $eval->get_all_players();

require __DIR__.'/views/evaluation_report.view.php';
<?php

$__not_ajax_but_use_ajax_config = true;
require dirname(__DIR__, 2).'/includes/partials/index_ajax.php';

if( isset($_GET['__'], $_GET['h']) && 
    md5($_GET['__']) === $_GET['h'] && 
    \CSRF_TOKEN::check('request_method', 'get') 
){
	$player_id   = $_GET['__'];
	$player_dets = query('players', [], [['player_id', $_GET['__']]])[0] ?? [];

	if( count($player_dets) > 0 ){
		$player_dets['name'] = $player_dets['first_name'].' '.$player_dets['last_name'];
	    $player_dets['photo'] = photo($player_dets['photo'], $player_dets['sex'], 'players', 'path');
	    $player_dets['date_of_birth'] = c_date('d-m-Y', $player_dets['date_of_birth']);

	    // achievements and career path
	    $achievements = query('players_achievements', [], [['player_id', $player_id]]);
	    $career_path  = query('players_career_path', [], [['player_id', $player_id]]);
	}else{
		echo '<script>window.parent.notify("Couldn\'t generate CV. Please refresh the page and try again.", "error"); window.parent.jQuery.fancybox.getInstance().close();</script>';
	    die();
	}
}else{
	echo '<script>window.parent.notify("Couldn\'t generate CV. Please refresh the page and try again.", "error"); window.parent.jQuery.fancybox.getInstance().close();</script>';
	die();
}

//
require DOCUMENT_ROOT.'vendor/autoload.php';

class PDF extends FPDF{
	public $widths;
	public $fontWeight;

    // multicell table
    public function SetWidths($w){
		// Set the array of column widths
		$this->widths=$w;
	}

	public function SetFontWeight($a){
		// Set the array of column alignments
		$this->fontWeight=$a;
	}

	public function Row($data, $border=0, $fill=false){
		//Draw the cells of the row
		$h = 0;
		for($i=0;$i<count($data);$i++){
			$w=$this->widths[$i];
			$fw=$this->fontWeight[$i] ?? '';

			$this->SetFont('Arial', $fw, 10);

			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();

			//Print the text and get height of column
			$h2 = $this->MultiCell($w, 7, $data[$i], $border, 'L', $fill);
			$h = $h2 > $h ? $h2 : $h; // get max height in row fo Ln

			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}

		$this->Ln($h);
	}

	public function Header(){
		$this->Image(DOCUMENT_ROOT.'assets/images/logo.png', 10, 10, 25, 15);
		$this->SetFillColor(32, 176, 83);
		$this->Rect(40, 12, 160, 10, 'F');
		$this->Ln(20);

	}

	public function Footer(){
		global $player_dets;
		$logo = DOCUMENT_ROOT.'assets/images/logo.png';
		
		$this->SetY(-15);
		$this->SetFont('Arial', 'I', 9);
		$this->SetTextColor(32, 176, 83);

		$this->Image($logo, $this->GetX(), $this->GetY()-2, 23, 12);
		$this->SetXY(40, $this->GetY());
		$h = $this->MultiCell(157, 4, 'Address: P.O.BOX CO 1485, TEMA - ACCRA. House Number 207, Awoshie- Anyaa. Phone: +233 261-961-164 / +233 542-365-481. Email: 18yardboxltd@gmail.com // Website: https://www.18yardboxltd.com', 0, 'L');

		$this->Rect(37, 280, 163, $h+4);
		$this->Ln(15);
	}
}


$title = 'Player Biodata CV For - '.preg_replace('/\W/', '', $player_dets['first_name']);

$pdf = new PDF();
$pdf->SetTitle($title);
$pdf->SetAuthor($config['COMPANY_NAME']);

//
$pdf->AddPage();

$pdf->SetFont('Times', 'BIU', 16);
$pdf->MultiCell(0, 7, strtoupper($title), 0, 'C');
$pdf->Ln(5);


$GetY = $pdf->GetY();
$pdf->SetLineWidth(0.2);
$pdf->Image($player_dets['photo'],$pdf->GetX()+1, $GetY+1, 33, 33);
$pdf->Rect($pdf->GetX(), $pdf->GetY(), 35, 35);

// company details
$pdf->SetFont('Arial', '', 10);

$pdf->SetXY(80, $GetY);
$pdf->Cell(45, 7, 'INTERMEDIARY', 1, 0, 'R');
$pdf->Cell(75, 7, '18yardbox Company Limited', 1, 1);

$pdf->SetXY(80, $pdf->GetY());
$pdf->Cell(45, 7, 'CONTACT PERSON', 1, 0, 'R');
$pdf->Cell(75, 7, 'Osbert Senam Agbaga (Manager)', 1, 1);

$pdf->SetXY(80, $pdf->GetY());
$pdf->Cell(45, 7, 'EMAIL', 1, 0, 'R');
$pdf->Cell(75, 7, '18yardboxltd@gmail.com', 1, 1);

$pdf->SetXY(80, $pdf->GetY());
$pdf->Cell(45, 7, 'TELEPHONE / MOBILE', 1, 0, 'R');
$pdf->Cell(75, 7, '+233 261961164 / 233 542365481', 1, 1);

$pdf->SetXY(80, $pdf->GetY());
$pdf->Cell(45, 7, 'PASSPORT NUMBER', 1, 0, 'R');
$pdf->Cell(75, 7, $player_dets['passport_no'], 1, 1);
$pdf->Ln(5);

// dio data
$pdf->SetFont('Arial', '', 11);

$pdf->Cell(0, 9, 'BIO DATA', 0, 1);

$pdf->SetFillColor(210, 210, 210);
$pdf->SetWidths([40, 150]);
$pdf->SetFontWeight(['B', '']);

$pdf->Row(['First Name', $player_dets['first_name']], 1, true);
$pdf->Row(['Last Name', $player_dets['last_name']], 1);
$pdf->Row(['Date Of Birth', $player_dets['date_of_birth']], 1, true);
$pdf->Row(['Sex', $player_dets['sex']], 1);
$pdf->Row(['Nationality', $player_dets['nationality']], 1, true);
$pdf->Row(['Marital Status', $player_dets['marital_status']], 1);
$pdf->Row(['Weight', $player_dets['weight']], 1, true);
$pdf->Row(['Height', $player_dets['height']], 1);
$pdf->Row(['Passport No.', $player_dets['passport_no']], 1, true);
$pdf->Row(['Preferred Position', $player_dets['preferred_position']], 1);
$pdf->Row(['Sub Position', $player_dets['sub_position']], 1, true);
$pdf->Row(['Current Club', $player_dets['current_club']], 1);
$pdf->Row(['Contract Expiration', $player_dets['contract_expiration']], 1, true);
$pdf->Row(['Level Played', $player_dets['level_played']], 1);
$pdf->Row(['Registration Type', $player_dets['registration_type']], 1, true);
$pdf->Row(['Playing Foot', $player_dets['playing_foot']], 1);
$pdf->Row(['Health', $player_dets['health']], 1, true);
$pdf->Row(['Player Profile', $player_dets['player_profile']], 1);
$pdf->Ln(10);

// personal skills
$pdf->AddPage();
$pdf->Cell(0, 9, 'PERSONAL SKILLS', 0, 1);

$pdf->SetWidths([24, 40, 23, 40, 23, 40]);
$pdf->SetFontWeight(['B', '', 'B', '', 'B', '']);
$pdf->Row([
	'Right Foot', $player_dets['right_foot'].'%',
	'Left Foot', $player_dets['left_foot'].'%',
	'Speed', $player_dets['speed'].'%'
], 1, true);

$pdf->Row([
	'Athleticism', $player_dets['athleticism'].'%',
	'Tactics', $player_dets['tactics'].'%',
	'Hard Work', $player_dets['hard_work'].'%'
], 1, true);

$pdf->Row([
	'Teamwork', $player_dets['teamwork'].'%',
	'Adaptability', $player_dets['adaptability'].'%',
	'Personality', $player_dets['personality'].'%'
], 1, true);

$pdf->Ln(10);

// achievements
$pdf->Cell(0, 9, 'ACHIEVEMENTS', 'B', 1);

for($i=0; $i<count($achievements); $i++){
	$pdf->MultiCell(0, 7, strval($i+1).'. '.$achievements[$i]['description']);
}

$pdf->Ln(10);

// career path
$pdf->Cell(0, 9, 'CAREER PATH', 0, 1);

$pdf->SetWidths([25, 60, 30, 25, 25, 25]);
$pdf->SetFontWeight(['B', 'B', 'B', 'B', 'B', 'B']);
$pdf->Row([
	'Period',
	'Club',
	'Country',
	'Matches',
	'Goals',
	'Club Level'
], 'T', true);

$pdf->SetFontWeight(['B', '', '', '', '', '']);
for($i=0; $i<count($career_path); $i++){
	$pdf->Row([
		$career_path[$i]['period'],
		$career_path[$i]['club'],
		$career_path[$i]['country'],
		$career_path[$i]['matches'],
		$career_path[$i]['goals'],
		$career_path[$i]['club_level'],
	]);
}

$pdf->Output('I', $title.'.pdf');
die();
<?php

if( $__require_config ?? true ) require __DIR__.'/config.php';

function connect($config){
	try{
		$conn = new PDO(
			'mysql:host='.$config['DB_HOST'].';dbname='.$config['DB_NAME'],
			$config['DB_USERNAME'], 
			$config['DB_PASSWORD']
		);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $conn;
	}catch( PDOException $e ){
		return false;
	}
}

// create connection to database
$connect = connect($config);

/* ************ ================ *********** */
function temporary_login(){
	return 	isset($_SESSION[USER_DETS_KEY]) && 
			isset($_SESSION[LOG_UNAME_KEY]) && 
			isset($_SESSION[LOG_PWORD_KEY]) && 
			isset($_COOKIE[LOG_LOGIN_KEY]);
}

function login_error($params=''){
	global $xhr_response, $__ajax, $__not_ajax_but_use_ajax_config, $__set_http_response_code;

	if( isset($__ajax) ){
		if( !isset($__not_ajax_but_use_ajax_config) ){
			$xhr_response['success'] = '__refresh__';
			echo json_encode($xhr_response);
			die();
		}else if( isset($__set_http_response_code) ){
			http_response_code($__set_http_response_code);
		}
	}else{
		header('Location: '.DOMAIN.'logout'.$params);
	}

	die();
}

function append_zeros($limit, $id){
	$append_zeros = '';
	for ($i=0; $i < intval($limit)-(strlen(strval($id))); $i++) { 
		$append_zeros .= '0';
	}
	return $append_zeros.strval($id);
}

function photo($photo, $sex, $dir, $type='url', $name=''){
	$dir = 'assets/images/'.$dir.(!empty($dir) ? '/' : '');

	if( !empty($photo) && file_exists(DOCUMENT_ROOT.$dir.$photo) ){
		$result = $photo;
	}else if( !empty($name) && preg_match('/[A-Za-z]/', $name[0]) ){
		$dir = 'assets/images/';
		$result = 'avatars/'.strtolower($name[0]).'.png';
	}else if( strtolower($sex) == 'female' ){
		$result = 'female.png';
	}else if( strtolower($sex) == 'male' ){
		$result = 'male.png';
	}else{
		$dir = 'assets/images/';
		$result = 'no_image.jpg';
	}

	return ($type == 'path' ? DOCUMENT_ROOT : DOMAIN).$dir.$result;
}

function query($table, $columns=[], $conditionals=[], $order=false, $order_dir='ASC'){
	global $connect;
	$table = 'unisoft_'.$table;

	$and_cond = '';
	$execute  = [];
	$columns  = $columns=='COUNT(*)' || count($columns)>0 ? implode(',',(array)$columns) :'*';
	$order_by = $order ? 'ORDER BY '.$order.' '.$order_dir : '';

	foreach($conditionals as $a){
		$a_col = $a[0]; // table column
		$a_val = $a[1]; // value
		$a_sym = $a[2] ?? '='; // symbol

		if( $a_sym == 'IN' ){
			$and_cond .= "AND $a_col $a_sym (".implode(',', $a_val).") ";
		}else{
			$and_cond .= "AND $a_col $a_sym :$a_col ";
			$execute[$a_col] = $a_val;
		}
	}

	$s = $connect->prepare("SELECT $columns FROM $table WHERE 1=1 $and_cond $order_by");
	$s->execute($execute);

	return $columns == 'COUNT(*)' ? $s->fetch()[0] : $s->fetchAll(\PDO::FETCH_ASSOC);
}

function utf8ize($input){
	if( is_array($input) ){
		foreach($input as $key=>$value){
			$input[$key] = utf8ize($value);
		}
	}else if( is_string($input) ){
		$input = mb_convert_encoding($input, 'UTF-8', 'UTF-8');
	}

	return $input;
}

function c_date($format, $date){ // customize date
	if( in_array($date, ['0000-00-00 00:00:00', '0000-00-00', '']) ){
		$result = '';
	}else{
		$result = date($format, strtotime($date));
	}

	return $result;
}

function sess_or_cook($type, $index){
	if( $type == 'sess' && isset($_SESSION[$index]) ){
		$result = $_SESSION[$index];
		unset($_SESSION[$index]);
	}else if( $type == 'cook' && isset($_COOKIE[$index]) ){
		$result = $_COOKIE[$index];
		setcookie($index, false, time()-1);
	}else{
		$result = false;
	}

	return $result;
}
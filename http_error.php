<?php

require __DIR__.'/includes/partials/index.php';

/**
 * @ page dependencies
*/
$page_dependencies = new \stdClass;

$page_dependencies->page_title = '404 - Page Not Found';
$page_dependencies->breadcrumb = [
    '404 - Page Not Found' => ''
];
$page_dependencies->plugins = [];
$page_dependencies->js = [];
// end of page dependencies

?>
<!DOCTYPE html>
<html>
<head>
	<?php include DOCUMENT_ROOT.'/includes/partials/head.php'; ?>
</head>
<body>
	<div class="app">
		<?php include DOCUMENT_ROOT.'includes/partials/preloader.php'; ?>
		<?php include DOCUMENT_ROOT.'includes/partials/header.php'; ?>
		<?php include DOCUMENT_ROOT.'includes/partials/sidebar.php'; ?>
		<main class="app-main">
			<div class="wrapper">
				<div class="empty-state">
					<div class="empty-state-container">
						<div class="state-figure">
							<img class="img-fluid" src="<?= DOMAIN; ?>assets/images/404.svg" alt="" style="max-width: 320px">
						</div>
						<h3 class="state-header"> Page Not found! </h3>
						<p class="state-description lead text-muted"> Sorry, we've misplaced that URL or it's pointing to something that doesn't exist. </p>
						<div class="state-action">
							<a href="<?= DOMAIN; ?>" class="btn btn-lg btn-light"><i class="fa fa-angle-right"></i> Go Back To Homepage</a>
						</div>
					</div>
				</div>
			</div>
		</main>
	</div>

	<?php include DOCUMENT_ROOT.'includes/partials/script.php'; ?>
</body>
</html>